define([
], function () {
    'use strict';

    describe('Guard basics', function () {
        it('should register global oxguard data object', function () {
            //add a version string, needed to load oxguard/register module
            ox.serverConfig.version = '';
            return require(['oxguard/register']).then(function () {
                expect(window.oxguarddata).to.exist;
            });
        });
    });
});
