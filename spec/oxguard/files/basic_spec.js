define([
    'io.ox/core/extensions',
    'spec/shared/capabilities',
    'oxguard/files/register'
], function (ext, capabilities) {
    describe('The plugin for files', function () {
        window.before(function () {
            return capabilities
                .preset('pim')
                .enable('guard:drive');
        });

        describe('implements extension points and', function () {
            it('should provide toolbar link for encryption', function () {
                var point = ext.point('io.ox/files/classic-toolbar/links');
                expect(point.has('encrypt')).to.be.true;
            });

            it('should provide inline link to remove encryption', function () {
                var point = ext.point('io.ox/files/classic-toolbar/links');
                expect(point.has('remencrypt')).to.be.true;
            });
        });
    });
});
