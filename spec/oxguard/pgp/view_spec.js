define([
    'io.ox/core/extensions',
    'spec/shared/capabilities',
    'fixture!testmails.json',
    'settings!oxguard',
    'mailvelope/main',
    'pgp_mail/view-pgp'
], function (ext, caputil, testmails, settings, api) {
    'use strict';

    //FIXME: remove mailvelope dependency from this code, again!
    var capabilities = caputil.preset('common').init(['mailvelope/main'], [api]);

    describe('PGP Mail Views', function () {
        describe('extend io.ox/mail/detail/body', function () {
            var point = ext.point('io.ox/mail/detail/body');

            describe('for encrypted mails', function () {
                var server;
                beforeEach(function () {
                    server = ox.fakeServer.create();
                    server.respondWith('GET', /\/api\/mail\?action=attachment/, [
                        200, {
                            'Content-Type': 'text/plain'
                        },
                        testmails.encrypted_body
                    ]);
                    settings.set('cryptoProvider', 'mailvelope');
                    return capabilities.enable(['mailvelope']);
                });
                afterEach(function () {
                    server.restore();
                });
                it('should add "encrypted_content" extension', function () {
                    expect(point.keys()).to.contain('encrypted_content');
                });
                it('should render content for pgp/mime encrypted mails', function () {
                    //if this test fails, make sure, "oxguard/view/pgp/mail/encrypted" contains only the default extension

                    var baton = new ext.Baton(testmails.encrypted_mail);

                    //processing the mail's source will collect all pgp attachments
                    ext.point('io.ox/mail/detail/source').all().forEach(function (p) {
                        if (p.id === 'collect_pgp_attachments') return;
                        ext.point('io.ox/mail/detail/source').disable(p.id);
                    });
                    ext.point('io.ox/mail/detail/source').invoke('process', this, baton);

                    var node = $('<div class="content">');
                    point.all().forEach(function (p) {
                        if (p.id === 'encrypted_content') return;
                        point.disable(p.id);
                    });
                    point.invoke('draw', $('<section class="body">').append(node), baton);

                    server.respond();
                    expect(node.text()).to.contain('-----BEGIN PGP MESSAGE-----');
                });
                it('should add a CSS class "encrypted" to the mail content node', function () {
                    var baton = new ext.Baton(testmails.encrypted_mail);
                    var node = $('<div class="content">');
                    point.all().forEach(function (p) {
                        if (p.id === 'encrypted_content' || p.id === 'collect_pgp_attachments') return;
                        point.disable(p.id);
                    });
                    point.invoke('draw', $('<div class="body">').append(node), baton);
                    expect(node.hasClass('encrypted')).to.be.true;
                });

                describe('provide API to draw encrypted messages', function () {
                    it('should test for the requires method of point "oxguard/view/pgp/mail/encrypted"', function () {
                        var baton = new ext.Baton(testmails.encrypted_mail);
                        var node = $('<div class="content">');
                        point.all().forEach(function (p) {
                            if (p.id === 'encrypted_content' || p.id === 'collect_pgp_attachments') return;
                            point.disable(p.id);
                        });

                        var draw1 = sinon.spy();
                        var draw2 = sinon.spy();
                        var draw3 = sinon.spy();
                        ext.point('oxguard/view/pgp/mail/encrypted').extend({
                            id: 'no_requires',
                            draw: draw1
                        });
                        ext.point('oxguard/view/pgp/mail/encrypted').extend({
                            id: 'truthy_requires',
                            requires: function () {
                                return true;
                            },
                            draw: draw2
                        });
                        ext.point('oxguard/view/pgp/mail/encrypted').extend({
                            id: 'falsy_requires',
                            requires: function () {
                                return false;
                            },
                            draw: draw3
                        });
                        point.invoke('draw', $('<div class="body">').append(node), baton);
                        server.respond();

                        expect(draw1.calledOnce, 'draw method called').to.be.true;
                        expect(draw2.calledOnce, 'draw method called').to.be.true;
                        expect(draw3.calledOnce, 'draw method called').to.be.false;
                    });
                });
            });

            it('for signed mails', function () {
                expect(point.keys()).to.contain('signed_content');
            });
        });
    });
});
