module.exports = {
  root: true,
  env: {
    browser: true,
    es2021: true
  },
  extends: [
    'standard'
  ],
  rules: {
    'node/no-callback-literal': 1,
    'array-callback-return': 1,
    'no-mixed-operators': 1,
    camelcase: 1,
    'no-case-declarations': 1,
    'no-new': 1,
    'new-cap': 1,
    'prefer-regex-literals': 1,
    'no-cond-assign': 1,
    'no-control-regex': 1,
    'no-useless-escape': 1,
    'node/handle-callback-err': 1
  }
}
