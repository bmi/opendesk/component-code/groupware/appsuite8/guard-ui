import $ from '$/jquery'

import capabilities from '$/io.ox/core/capabilities'

const auth = {}

// This file is essentially mirror copy of the core version
// Duplicate is required for routing once Guard is installed
auth.authorize = function authorize (baton, options) {
  const def = $.Deferred()
  if (capabilities.has('guard')) {
    import('@/io.ox/guard/auth').then(function ({ default: auth_core }) {
      auth_core.authorize(baton, options).then(function (auth) {
        def.resolve(auth)
      }, function (reject) {
        def.reject(reject)
      })
    }, function (reject) {
      def.reject(reject)
    })
  } else {
    def.reject()
  }
  return def
}

export default auth
