/* All content on this website (including text, images, source
 * code and any other original works), unless otherwise noted,
 * is licensed under a Creative Commons License.
 *
 * http://creativecommons.org/licenses/by-nc-sa/2.5/
 *
 * Copyright (C) 2016-2020 OX Software GmbH
 */

const util = {}

util.hasEncryptedAttachment = function hasEncryptedAttachment (baton) {
  const attachments = baton.model.get('attachments').models
  for (let i = 0; i < attachments.length; i++) {
    if (attachments[i].get('epass') === undefined) { // Do not need to worry if epass present
      if (attachments[i].get('meta').encrypted === true) return (3)
      if (attachments[i].get('filename').indexOf('.pgp') > 0) return (3)
    }
  }
  return (0)
}

export default util
