/**
 * All content on this website (including text, images, source
 * code and any other original works), unless otherwise noted,
 * is licensed under a Creative Commons License.
 *
 * http://creativecommons.org/licenses/by-nc-sa/2.5/
 *
 * Copyright (C) 2016-2020 OX Software GmbH
 */

import $ from '$/jquery'
import _ from '$/underscore'
import guardModel from '@/io.ox/guard/core/guardModel'
import util from '@/io.ox/guard/util'
import { settings } from '@/io.ox/guard/settings/guardSettings'
import '@/io.ox/guard/oxguard_core'
import gt from 'gettext'
import icons from '@/io.ox/guard/core/icons'
import '@/io.ox/guard/mail/style.scss'

let prompting = false

function getKeyIcon () {
  return $('<div>').addClass('oxguard_token').append(icons.getIcon({ name: 'key', className: 'key' }))
}

function getFailIcon () {
  return $('<div>').addClass('oxguard_token failure').append(icons.getIcon({ name: 'slash-circle', className: 'key' }))
}

function getGuestIcon () {
  return $('<div>').addClass('oxguard_token').append(icons.getIcon({ name: 'person', className: 'key' }))
}

function drawKeyIcons (info, location) {
  const input = $(location).parent().find('.token-input')
  const inputwidth = input.width() - 30
  const key = getKeyIcon()
  if (info && info.result) {
    switch (info.result) {
      case 'pgp' :
        if (info.key.keySource && info.key.keySource.trusted) {
          key.addClass('trusted_key')
        } else {
          key.addClass('untrusted_key')
        }
        createTooltip(info.key, key, location)
        $(location).find('.close').before(key)
        break
      case 'new' :
        key.addClass('trusted_key')
        $(location).find('.close').before(key)
        createTooltip('new', key, location)
        break
      case 'guest' : {
        const gKey = getGuestIcon().addClass('guardGuest')
        $(location).find('.close').before(gKey)
        createTooltip('guest', gKey, location)
        break
      }
      case 'fail' :
        $(location).find('.close').before(getFailIcon())
        break
      default:
        console.error('Unknown key type ' + info.result)
    }
  }
  input.width(inputwidth)
}

function createTooltip (data, loc, t) {
  import('@/io.ox/guard/pgp/keyDetails').then(({ default: pubKeys }) => {
    let div = $('<div>')
    const name = $(t).attr('aria-label')
    div.append($('<p>').append(name))
    if (data === 'new') { // New recipient
      div.append(gt('Key will be created upon sending.'))
    } else if (data === 'guest') {
      div.append(gt('Will create new guest account for %s', guardModel().getName()))
    } else {
      const detail = pubKeys.keyDetail([data], true)
      div = $(detail.div).find('#keyDetail')
      div.append(gt('Source: ') + getSourceTranslation(data.keySource.name) + '<br/>')
      div.append(data.keySource.trusted ? gt('Trusted') : gt('Untrusted Source'))
      div.addClass(data.keySource.trusted ? 'trusted_key' : 'untrusted_key')
    }
    $(loc).tooltip({
      title: div.html(),
      trigger: 'hover',
      placement: 'bottom',
      html: true,
      selector: loc,
      delay: 200,
      animation: true,
      viewport: { selector: '.io-ox-mail-compose', padding: 10 }
    })
    const id = 'div' + (data.id ? data.id : Math.random())
    $(loc).append($('<div id="' + id + '" class="sr-only srdetails">').html(div.html()))
    t.attr('aria-describedby', id + ' ' + t.attr('aria-describedby'))
  })
}

function getSourceTranslation (source) {
  switch (source) {
    case 'HKP_PUBLIC_SERVER':
      return gt('Public server')
    case 'HKP_SRV_DNSSEC_SERVER':
      return gt('Secure DNS referred')
    case 'HKP_TRUSTED_SERVER':
      return gt('Trusted server')
    case 'HKP_SRV_SERVER':
      return gt('DNS referred')
    case 'HKP_CACHE':
      return gt('DNS referred')
    case 'GUARD_KEY_PAIR':
      return gt('System key')
    case 'GUARD_USER_UPLOADED':
      return gt('Uploaded key')
    case 'GUARD_USER_SHARED':
      return gt('Shared key')
    case 'WKS_SERVER':
      return gt('WebKey Directory Service')
    case 'WKS_SRV_DNSSEC_SERVER':
      return gt('WebKey DNSEC Service')
    case 'AUTOCRYPT_KEY':
      return gt('Autocrypt Keys')
    case 'AUTOCRYPT_KEY_USER_VERIFIED':
      return gt('Autocrypt Keys')
    default:
      return source
  }
}

// Return a has of string
function hash (str) {
  let hashed = 0
  if (str.length === 0) return hashed
  for (let i = 0; i < str.length; i++) {
    hashed = (31 * hashed + str.charCodeAt(i)) << 0
  }
  return hashed
}

function checkrecipient (baton) {
  if (!util.isGuardConfigured()) {
    if (!prompting) {
      prompting = true
      import('@/io.ox/guard/core/createKeys').then(({ default: keys }) => {
        keys.createKeysWizard()
          .done(function () {
            checkrecipient(baton)
          })
          .fail(function () {
            baton.view.securityModel.set('encrypt', false)
          })
          .always(function () {
            prompting = false
          })
      })
    }
    return// Do not check recipients until the master key is created (in case mailing to self)
  }
  import('@/io.ox/guard/mail/keymanager').then(({ default: keyman }) => {
    keyman.checkAll(baton)
  })
}

function unlock (baton) {
  if (_.device('smartphone')) {
    $('.btn-primary:visible').html(gt('Send'))
  } else if (baton.view.app.get('window')) {
    const headerview = baton.view.app.get('window').nodes.header
    const footerview = baton.view.app.get('window').nodes.footer
    headerview.find('.btn-primary:visible').html(gt('Send'))
    if (footerview) {
      footerview.find('.btn-primary:visible').html(gt('Send'))
    }
  }
  baton.view.$el.find('.oxguard_key').hide()
  baton.view.$el.find('.oxguard_token').hide()
  baton.view.$el.find('.oxguard_token .srdetails').remove()
}

function lock (baton) {
  if (_.device('smartphone')) {
    $('.btn-primary:visible').html((settings.get('advanced') ? gt('Send Encrypted') : gt('Send Secure')))
  }
  checkrecipient(baton)
  baton.view.$el.find('.oxguard_key').show()
  baton.view.$el.find('.oxguard_token').show()
}

export default {
  lock: lock,
  hash: hash,
  unlock: unlock,
  drawKeyIcons: drawKeyIcons
}
