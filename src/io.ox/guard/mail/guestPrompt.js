/**
 * All content on this website (including text, images, source
 * code and any other original works), unless otherwise noted,
 * is licensed under a Creative Commons License.
 *
 * http://creativecommons.org/licenses/by-nc-sa/2.5/
 *
 * Copyright (C) 2016-2020 OX Software GmbH
 */

import $ from '$/jquery'
import ox from '$/ox'
import ModalDialog from '$/io.ox/backbone/views/modal'
import gt from 'gettext'
import guardModel from '@/io.ox/guard/core/guardModel'
import '@/io.ox/guard/mail/style.scss'

// Display options for guest account header (introduction)
function guestOptions (baton) {
  const def = $.Deferred()
  new ModalDialog({
    async: true,
    point: 'io.ox/guard/mail/guestPrompt',
    id: 'guestPrompt',
    title: gt('Recipient Options'),
    width: 490,
    data: {
      baton: baton,
      def: def
    }
  })
    .extend({
      body: function () {
        const data = this.options.data
        const help = $('<p>' + gt('At least one of your recipients uses a different webmail system.  They will receive an email with a link for them to read this message.  If you like, you can add a personalized message that will appear at the top of this message.  This will help identify the message as coming from you.') + '</p>')
        const messageLabel = $('<label class="oxguard_message_label">' + gt('Message:') + '</label>')
        data.message = $('<textarea type="text" id="message" class="og_guest_message"></textarea>')
        const languageLabel = $('<br/><label style="width:100%; padding-top:10px;">' + gt('Language for recipient Email') + '</label>')
        data.langselect = $('<select class="og_language"></select>')
        const warning = $('<p class="oxguard_warn" style="margin-top:10px;">').append(gt('Note: Emails sent to mailing lists or groups will not work.'))
        // Populate language selector with languages avail
        const languages = guardModel().get('lang')
        $.each(languages, function (c, n) {
          data.langselect.append('<option value="' + c + '">' + n + '</option>')
        })
        this.$body.append(help).append(messageLabel).append(data.message).append(languageLabel.append(data.langselect)).append(warning)
      },
      setup: function () {

      }
    })
    .addButton({ label: gt('OK'), action: 'ok' })
    .addCancelButton()
    .on('ok', function () {
      const message = this.options.data.message
      const data = {
        message: message.val(),
        language: this.options.data.langselect.val()
      }
      const def = this.options.data.def
      this.close()
      // Check if requires PIN to be displayed
      Promise.all([import('@/io.ox/guard/mail/pin'),
        import('$/io.ox/core/capabilities')])
        .then(function ([{ default: pin }, { default: capabilities }]) {
          if (capabilities.has('guard-pin')) {
            pin.showpin(baton)
              .done(function (e) {
                data.pin = e
                def.resolve(data)
              })
              .fail(function () {
                def.resolve(data)
              })
          } else def.resolve(data)
        })
    })
    .on('cancel', function () {
      this.options.data.def.reject()
      baton.view.app.getWindow().idle()
    })
    .on('open', function () {
      const urlLang = ox.language
      if ($('.og_language option[value="' + urlLang + '"]').length > 0) {
        this.options.data.langselect.val(urlLang)
      } else {
        this.options.data.langselect.val(urlLang.substring(0, 2))
      }
    })
    .open()

  return def
}

export default {
  guestOptions: guestOptions
}
