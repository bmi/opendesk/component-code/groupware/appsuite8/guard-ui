/**
 * All content on this website (including text, images, source
 * code and any other original works), unless otherwise noted,
 * is licensed under a Creative Commons License.
 *
 * http://creativecommons.org/licenses/by-nc-sa/2.5/
 *
 * Copyright (C) 2016-2020 OX Software GmbH
 */

import $ from '$/jquery'
import ox from '$/ox'
import keysAPI from '@/io.ox/guard/api/keys'
import gt from 'gettext'
import { settings as calSettings } from '$/io.ox/calendar/settings'

// We only want to pull the blacklist once
let blackList

// Check the list of previously fetched key results to see if the email is there.  If so, return, otherwise query backend
function checkRecips (email, baton) {
  const def = $.Deferred()
  let gkeys = baton.config.get('gkeys')

  if (gkeys === undefined) {
    // If undefined, create gkeys now
    gkeys = {}
    baton.config.set('gkeys', gkeys)
    getKeyResult(email, baton)
      .done(function (keyResults) {
        def.resolve(keyResults)
      })
    return def
  }
  if (gkeys[email] && !gkeys[email].pending) {
    def.resolve(gkeys)
    return def
  }
  if (gkeys[email] && gkeys[email].pending) {
    return gkeys[email].def
  }
  getKeyResult(email, baton)
    .done(function (keyResults) {
      def.resolve(keyResults)
    })
  return def
}

// Query guard server for key result
function getKeyResult (email, baton) {
  const def = $.Deferred()
  const result = {
    email: email,
    result: null,
    pending: true,
    def: def
  }
  const gkeys = baton.config.get('gkeys')

  gkeys[email] = result
  baton.config.set('gkeys', gkeys)

  if (isBlackListed(email)) {
    import('$/io.ox/core/notifications').then(({ default: notifications }) => {
      notifications.yell('error', gt('Mailing list / blocked email found.  Encrypted emails to this email will fail.  %s', email))
    })
    baton.view.securityModel.set('encrypt', false)
    def.resolve(updateKeys(email, baton, 'fail'))
  }

  return keysAPI.getRecipientsPublicKey(email).then(function (data) {
    try {
      if (data.preferredFormat === 'PGP/INLINE') {
        // If needs inline, then set
        baton.view.securityModel.setInline()
      }
      const keys = (data.publicRing && data.publicRing.keys) ? data.publicRing.keys : undefined
      let encryptionKey
      if (keys) {
        keys.forEach(function (key) {
          if (key.encryptionKey && !key.expired && !key.revoked) {
            encryptionKey = key
            encryptionKey.keySource = data.keySource
            const lang = ox.language.substring(0, 2)
            encryptionKey.Created = (new Date(key.creationTime)).toLocaleDateString(lang, { year: '2-digit', month: '2-digit', day: '2-digit' })
          }
        })
      }
      if (encryptionKey || data.newKey) {
        // Expired keys.  Should never occur.  Remove code?
        if (encryptionKey && encryptionKey.expired === true) {
          import('$/io.ox/core/notifications').then(({ default: notifications }) => {
            notifications.yell('error', gt('The key for %s has expired.  You will not be able to send an encrypted email to this recipient.', data.email))
          })
          return { result: 'fail', keyData: encryptionKey }
        }
        //
        if (data.guest && data.newKey) {
          return { result: 'guest', keyData: encryptionKey }
        }
        return { result: data.newKey ? 'new' : 'pgp', keyData: encryptionKey }
      }

      return { result: 'fail', keyData: encryptionKey }
    } catch (e) {
      console.log(e)
    }
  }, function (error) {
    return { result: 'fail', keyData: error }
  }).then(function (data) {
    return def.resolve(updateKeys(email, baton, data.result, data.keyData))
  })
}

// Check if email is in blacklist
function isBlackListed (email) {
  if (!blackList) { // Initialize blacklist
    blackList = calSettings.get('participantBlacklist')
    if (blackList) {
      blackList = blackList.toLowerCase()
    } else {
      blackList = ''
    }
  }
  if (blackList && blackList !== '') {
    if (blackList.indexOf(email.toLowerCase()) > -1) {
      return true
    }
  }

  return false
}

// Update the list of gkeys with the result
function updateKeys (email, baton, result, keyData) {
  const gkeys = baton.config.get('gkeys')
  gkeys[email] = {
    result: result,
    pending: false,
    key: keyData
  }
  baton.config.set('gkeys', gkeys)
  baton.model.trigger('change:gkeys')
  return gkeys
}

function updateTokens (baton) {
  baton.model.trigger('change:to', baton.model, baton.model.get('to'))
  baton.model.trigger('change:cc', baton.model, baton.model.get('cc'))
  baton.model.trigger('change:bcc', baton.model, baton.model.get('bcc'))
}

function checkAll (baton) {
  updateTokens(baton)
}

// Delete specific email from gkeys array
function deleteRecip (email, baton) {
  const gkeys = baton.config.get('gkeys')
  delete gkeys[email]
  baton.config.set('gkeys', gkeys)
}

// Check if an email is located in gkeys
function isPresent (email, recips) {
  for (let i = 0; i < recips.length; i++) {
    if (recips[i][1] === email) return true
  }
  return false
}

// Clean the list of gkeys, remove tokens that were deleted
function clean (baton) {
  const to = baton.model.get('to') || []
  const cc = baton.model.get('cc') || []
  const bcc = baton.model.get('bcc') || []
  const total = to.concat(cc).concat(bcc)
  const gkeys = baton.config.get('gkeys') || []
  const newkeys = []
  for (const email in gkeys) {
    if (isPresent(email, total)) {
      newkeys[email] = gkeys[email]
    }
  }
  baton.config.set('gkeys', newkeys)
}

// Check if Guest present in list
function isGuest (baton) {
  try {
    clean(baton)
    let keys = baton.config.get('gkeys')
    if (keys === undefined) keys = []
    for (const email in keys) {
      if (keys[email].result === 'guest') return true
    }
  } catch (e) {
    console.log(e)
  }

  return false
}

function getGuests (baton) {
  const guests = []
  try {
    let keys = baton.config.get('gkeys')
    if (keys === undefined) keys = []
    for (const email in keys) {
      if (keys[email].result === 'guest') guests.push(email)
    }
  } catch (e) {
    console.log(e)
  }
  return (guests)
}

export default {
  checkRecips: checkRecips,
  checkAll: checkAll,
  clean: clean,
  isGuest: isGuest,
  getGuests: getGuests,
  deleteRecip: deleteRecip
}
