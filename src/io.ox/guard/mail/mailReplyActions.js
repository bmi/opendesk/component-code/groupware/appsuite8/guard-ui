/* All content on this website (including text, images, source
 * code and any other original works), unless otherwise noted,
 * is licensed under a Creative Commons License.
 *
 * http://creativecommons.org/licenses/by-nc-sa/2.5/
 *
 * Copyright (C) 2016-2020 OX Software GmbH
 */

import _ from '$/underscore'
import $ from '$/jquery'
import gt from 'gettext'
import { settings } from '@/io.ox/guard/settings/guardSettings'
import ext from '$/io.ox/core/extensions'
import guardModel from '@/io.ox/guard/core/guardModel'

ext.point('io.ox/mail/compose/boot').extend({
  id: 'detectNewMail',
  index: 'first',
  perform: function (baton) {
    // returns here if any other type than new (e.g. replies etc)
    if (baton.data && baton.data.type && baton.data.type.toLowerCase() !== 'new') return
    // returns here, if restorepoint or copy of draft is opened
    if (baton.data && baton.data.id) return
    baton.config = _.extend({}, baton.config, { newmail: true })
  }
})

ext.point('io.ox/mail/compose/boot').extend({
  id: 'checkSecuritySettings',
  index: 1001,
  perform: function (baton) {
    if (baton.data.original && baton.data.original.security && baton.data.original.security.decrypted) {
      baton.view.securityModel.set('authToken', baton.data.original.security.authentication, true)
      baton.view.securityModel.set('encrypt', true)
    }
    if (baton.data.type === 'reply' || baton.data.type === 'replyall' || baton.data.type === 'forward') {
      checkReply(baton)
    }
    if (baton.data.type === 'edit') {
      checkEdit(baton)
    }
    checkEncrFile(baton)
    if (baton.model.get('security').encrypt) {
      import('@/io.ox/guard/util').then(({ default: util }) => {
        if (!util.hasGuardMailCapability() || util.isGuest()) {
          baton.view.toggleEncryption.forceEncryption()
          disableRecipients(baton.view)
        }
      })
      baton.model.save()
    }
  }
})

function checkEncrFile (baton) {
  if (!baton.data) return
  const atts = baton.data.attachments
  if (atts) {
    atts.forEach((att) => {
      if (att.encrypted) {
        baton.view.securityModel.set('encrypt', true)
      }
    })
  }
}

// If loading a draft email, need to set security settings same as before
function checkEdit (baton) {
  const obj = baton.data.original
  if (obj && obj.headers && obj.headers['X-Security']) {
    const reply = obj.headers['In-Reply-To'] !== undefined // If this is editing a reply
    const securitySettings = _.isArray(obj.headers['X-Security']) ? obj.headers['X-Security'][0] : obj.headers['X-Security']
    const settings = securitySettings.split(';')
    settings.forEach(function (setting) {
      const s = setting.split('=')
      const value = (s[1] ? (s[1].indexOf('true') > -1) : false)
      switch (s[0].trim()) {
        case 'sign':
          baton.view.securityModel.set('PGPSignature', value)
          break
        case 'encrypt':
          baton.view.securityModel.set('encrypt', value)
          if (value === true && reply) { // Check if we should enforce the secure reply here
            if (guardModel().getSettings().secureReply) {
              baton.view.toggleEncryption.forceEncryption()
            }
          }
          break
        case 'pgpInline':
          if (baton.view.securityModel.get('encrypt')) baton.view.securityModel.set('PGPFormat', (value ? 'inline' : 'mime'))
          break
        default:
          console.error('Unknown security parameter ' + s[0])
      }
    })
    if (obj.security.authentication) {
      baton.view.securityModel.set('authentication', obj.security.authentication)
    }
    baton.model.unset('security_info')
  }
}

// When doing reply/forward, check essential security data moved to new model
function checkReply (baton) {
  const obj = baton.data.original
  const securityModel = baton.view.securityModel
  if (obj && obj.security && obj.security.decrypted) {
    securityModel.set('encrypt', true)
    const security = baton.model.get('security')
    security.encrypt = true
    if (obj.security.authentication) {
      security.authentication = obj.security.authentication
    }
    const headers = baton.model.get('headers') || {}
    baton.model.set('headers', _.extend(headers, { 'X-Guard-msg-ref': obj.id }))
    security.msgRef = obj.id
    if (guardModel().getSettings().secureReply) {
      baton.view.toggleEncryption.forceEncryption()
    }
    baton.model.set('security', _.extend(baton.model.get('security'), { authentication: obj.security.authentication }))
  }
  baton.model.unset('security_info')

  // Check for default signed in reply emails
  if (settings.get('defaultSign')) {
    securityModel.set('PGPSignature', true)
  }
}

function disableRecipients (view) {
  view.$el.find('.recipient-actions').remove()
  view.$el.find('.tokenfield').css('background-color', '#e0e0e0')
  view.$el.find('.token-input').click(function (e) {
    $(e.currentTarget).blur()
    e.preventDefault()
    import('@/io.ox/guard/oxguard_core').then(({ default: core }) => {
      core.notifyError(gt('Recipients cannot be added to this encrypted reply'))
    })
  })
  view.$el.find('.twitter-typeahead').hide()
}
