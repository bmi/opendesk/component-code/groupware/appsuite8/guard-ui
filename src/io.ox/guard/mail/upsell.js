/* All content on this website (including text, images, source
 * code and any other original works), unless otherwise noted,
 * is licensed under a Creative Commons License.
 *
 * http://creativecommons.org/licenses/by-nc-sa/2.5/
 *
 * Copyright (C) 2016-2020 OX Software GmbH
 */

import http from '@/io.ox/guard/core/og_http'
import ox from '$/ox'
import $ from '$/jquery'

let adding = false

// Add upsell message to header of email
function upsell (location) {
  const params = '&cid=' + ox.context_id + '&id=' + ox.user_id + '&lang=' + ox.language
  if (adding) return
  adding = true
  http.get(ox.apiRoot + '/oxguard/mail?action=upsell', params)
    .done(function (data) {
      const upsellDiv = $('<div class="guardUpsell"></div>').append(data)
      location.find('.detail-view-header').after(upsellDiv)
    })
    .always(function () {
      adding = false
    })
}

export default upsell
