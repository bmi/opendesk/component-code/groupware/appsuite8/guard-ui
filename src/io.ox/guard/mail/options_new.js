/**
 * All content on this website (including text, images, source
 * code and any other original works), unless otherwise noted,
 * is licensed under a Creative Commons License.
 *
 * http://creativecommons.org/licenses/by-nc-sa/2.5/
 *
 * Copyright (C) 2016-2020 OX Software GmbH
 */

import $ from '$/jquery'
import core from '@/io.ox/guard/mail/oxguard_mail_compose_recip'
import coreUtil from '@/io.ox/guard/util'
import { settings } from '@/io.ox/guard/settings/guardSettings'
import guardModel from '@/io.ox/guard/core/guardModel'
import gt from 'gettext'
import '@/io.ox/guard/mail/style.scss'

function createlock (baton) {
  baton.view.listenTo(baton.view.securityModel, 'change:encrypt', function () {
    setLock(baton)
  })
  setLock(baton)
}

function setLock (baton) {
  if (baton.view.securityModel.get('encrypt')) {
    core.lock(baton)
  } else {
    core.unlock(baton)
  }
}

function warnHTML (baton) {
  const format = baton.view.securityModel.get('PGPFormat')
  const encrypt = baton.view.securityModel.get('encrypt')
  if (format !== 'inline' || !encrypt) return // Don't display warning if not Inline or not encrypted
  if (baton.config.get('editorMode') === 'text') return
  import('$/io.ox/backbone/views/modal').then(({ default: ModalDialog }) => {
    new ModalDialog({
      async: true,
      point: 'io.ox/guard/files/htmlWarning',
      title: gt('HTML Formatted Email'),
      id: 'htmlWarning',
      width: 450
    })
      .extend({
        explanation: function () {
          this.$body
            .append($('<p>').text(gt('HTML formatted emails may cause issues with PGP inline.  Click OK to change to Plain Text formatted Emails.')))
        }
      })
      .addButton({ label: gt('OK'), action: 'ok' })
      .addCancelButton()
      .on('ok', function () {
        this.close()
        baton.config.set('editorMode', 'text')
        baton.config.on('change:editorMode', function () {
          if ((baton.config.get('editorMode') === 'html') && (baton.view.securityModel.get('PGPFormat') === 'inline')) {
            new ModalDialog({
              async: true,
              point: 'io.ox/guard/files/htmlSwitchWarning',
              title: gt('HTML Formatted Email'),
              id: 'htmlSwitchWarning',
              width: 450
            })
              .extend({
                explanation: function () {
                  this.$body.append($('<p>').append(gt('HTML format is not recommended when using PGP Inline')))
                }
              })
              .addButton({ label: gt('OK'), action: 'ok' })
              .on('ok', function () {
                this.close()
              })
              .open()
          }
        })
      })
      .open()
  })
}

function createOptions (baton, count) {
  if (!guardModel().isLoaded()) {
    coreUtil.addOnLoaded(function () {
      createOptions(baton, count)
    })
    return
  }

  const securityModel = baton.view.securityModel

  const headers = baton.model.get('headers')
  if (headers && (headers['X-OxGuard-Sign'] === 'true')) {
    securityModel.set('PGPSignature', true)
  }

  createlock(baton)
  // Guard bug GUARD-200, deleted user still has defaults applied.  Verify configured before applying
  if (baton.config.get('newmail') === true && coreUtil.hasGuardMailCapability() && coreUtil.isGuardConfigured()) { // New email doesn't have recips yet.  Check to see if there are defaults
    if (settings.get('defaultInline')) {
      securityModel.setInline()
    } else securityModel.setMime()

    baton.config.unset('newmail')

    if (settings.get('defaultSign')) {
      securityModel.set('PGPSignature', true)
    }

    securityModel.on('change:PGPFormat change:encrypt', function () {
      warnHTML(baton)
    })

    // If default is to use guard, go ahead and lock
    if (settings.get('defaultEncrypted')) {
      securityModel.set('encrypt', true)
      core.lock(baton)
    }
  }

  securityModel.on('change:PGPFormat', function () {
    securityModel.set('encrypt', true)
  })

  securityModel.on('change:PubKey', function () {
    if (securityModel.get('PubKey')) {
      attachPubKey(baton)
    }
  })
}
// Attach the users public key to the email
function attachPubKey (baton) {
  const attachments = baton.model.get('attachments')
  const securityModel = baton.view.securityModel
  for (let i = 0; i < attachments.models.length; i++) {
    if (attachments.models[i].get('guard') === 'pubkey') {
      securityModel.set('PubKey', false)
      return
    }
  }
  Promise.all([import('@/io.ox/guard/api/keys'),
    import('$/io.ox/core/notifications'),
    import('$/io.ox/mail/compose/api')])
    .then(function ([{ default: keysAPI }, { default: notify }, { default: composeApi }]) {
      keysAPI.download({ keyType: 'public' })
        .done(function (data) {
          try {
            const file = new Blob([data.key], { type: 'application/pgp-keys' })
            file.name = 'public.asc'
            composeApi.space.attachments.add(baton.model.get('id'), { file: file })
              .then(function (data) {
                baton.model.attachFiles(data)
              })
          } catch (e) {
            console.error(e)
            notify.yell('error', gt('Unable to attach your public key'))
          }
        })
        .fail(function () {
          notify.yell('error', gt('Unable to attach your public key'))
        })
        .always(function () {
          securityModel.set('PubKey', false)
        })
    })
}

export default {
  createOptions: createOptions,
  warnHTML: warnHTML
}
