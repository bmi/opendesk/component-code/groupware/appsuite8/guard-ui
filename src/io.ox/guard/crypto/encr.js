import JS from 'jsencrypt'
import guardModel from '@/io.ox/guard/core/guardModel'

function getEncrypted (pubKey, data) {
  try {
    if (pubKey.key === null) return (null)
    const encrypt = new JS()
    encrypt.setPublicKey(pubKey.key)
    if (encrypt.getPublicKey().indexOf('PUBLIC') < 0) {
      console.log('pubkey import fail')
      return (null)
    }
    const encrypted = encrypt.encrypt(data)
    return (encrypted)
  } catch (e) {
    console.log(e)
    return (null)
  }
}

function cryptPass (password) {
  if (guardModel().get('pubKey') === undefined) return (null)
  return (getEncrypted(guardModel().get('pubKey'), password))
}

/*
function decodeKey(key, password) {

    try {
        var privKey = '';
        if (key.indexOf('BEGIN ENCRYPTED PRIVATE KEY') > 0) { //pkcs8 vs 5
            privKey = pk.PKCS5PKEY.getPlainPKCS8HexFromEncryptedPKCS8PEM(key, password);
        } else {
            privKey = pk.PKCS5PKEY.getDecryptedKeyHex(key, password.trim());
        }
        return (privKey);
    } catch (e) {
        console.log(e);
    }

}

function createForm(location) {
    var dialog = new dialogs.CreateDialog({ width: 450, center: true,  enter : 'ok'});
    dialog.header($('<h4>').text(gt('Need Private Key')));
    var info = $('<p>' + gt('Please select the file location of your private key.') + '<br/>' + gt('The key will be used locally only.  It will not be sent to the server.') + '</p>');
    var password = $('<label>' + gt('Private Key Password') + '</label><input type="password" id="priv_pass"/></br>');
    info.append(password);
    var btn = $('<button class="btn og_getkey">' + gt('Select Key Location') + '</button>');

    var input = $('<input type="file" id="files" name="files[] style="padding:10px;visibility:hidden;position:absolute;top-50px;left:-50px;"/>');
    btn.click(function () {
        input.click();
    });
    input.change(function (evt) {
        var file = evt.target.files[0];
        var reader = new FileReader();

        reader.onload = function (event) {
            var data = event.target.result;
            var privkey = decodeKey(data, $('#priv_pass').val());
            test(privkey);
            dialog.close();
        };
        reader.readAsText(file);

    });
    btn.after(input);
    dialog.getBody().append(info.append(btn));
    dialog
    .addButton('cancel', gt('Cancel'), 'cancel');
    dialog.show();
}
*/

export { getEncrypted, cryptPass }
