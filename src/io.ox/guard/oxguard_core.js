/*
 * All content on this website (including text, images, source
 * code and any other original works), unless otherwise noted,
 * is licensed under a Creative Commons License.
 *
 * http://creativecommons.org/licenses/by-nc-sa/2.5/
 *
 * Copyright (C) 2016-2020 OX Software GmbH
 */

import $ from '$/jquery'
import gt from 'gettext'
import _ from '$/underscore'
import ox from '$/ox'
import authAPI from '@/io.ox/guard/api/auth'
import loginAPI from '@/io.ox/guard/api/login'
import util from '@/io.ox/guard/util'
import guardModel from '@/io.ox/guard/core/guardModel'

let badCount = 0

function metrics (app, action) {
  import('$/io.ox/metrics/main').then(({ default: metrics }) => {
    if (!metrics.isEnabled()) return
    metrics.trackEvent({
      app: app,
      target: 'guard',
      type: 'click',
      action: action
    })
  })
}

// Escape html data to prevent cross scripting attack
function htmlSafe (data) {
  const xss = {
    '&': '&amp;',
    '<': '&lt;',
    '>': '&gt;',
    '"': '&quot;',
    "'": '&#39;',
    '/': '&#x2F;',
    '`': '&#x60;',
    '=': '&#x3D;'
  }
  return String(data).replace(/[&<>"'`=/]/g, function (s) {
    return xss[s]
  })
}

/// / Failure Handling

function handleJsonError (error, errorResponse, baton) {
  errorResponse.errorMessage = gt('Error')
  errorResponse.retry = false
  if (error.code !== undefined) {
    switch (error.code) {
      case 'GRD-PGP-0005':
        if (guardModel().hasAuth()) { // If bad password based on auth, then clear auth and redo
          guardModel().clearAuth()
          if (baton) baton.view.redraw()
          return
        }
        errorResponse.errorMessage = gt('Unable to decrypt Email, incorrect password.')
        notifyError(gt('Bad password'))
        badCount++
        if (badCount > 2 && guardModel().get('recoveryAvail')) {
          errorResponse.errorMessage += ('<br/>' + gt('To reset or recover your password, click %s', '<a id="guardsettings" href="#">' + gt('Settings') + '</a>'))
        }
        errorResponse.retry = true
        break
      default:
        if (error.error !== undefined) {
          errorResponse.errorMessage = error.error
        }
    }
  }
}

function showError (errorJson) {
  const errorResp = {}
  try {
    handleJsonError(errorJson, errorResp)
  } catch (e) {
    errorResp.errorMessage = gt('Unspecified error')
  }
  notifyError(errorResp.errorMessage)
}

function notifyError (message) {
  notify('error', message)
}

function notify (type, message) {
  import('$/io.ox/core/notifications').then(({ default: notify }) => {
    notify.yell(type, message)
  })
}

function checkJsonOK (json) {
  if (json && json.error) {
    showError(json)
    return false
  }
  return true
}

/// Authentication Section

// Password prompt
function getPassword (message, timeOption, options) {
  const def = $.Deferred()
  import('@/io.ox/guard/core/passwordPrompt').then(({ default: prompt }) => {
    prompt.open(message, timeOption, options)
      .done(function (data) {
        def.resolve(data)
      })
      .fail(function (e) {
        def.reject(e)
      })
  })
  return (def)
}

// Save a password in both auth code and token
function savePassword (password, minutesValid) {
  // TODO: ensure calling methods use right value
  // 0 for session
  if (minutesValid > 9999) minutesValid = 0

  return authAPI.authAndSave({
    password: password,
    minutesValid: minutesValid
  }).then(function (authdata) {
    let data = authdata
    if (_.isArray(authdata)) {
      data = authdata[0] // This is a temporary fix for working with smime 2.10.7
    }
    storeAuth(data.auth, data.minutesValid)
    return data
  })
    .fail(function (e) {
      switch (e.code) {
        case 'GRD-MW-0003':
          notifyError(gt('Bad password'))
          break
        case 'GRD-MW-0005':
          notifyError(gt('Account locked out due to bad attempts.  Please try again later.'))
          break
        default:
          notifyError(gt('Error checking password') + '\r\n' + e.error)
          break
      }
      console.error(e)
    })
}

// Store the auth locally for a time
function storeAuth (auth, duration) {
  if (duration >= 0) {
    guardModel().setAuth(auth)
    const time = duration * 1000 * 60
    if (time > 0) { // 0 is indefinite
      window.setTimeout(function () {
        guardModel().clearAuth()
      }, time)
    }
  }
}

// Check if authentication token in session and valid
const checkAuth = (function () {
  // TODO: maybe it's possible to use api-event-handlers instead ('before:check', 'success:check')
  function preAuthCallback (o) {
    if (o && _.isFunction(o.preAuthCallback)) {
      o.preAuthCallback()
      return true
    }
    return false
  }
  // TODO: maybe it's possible to use api-event-handlers instead ('fail:check')
  /**  TODO FIX THIS
    function failAuthCallback(o) {
        if (o && _define('oxguard/oxguard_core', [
            'oxguard/api/auth',
            'oxguard/api/login',
            'oxguard/util',
            'gettext!oxguard'
        ], function (authAPI, loginAPI, util, gt) {
        .isFunction(o.failAuthCallback)) {
            o.failAuthCallback();
        }
    }
    */
  function callback (o, auth) {
    if (o && _.isFunction(o.callback)) {
      o.callback(auth)
    }
  }

  return function (o) {
    let preAuthDone
    // stored auth code
    if (guardModel().hasAuth()) {
      preAuthDone = preAuthCallback(o)
    }
    return authAPI.check().then(function (data) {
      storeAuth(data.auth, data.minutesValid)
      // OK only if exists and still time left.
      if (!preAuthDone) preAuthCallback(o)
      callback(o, data.auth)
      return data
    }, function (e) {
      if (ox.debug) console.log(e)
      // if (preAuthDone) failAuthCallback(o)
      return $.Deferred().reject('fail')
    })
  }
})()

// Authenticate against the Guard server, checking for specific key ID, return settings info
// TODO: remove unused params userid/extra (check other potentially calling modules like office first)
function auth (userid, password, extra, keyid) {
  return util.getUsersPrimaryEmail().then(function (email) {
    if (!email) return $.Deferred().reject()
    return loginAPI.login({
      encr_password: password,
      lang: ox.language,
      email: email
    }, keyid)
  })
}

// Simple call to verify password.  Keyid optional
function verifyPassword (password, keyid) {
  return auth(undefined, password, undefined, keyid).then(function (data) {
    if (data.auth && data.auth.length > 20) {
      return // Good auth
    }
    import('@/io.ox/guard/auth').then(({ default: auth_core }) => auth_core.handleFail(data.auth))
    return $.Deferred.reject()
  })
}

export default {
  auth: auth,
  verifyPassword: verifyPassword,
  checkAuth: checkAuth,
  checkJsonOK: checkJsonOK,
  getPassword: getPassword,
  htmlSafe: htmlSafe,
  metrics: metrics,
  notify: notify,
  notifyError: notifyError,
  savePassword: savePassword,
  showError: showError,
  storeAuth: storeAuth
  // moved to util
  // autoCryptEnabled: util.autoCryptEnabled,
  // sanitize: util.sanitize,
  // validateEmail: util.validateEmail
}
