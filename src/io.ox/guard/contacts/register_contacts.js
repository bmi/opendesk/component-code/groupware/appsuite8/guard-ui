/**
 * All content on this website (including text, images, source
 * code and any other original works), unless otherwise noted,
 * is licensed under a Creative Commons License.
 *
 * http://creativecommons.org/licenses/by-nc-sa/2.5/
 *
 * Copyright (C) 2016-2020 OX Software GmbH
 */

import $ from '$/jquery'
import ext from '$/io.ox/core/extensions'
import pubkeys from '$/io.ox/guard/pgp/keyDetails'
import { settings } from '@/io.ox/guard/settings/guardSettings'
import gt from 'gettext'
import '@/io.ox/guard/style.scss'
import '@/io.ox/guard/contacts/style.scss'

ext.point('io.ox/contacts/detail/content').extend({
  id: 'pgppublic',
  index: 'last',
  draw: function (baton) {
    // FIXME: this completely disables the point for apps other than settings and contacts (like mail called from sidepanel)
    if (!(baton.app && baton.app.cid)) return
    // advanced view only
    if (!settings.get('advanced')) return
    // not listing for distribution lists
    if (baton.data && baton.data.distribution_list) return

    const cid = baton.app.cid; const data = baton.data

    this.append(
      $('<section class="block" data-block="guard">').attr('id', 'contactKeys' + cid).append(
        $('<dl class="dl-horizontal">').append(
          $('<dt>').text(gt('PGP Keys')),
          $('<dd>').append(
            // only keys ids are listed - all other elements are hidden (css)
            pubkeys.listPublic({
              header: false,
              id: cid,
              folder_id: data.folder_id,
              contact_id: data.id
            })
          )
        )
      )
    )
  }
})
