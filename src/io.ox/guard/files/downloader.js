/**
 * All content on this website (including text, images, source
 * code and any other original works), unless otherwise noted,
 * is licensed under a Creative Commons License.
 *
 * http://creativecommons.org/licenses/by-nc-sa/2.5/
 *
 * Copyright (C) 2016-2020 OX Software GmbH
 */

// Open file in viewer

import ox from '$/ox'
import _ from '$/underscore'

function viewFile (baton, action, auth) {
  const file_options = {
    params: {
      cryptoAction: 'Decrypt',
      cryptoAuth: auth,
      session: encodeURIComponent(ox.session)
    }
  }

  baton.array().map(function (file) {
    file.file_options = file_options
    file.source = 'guardDrive'
  })
  const selection = [].concat(baton.data)
  // Check if multiple files for scrolling
  if (baton.all) {
    baton.all.models.map(function (file) {
      if (file.get('filename') && (/(\.pgp)$/i.test(file.get('filename')) || (file.get('source') === 'guard') || (file.get('meta') && (file.get('meta').Encrypted === true)))) { // Make sure OxGuard file
        file.set('file_options', file_options)
        file.set('source', 'guardDrive')
      }
      file.on('change:source', function () { // Make sure source isn't changed in file updates
        file.set({ source: 'guardDrive' }, { silent: true })
      })
    })
  }

  if (baton.isViewer) { // If already in viewer, display the version selected
    if (!baton.viewerEvents) { return }
    baton.viewerEvents.trigger('viewer:display:version', baton.data)
    return
  }
  import('$/io.ox/core/viewer/main').then(({ default: Viewer }) => {
    const viewer = new Viewer()
    if (selection.length > 1) {
      // only show selected files - the first one is automatically selected
      viewer.launch({ files: selection })
    } else {
      viewer.launch({ selection: _(selection).first(), files: (baton.all ? baton.all.models : selection) })
    }
  })
}

export default {
  viewFile: viewFile
}
