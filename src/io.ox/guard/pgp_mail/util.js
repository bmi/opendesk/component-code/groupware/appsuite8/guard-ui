/**
 * All content on this website (including text, images, source
 * code and any other original works), unless otherwise noted,
 * is licensed under a Creative Commons License.
 *
 * http://creativecommons.org/licenses/by-nc-sa/2.5/
 *
 * Copyright (C) 2016-2020 OX Software GmbH
 * Author Greg Hill <greg.hill@open-xchange.com>
 */

import _ from '$/underscore'
import $ from '$/jquery'
import guardModel from '@/io.ox/guard/core/guardModel'

function isEncryptEnabled () {
  if (guardModel().hasGuardMail()) return true
  return false
}

function isPGPMail (mail) {
  return isEncryptedMail(mail) || isSignedMail(mail)
}

function isEncryptedMail (mail) {
  if (mail.security_info && mail.security_info.encrypted) return true
  if (mail.content_type && mail.content_type.indexOf('multipart/encrypted') > -1) return (true)
  try {
    if (mail.attachments && mail.attachments[0]) {
      if (mail.attachments[0].content.indexOf('---BEGIN PGP MESSAGE') > -1) {
        mail.PGPInline = true
        return (true)
      }
    }
  } catch (e) {
    console.log('error checking body' + e)
  }
  return (false)
}

function isDecrypted (data) {
  if (_.isArray(data)) {
    let found = false
    data.forEach(function (obj) {
      if (obj.security && obj.security.decrypted) found = true
    })
    return found
  }
  if (data.security && data.security.decrypted) return true

  return false
}

function isSignedMail (mail) {
  return mail.content_type === 'multipart/signed'
}

function getPGPInfo () {
  return $.when()
}

export { isPGPMail, isDecrypted, isEncryptedMail, isSignedMail, getPGPInfo, isEncryptEnabled }
