/**
 * All content on this website (including text, images, source
 * code and any other original works), unless otherwise noted,
 * is licensed under a Creative Commons License.
 *
 * http://creativecommons.org/licenses/by-nc-sa/2.5/
 *
 * Copyright (C) 2016-2020 OX Software GmbH
 */

import Backbone from '$/backbone'
import _ from '$/underscore'
import http from '$/io.ox/core/http'

function log (e, o) {
  const collection = http.log()
  collection.add(new Backbone.Model(e).set({
    params: o.params,
    data: o.data,
    index: collection.length,
    timestamp: _.now(),
    url: o.url
  }))
}

function fail (r, url, params, data) {
  let errorText = r.statusText
  if (r.status === 406 || r.status === 500) {
    errorText = r.status + ': ' + errorText + ' - ' + r.responseText
    r.status = undefined
  }
  const e = {
    error: errorText,
    status: r.status
  }
  const o = {
    params: params,
    url: url,
    data: data
  }
  log(e, o)
}

function slow (took, url, params, data) {
  const e = { error: 'Took: ' + (Math.round(took / 100) / 10) + 's', status: 200, took: took }
  const o = {
    params: params,
    url: url,
    data: data
  }
  log(e, o)
}

export default {
  fail: fail,
  slow: slow
}
