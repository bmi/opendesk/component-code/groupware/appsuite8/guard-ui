/**
 * All content on this website (including text, images, source
 * code and any other original works), unless otherwise noted,
 * is licensed under a Creative Commons License.
 *
 * http://creativecommons.org/licenses/by-nc-sa/2.5/
 *
 * Copyright (C) 2016-2020 OX Software GmbH
 */

import $ from '$/jquery'
import guardModel from '@/io.ox/guard/core/guardModel'
import '@/io.ox/guard/core/style.scss'
import gt from 'gettext'

// Compare two el1 to el2 values whenever el1 changes.  If hint
// has value, then color and text if different
function passwordCompare (el1, el2, hint) {
  const el1ToCompare = el1.getForCompare ? el1.getForCompare() : el1 // Check if password View
  const el2ToCompare = el2.getForCompare ? el2.getForCompare() : el2
  $(el1ToCompare).on('input', function () {
    $(el1ToCompare).removeClass('oxguard_badpass')
    if ($(el1ToCompare).length >= 1) {
      el2ToCompare.removeClass('oxguard_warnpass').removeClass('oxguard_badpass').removeClass('oxguard_goodpass') // remove color markup of the first password box
      if ($(el1ToCompare).val() !== $(el2ToCompare).val()) {
        $(el1ToCompare).addClass('oxguard_badpass')
        if (hint !== undefined) {
          hint.removeClass('oxguard_hint_bad').removeClass('oxguard_hint_good').removeClass('oxguard_hint_warn')
          hint.addClass('oxguard_hint_bad').text(gt('Passwords not equal'))
        }
      } else if (hint !== undefined) {
        hint.text('')
      }
    }
  })
}

// Add event to check password strength of element
function passwordCheck (element, hint) {
  const checkElement = element.getForCompare ? element.getForCompare() : element
  $(checkElement).on('input', function () {
    check.call(this, hint)
  })
}

function check (hint) {
  let min = guardModel().getSettings().min_password_length
  let len = guardModel().getSettings().password_length
  if (min === undefined || len === undefined) {
    min = 6
    len = 10
  }
  if (len <= min) len = min + 1
  const el = $(this)
  const val = el.val()
  el.removeClass('oxguard_warnpass').removeClass('oxguard_badpass').removeClass('oxguard_goodpass')
  if (hint !== undefined) {
    hint.removeClass('oxguard_hint_bad').removeClass('oxguard_hint_good').removeClass('oxguard_hint_warn')
  }
  if (val.length === 0) {
    return
  }
  if (val.length < min) {
    el.addClass('oxguard_badpass')
    if (hint !== undefined) {
      hint.addClass('oxguard_hint_bad').text(gt('Too short'))
    }
    return
  }
  const regex = /(?=^.{8,}$)(?=.*\d)(?=.*[!@#$%^&*]+)(?![.\n])(?=.*[A-Z])(?=.*[a-z]).*$/
  if (val.match(regex)) {
    el.addClass('oxguard_goodpass')
    if (hint !== undefined) {
      hint.addClass('oxguard_hint_good').text(gt('Strong'))
    }
    return
  }
  el.addClass('oxguard_warnpass')
  if (hint !== undefined) {
    hint.addClass('oxguard_hint_warn').text(gt('Weak'))
  }
}

export default {
  passwordCheck: passwordCheck,
  passwordCompare: passwordCompare
}
