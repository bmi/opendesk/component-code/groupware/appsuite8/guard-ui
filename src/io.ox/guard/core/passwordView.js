/**
 * All content on this website (including text, images, source
 * code and any other original works), unless otherwise noted,
 * is licensed under a Creative Commons License.
 *
 * http://creativecommons.org/licenses/by-nc-sa/2.5/
 *
 * @author Greg Hill <greg.hill@open-xchange.com>
 *
 * Copyright (C) 2016-2020 OX Software GmbH
 */

import Backbone from '$/backbone'
import mini from '$/io.ox/backbone/mini-views/common'
import yell from '$/io.ox/core/yell'
import guardModel from '@/io.ox/guard/core/guardModel'
import gt from 'gettext'
import $ from '$/jquery'
import '@/io.ox/guard/core/style.scss'

const PasswordModel = Backbone.Model.extend({
  validate: function (attr) {
    if (!this.get('validate')) return
    let min = guardModel().getSettings().min_password_length
    let len = guardModel().getSettings().password_length
    if (min === undefined || len === undefined) {
      min = 6
      len = 10
    }
    if (len <= min) len = min + 1
    const password = attr[this.get('id')]
    if (password.length < min) {
      return (gt('Password not long enough.  Needs to be at least %i characters long', min))
    }
  }
})

const PasswordView = Backbone.View.extend({
  initialize: function (options) {
    const o = this.options = options || {}
    this.model = new PasswordModel()
    if (o.validate) {
      this.model.set('validate', true)
    }
    this.listenTo(this.model, 'invalid', this.invalid)
    this.listenTo(this.model, 'valid', this.valid)
    this.isValid = true
    if (!o.id && !o.name) {
      o.id = this.model.get('cid')
    }
    this.name = o.name ? o.name : o.id
    this.id = o.id ? o.id : o.name
    this.model.set('id', o.id)
    this.o = o
  },
  invalid: function (model, message) {
    yell('error', message)
    this.isValid = false
  },
  valid: function () {
    this.isValid = true
  },
  dispose: function () {
    this.stopListening()
    this.model = null
  },
  render: function () {
    const opt = {
      name: this.name,
      id: this.id,
      model: this.model
    }

    if (!this.o.autocomplete) {
      opt.autocomplete = false // default to no autocomplete
    }

    this.view = new mini.PasswordView(opt).render()
    if (this.o.class) {
      this.view.$el.addClass(this.o.class)
    }
    return this.view
  },
  getProtected: function () {
    this.render()
    let noAutofill = ''
    for (let i = 0; i < 2; i++) {
      noAutofill += '<input type="password" data-lpignore="true" style="position:absolute;left:-10000px" value="v' + i + '" aria-hidden="true" tabindex="-1">'
    }
    return ($(noAutofill).add(this.view.$el))
  },
  getInputField: function () {
    this.render()
    return this.view.$el
  },
  getForCompare: function () {
    return this.view.$el
  },
  getValue: function () {
    this.view.$el.trigger('change')
    if (this.isValid) return this.model.get(this.id)
    return undefined
  }
})

export default {
  view: PasswordView
}
