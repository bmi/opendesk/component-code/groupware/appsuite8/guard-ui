/**
 * This work is provided under the terms of the CREATIVE COMMONS PUBLIC
 * LICENSE. This work is protected by copyright and/or other applicable
 * law. Any use of the work other than as authorized under this license
 * or copyright law is prohibited.
 *
 * http://creativecommons.org/licenses/by-nc-sa/2.5/
 *
 * © 2016 OX Software GmbH, Germany. info@open-xchange.com
 *
 * @author Greg Hill <greg.hill@open-xchange.com>
 */

import Backbone from '$/backbone'
import $ from '$/jquery'
import keysAPI from '@/io.ox/guard/api/keys'
import ModalDialog from '$/io.ox/backbone/views/modal'
import core from '@/io.ox/guard/oxguard_core'
import gt from 'gettext'
import '@/io.ox/guard/settings/style.scss'

// callers
// - settings -> guard -> Autocrypt keys -> delete

const POINT = 'oxguard/core/confirmView'

function open (text, data) {
  return new ModalDialog({
    async: true,
    point: POINT,
    title: gt('Confirmation Required'),
    width: 400,
    model: new Backbone.Model(data)
  })
    .build(function () {
      this.$body.append(
        $('<span>').append(text)
      )
    })
    .addButton({ label: gt('Delete'), action: 'delete' })
    .addCancelButton()
    .on('delete', function () {
      const data = this.model.toJSON()
      core.metrics('settings', 'delete-public-key')
      return keysAPI.deleteExternalPublicKey(data.id, data.keyType).always(function () {
        this.idle()
        this.close()
      }.bind(this))
    })
    .open()
}

export default {
  open: open
}
