/**
 * All content on this website (including text, images, source
 * code and any other original works), unless otherwise noted,
 * is licensed under a Creative Commons License.
 *
 * http://creativecommons.org/licenses/by-nc-sa/2.5/
 *
 * Copyright (C) 2016-2020 OX Software GmbH
 */

import $ from '$/jquery'
import _ from '$/underscore'
import gt from 'gettext'
import ext from '$/io.ox/core/extensions'
import util from '@/io.ox/guard/util'
import capabilities from '$/io.ox/core/capabilities'
import guardModel from '@/io.ox/guard/core/guardModel'
import { settings } from '@/io.ox/guard/settings/guardSettings'
import { updateAdvanced } from '@/io.ox/guard/settings/pane'

if (util.isGuardConfigured() || capabilities.has('guard-mail') || capabilities.has('guard-drive')) {
  // Add settings for encryption to the Settings Page

  const highlightAdvance = (container, selector) => {
    settings.set('advanced', true)
    updateAdvanced()
    return container.find(selector)
  }

  ext.point('io.ox/settings/pane/general/security').extend({
    id: 'io.ox/guard',
    title: guardModel().getName(),
    index: 600,
    loadSettingPane: true,
    load: () => Promise.all([
      import('@/io.ox/guard/settings/pane'),
      settings.ensureData()
    ]),

    searchTerms: [{
      title: gt('Default to send encrypted when composing email'),
      highlight: '#oxguard-defaultencrypted'
    }, {
      title: gt('Default adding signature to outgoing mails'),
      highlight: '#oxguard-defaultsign'
    }, {
      title: gt('Default to using PGP inline for new mails'),
      highlight: container => highlightAdvance(container, '#oxguard-defaultinline')
    }, {
      title: gt('Remember password default'),
      highlight: '#settings-defRemember'
    }, {
      title: gt('Password management'),
      highlight: '#passwordDiv'
    }, {
      title: gt('Change password'),
      highlight: '#changePassword'
    }, {
      title: gt('Reset password'),
      highlight: '#resetPassword',
      requires: () => guardModel().get('recoveryAvail') && !guardModel().get('new')
    }, {
      title: gt('Set email address for reset'),
      highlight: '#changeEmail',
      requires: () => guardModel().get('recoveryAvail') && !guardModel().get('new')
    }, {
      title: gt('Delete password recovery'),
      highlight: container => highlightAdvance(container, '#deleteRecovery'),
      requires: () => guardModel().get('recoveryAvail') && !guardModel().get('new') && !guardModel().getSettings().noDeleteRecovery && !util.isGuest()
    }, {
      title: gt('Mail Cache'),
      highlight: container => highlightAdvance(container, '#mailcache legend')
    }, {
      title: gt('Download my public key'),
      highlight: container => highlightAdvance(container, 'button[name="downloadPublic"]')
    }, {
      title: gt('Your keys'),
      highlight: container => highlightAdvance(container, 'button[name="yourKeys"]')
    }, {
      title: gt('Public keys of recipients'),
      highlight: container => highlightAdvance(container, 'button[name="recipients"]'),
      requires: () => util.hasGuardMailCapability()
    }, {
      title: gt('Autocrypt Keys'),
      highlight: container => highlightAdvance(container, 'button[name="autoCrypt"]'),
      requires: () => util.hasGuardMailCapability() && util.autoCryptEnabled()
    }, {
      title: gt('Search incoming emails for keys in header'),
      highlight: container => highlightAdvance(container, '#oxguard-enableautocrypt')
    }, {
      title: gt('Import autocrypt keys without asking'),
      highlight: container => highlightAdvance(container, '#oxguard-autoaddautocrypt')
    }, {
      title: gt('Autocrypt transfer keys'),
      highlight: container => highlightAdvance(container, 'button[name="transferKeys"]')
    }]
  })

  ext.point('io.ox/settings/help/mapping').extend({
    id: 'guardHelp',
    index: 200,
    list: function () {
      _.extend(this, {
        'virtual/settings/oxguard': {
          base: 'help',
          target: 'ox.appsuite.user.sect.guard.settings.html'
        }
      })
    }
  })
}

if (!util.isGuardLoaded()) {
  const removeIfNotNeeded = function () {
    if (!util.isGuardConfigured() && !(capabilities.has('guard-mail') || capabilities.has('guard-drive'))) {
      $('[data-model="virtual/settings/oxguard"]').remove()
    }
  }
  util.addOnLoaded(removeIfNotNeeded)
}
