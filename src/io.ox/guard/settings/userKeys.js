/**
 * All content on this website (including text, images, source
 * code and any other original works), unless otherwise noted,
 * is licensed under a Creative Commons License.
 *
 * http://creativecommons.org/licenses/by-nc-sa/2.5/
 *
 * @author Greg Hill <greg.hill@open-xchange.com>
 * Copyright (C) 2016-2020 OX Software GmbH
 */

import $ from '$/jquery'
import _ from '$/underscore'
import '@/io.ox/guard/oxguard_core'
import keysAPI from '@/io.ox/guard/api/keys'
import guardModel from '@/io.ox/guard/core/guardModel'
import gt from 'gettext'
import '@/io.ox/guard/pgp/style.scss'
import icons from '@/io.ox/guard/core/icons'

// Generate list of public keys
// - settings -> guard -> Your Keys

let userKeyData = null

// register refresh listeners
keysAPI.on('create', updateUserKeys)
keysAPI.on('delete revoke', function (e, keyId) {
  // check if delete key is part of the table and refresh
  const node = $('#userkeytable' + ' [data-id="' + keyId + '"]')
  if (node.length) updateUserKeys()
})

// List of users private keys
function userKeys () {
  const pubdiv = $('<div class="userKeyDiv"/>')
  const tableHeader = $('<b>').text(gt('Your key list'))
  const refresh = $('<a href="#" style="margin-left:10px;">')
    .attr('title', gt('Refresh'))
    .attr('id', 'refreshuserkeys')
    .append(icons.getIcon({ name: 'arrow-repeat' }))
    .click(function (e) {
      $('#refreshuserkeys').addClass('animate-spin')
      updateUserKeyTable().done(function () {
        $('#refreshuserkeys').removeClass('animate-spin')
      })
      e.preventDefault()
    })
  const addKey = $('<a href="#" style="margin-left:10px;">')
    .attr('title', gt('Add user key'))
    .append(icons.getIcon({ name: 'plus', className: 'og_larger' }).attr('id', 'adduserkeys'))
    .click(function (e) {
      import('@/io.ox/guard/settings//views/uploadPrivateView').then(({ default: view }) => {
        view.open()
      })
      e.preventDefault()
    })
  tableHeader.append(refresh).append(addKey)
  const tablediv = $('<div class="keytable">')
  const table = $('<table id="userkeytable">')
  pubdiv.append(tableHeader.append(tablediv.append(table)))
  updateUserKeys()
  return (pubdiv)
}

function updateUserKeys () {
  const def = $.Deferred()
  $('#refreshuserkeys').addClass('animate-spin')
  updateUserKeyTable().done(function () {
    $('#refreshuserkeys').removeClass('animate-spin')
    def.resolve()
  })
  return def
}

function getCheck () {
  return icons.getIcon({ name: 'check-square' })
}

function updateUserKeyTable () {
  const deferred = $.Deferred()
  userKeyData = {}
  // Send the request to the oxguard server for the encrypted content key to decrypt
  keysAPI.getKeys().done(function (keyrings) {
    if (keyrings.length === 0) {
      // Keys deleted, no keys, open new key dialog
      guardModel().setAuth('No Key')
      import('@/io.ox/guard/settings//views/uploadPrivateView').then(({ default: view }) => {
        view.open()
      })
    }
    if (keyrings.length !== 0) {
      // create table and headers
      const table = $('<table id="userkeytable">').append(
        [gt('Key ID'), gt('Private key'), gt('Current'), gt('Details'), gt('Delete'), gt('Download'), gt('Edit')].map(function (label) {
          return $('<th>').text(label)
        })
      )

      _.each(keyrings, function (keyring, index) {
        _.each(keyring.publicRing.keys, function (data) {
          // Save in cache
          userKeyData[data.id] = data

          // add row for each key
          table.append(
            $('<tr>')
              .attr({ 'data-id': data.id })
              .addClass(data.revoked ? 'revoked' : '')
              .addClass(data.expired ? 'expired' : '')
              .append(
                // fingerprint
                $('<td data-name="fingerprint">').append(
                  data.masterKey ? $() : icons.getIcon({ name: 'arrow-return-right' }), // style="padding-right:7px; vertical-align:bottom;">'),
                  $('<span>').text(data._short)
                ),

                // private key: check mark
                $('<td data-name="privatekey">').append(
                  data.hasPrivateKey ? getCheck().attr('title', gt('Has private key')) : $()
                ),

                // current: check mark OR checkbox
                $('<td data-name="current">').append(
                  keyring.current
                    ? getCheck().attr('title', gt('Current'))
                    : $('<input type="checkbox" class="makecurrent">').attr('title', gt('Make current'))
                ),

                // action: details
                $('<td data-name="details">').append(
                  $('<a class="userpgpdetail" href="#">')
                    .attr('title', gt('Details'))
                    .append(icons.getIcon({ name: 'search' }))
                ),

                // action: delete
                $('<td data-name="delete">').append(
                  !data.masterKey
                    ? $()
                    : $('<a href="#" class="delPriv">').attr({
                      master: data.masterKey,
                      private: !!data.hasPrivateKey,
                      title: gt('Delete')
                    })
                      .append(icons.getIcon({ name: 'trash' }))
                ),

                // action: download
                $('<td data-name="download">').append(
                  !data.masterKey
                    ? $()
                    : $('<a href="#" class="keydownload">').attr({
                      title: gt('Download')
                    })
                      .append(icons.getIcon({ name: 'download' }))
                ),

                // action: edit
                $('<td data-name="edit">').append(
                  !data.masterKey || !data.hasPrivateKey
                    ? $()
                    : $('<a href="#" class="keyedit">').attr({
                      title: gt('Edit IDs')
                    })
                      .append(icons.getIcon({ name: 'pencil' }))
                )
              )
          )
        })
        // add spacer
        if (index + 1 < keyrings.length) {
          table.append(
            $('<tr class="table-spacer">').append($('<td>').append($('<hr>')))
          )
        }
      })
      $('#userkeytable').replaceWith(table)

      table.on('click', '.userpgpdetail', function (e) {
        const id = $(this).closest('tr').attr('data-id')
        showLocalDetail(id)
        e.preventDefault()
      })

      table.on('click', '.keydownload', function (e) {
        const id = $(this).closest('tr').attr('data-id')
        import('@/io.ox/guard/settings//views/downloadPrivateView').then(({ default: view }) => {
          view.open(id)
        })
        e.preventDefault()
      })

      table.on('click', '.makecurrent', function (e) {
        const id = $(this).closest('tr').attr('data-id')
        makeCurrent(id)
        e.preventDefault()
      })

      table.on('click', '.delPriv', function (e) {
        const id = $(this).closest('tr').attr('data-id')
        const master = $(this).attr('master')
        const privateAvail = $(this).attr('private')
        if (privateAvail === 'false') {
          // If no private avail, we are just deleting a public key
          import('@/io.ox/guard/settings//views/deletePublicView').then(({ default: view }) => {
            view.open(id, privateAvail)
          })
        } else {
          import('@/io.ox/guard/settings//views/deletePrivateView').then(({ default: view }) => {
            view.open(id, master)
          })
        }
        e.preventDefault()
      })

      table.on('click', '.keyedit', function (e) {
        const id = $(this).closest('tr').attr('data-id')
        import('@/io.ox/guard/settings//views/editUserIdView').then(({ default: view }) => {
          view.open(id)
        })
        e.preventDefault()
      })
    } else {
      $('#userkeytable').replaceWith('<table id="userkeytable"><tr><td>' + gt('No Keys Found') + '</td></tr></table>')
    }

    deferred.resolve('OK')
    hideWait()
  })
    .fail(function () {
      hideWait()
    })
  return deferred
}

function showLocalDetail (id) {
  const data = getCacheData(id)
  import('@/io.ox/guard/settings//views/privateDetailView').then(({ default: view }) => {
    view.open(data)
  })
}

function makeCurrent (id) {
  wait()
  // Send the request to the oxguard server for the encrypted content key to decrypt
  keysAPI.setCurrentKey(id)
    .done(updateUserKeyTable)
    .fail(function (e) {
      import('$/io.ox/core/notifications').then(({ default: notify }) => {
        notify.yell('error', e.responseText)
        console.log(e)
        hideWait()
      })
    })
}

function wait () {
  $('#og_wait').show()
}

function hideWait () {
  $('#og_wait').hide()
}

function getCacheData (id) {
  const data = userKeyData[id]
  return (data)
}

export default {
  userKeys: userKeys
}
