/**
 * This work is provided under the terms of the CREATIVE COMMONS PUBLIC
 * LICENSE. This work is protected by copyright and/or other applicable
 * law. Any use of the work other than as authorized under this license
 * or copyright law is prohibited.
 *
 * http://creativecommons.org/licenses/by-nc-sa/2.5/
 *
 * © 2016 OX Software GmbH, Germany. info@open-xchange.com
 *
 * @author Greg Hill <greg.hill@open-xchange.com>
 */

import $ from '$/jquery'
import Backbone from '$/backbone'
import ModalDialog from '$/io.ox/backbone/views/modal'
import { compactSelect } from '$/io.ox/core/settings/util'
import keysAPI from '@/io.ox/guard/api/keys'
import PasswordView from '@/io.ox/guard/core/passwordView'
import yell from '$/io.ox/core/yell'
import core from '@/io.ox/guard/oxguard_core'
import guardModel from '@/io.ox/guard/core/guardModel'
import gt from 'gettext'

function openModalDialog (keyid) {
  return new ModalDialog({
    async: true,
    point: 'io.ox/guard/settings/revokePrivate',
    title: gt('Revoke Private Key'),
    width: 640,
    model: new Backbone.Model({ revokereason: 'NO_REASON' }),
    enter: 'cancel',
    focus: '#revokepass'
  })
    .extend({
      warning: function () {
        this.$body.append(
          $('<p class="bg-warning bg-padding">').append(
            gt('This will advise people not to use this key. You can still use the key to decode existing items.')
          )
        )
      },
      password: function () {
        this.$body.append(
          $('<label for="revokepass">').text(
            gt('Your %s password', guardModel().getName())
          ),
          new PasswordView.view({ id: 'revokepass' }).getProtected()
        )
      },
      revoke: function () {
        this.$body.append(
          compactSelect('revokereason', gt('Revocation reason'), this.model, [
            { label: gt('No Reason'), value: 'NO_REASON' },
            { label: gt('Key superseded'), value: 'KEY_SUPERSEDED' },
            { label: gt('Key compromised'), value: 'KEY_COMPROMISED' },
            { label: gt('Key retired'), value: 'KEY_RETIRED' },
            { label: gt('User no longer valid'), value: 'USER_NO_LONGER_VALID' }
          ])
        )
      }

    })
    .addButton({ label: gt('Revoke'), action: 'revoke', className: 'btn-default' })
    .on('revoke', function () {
      const dialog = this
      const password = $('#revokepass').val()
      const reason = this.model.get('revokereason')
      const invalid = password.length <= 1

      if (invalid) {
        this.idle()
        return yell('info', gt('Please enter your password'))
      }

      core.metrics('settings', 'revoke-private-key')
      return keysAPI.revoke(keyid, { password: password, reason: reason }).then(function () {
        dialog.close()
      }, function () {
        dialog.idle()
      })
    })
    .addCloseButton()
    .open()
}

export default {
  open: openModalDialog
}
