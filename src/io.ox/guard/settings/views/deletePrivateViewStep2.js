/**
 * This work is provided under the terms of the CREATIVE COMMONS PUBLIC
 * LICENSE. This work is protected by copyright and/or other applicable
 * law. Any use of the work other than as authorized under this license
 * or copyright law is prohibited.
 *
 * http://creativecommons.org/licenses/by-nc-sa/2.5/
 *
 * © 2016 OX Software GmbH, Germany. info@open-xchange.com
 *
 * @author Greg Hill <greg.hill@open-xchange.com>
 */

import $ from '$/jquery'
import ModalDialog from '$/io.ox/backbone/views/modal'
import yell from '$/io.ox/core/yell'
import '@/io.ox/guard/oxguard_core'
import guardModel from '@/io.ox/guard/core/guardModel'
import PasswordView from '@/io.ox/guard/core/passwordView'
import keysAPI from '@/io.ox/guard/api/keys'
import gt from 'gettext'
import '@/io.ox/guard/settings//style.scss'

function openModalDialog (keyid) {
  return new ModalDialog({
    async: true,
    point: 'io.ox/guard/settings/deletePrivateStep2',
    title: gt('Delete Private Key'),
    width: 640,
    focus: '#deletepass'
  })
    .extend({
      warning: function () {
        this.$body.append(
          $('<p class="bg-danger bg-padding">').append(
            gt('You will no longer be able to decode any items that were encrypted with this key!')
          )
        )
      },
      password: function () {
        this.$body.append(
          $('<label for="deletepass">').text(
            gt('Your %s password', guardModel().getName())
          ),
          new PasswordView.view({ id: 'deletepass' }).getProtected()
        )
      }
    })
    .addButton({ label: gt('Delete'), action: 'delete', className: 'btn-default' })
    .on('delete', function () {
      const dialog = this
      const password = $('#deletepass').val()
      const invalid = password.length <= 1

      if (invalid) {
        this.idle()
        return yell('info', gt('Please enter your password'))
      }

      return keysAPI.delete(keyid, { password: password }).then(function () {
        dialog.close()
      }, function () {
        dialog.idle()
      })
    })
    .addCloseButton()
    .open()
}

export default {
  open: openModalDialog
}
