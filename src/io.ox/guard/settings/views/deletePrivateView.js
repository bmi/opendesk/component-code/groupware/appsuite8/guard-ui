/**
 * This work is provided under the terms of the CREATIVE COMMONS PUBLIC
 * LICENSE. This work is protected by copyright and/or other applicable
 * law. Any use of the work other than as authorized under this license
 * or copyright law is prohibited.
 *
 * http://creativecommons.org/licenses/by-nc-sa/2.5/
 *
 * © 2016 OX Software GmbH, Germany. info@open-xchange.com
 *
 * @author Greg Hill <greg.hill@open-xchange.com>
 */

import $ from '$/jquery'
import guardModel from '@/io.ox/guard/core/guardModel'
import ModalDialog from '$/io.ox/backbone/views/modal'
import gt from 'gettext'
import '@/io.ox/guard/settings//style.scss'

function openModalDialog (keyid, isMaster) {
  const allowDelete = !guardModel().getSettings().noDeletePrivate && isMaster

  return new ModalDialog({
    async: true,
    point: 'io.ox/guard/settings/deletePrivate',
    id: 'delete-private-key',
    title: allowDelete ? gt('Delete Private Key') : gt('Revoke Private Key'),
    width: 640,
    keyid: keyid
  })
    .addCloseButton()
    .extend({
      options: function () {
        if (!allowDelete) return
        this.$body.append(
          $('<p>').text(gt('There are two options if you no longer want to use this key.'))
        )
      },
      revoke: function () {
        // text
        this.$body.append(
          $('<p>').append(
            allowDelete ? $('<h5>').text(gt('Revoke the key')) : $(),
            $('<span>').text(gt('This advises others that you no longer want this key used. You can continue to decode any encrypted data using the key.')),
            $.txt(' '),
            $('<i>').text(gt('Recommended'))
          )
        )
        // button
        const dialog = this
        this.addButton({ label: gt('Revoke'), action: 'revoke', className: 'btn-default' })
          .on('revoke', function () {
            import('@/io.ox/guard/settings/views/revokePrivateView').then(({ default: revoke }) => {
              revoke.open(this.options.keyid)
              dialog.close()
            })
          })
      },
      delete: function () {
        if (!allowDelete) return

        // text
        this.$body.append(
          $('<p>').append(
            $('<h5>').text(gt('Deleting the key')),
            $('<span>').text(gt('Deleting the private key will render all encrypted items unreadable.  You will not be able to decode emails or files that were enrypted with this key. This will delete this key and any sub-keys. This cannot be undone.'))
          )
        )
        const dialog = this
        // button
        this.addAlternativeButton({ label: gt('Delete'), action: 'delete' })
          .on('delete', function () {
            import('@/io.ox/guard/settings/views/deletePrivateViewStep2').then(({ default: view }) => {
              view.open(this.options.keyid)
              dialog.close()
            })
          })
      }
    })
    .open()
}

export default {
  open: openModalDialog
}
