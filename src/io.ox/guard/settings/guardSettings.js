import { Settings } from '$/io.ox/core/settings'

export const defaults = {
  cryptoProvider: undefined,
  enableAutocrypt: true,
  wipe: 'false'
}

export const settings = new Settings('oxguard', () => defaults)
