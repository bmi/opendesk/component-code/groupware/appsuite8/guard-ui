/**
 * All content on this website (including text, images, source
 * code and any other original works), unless otherwise noted,
 * is licensed under a Creative Commons License.
 *
 * http://creativecommons.org/licenses/by-nc-sa/2.5/
 *
 * @author Greg Hill <greg.hill@open-xchange.com>
 *
 * Copyright (C) 2016-2020 OX Software GmbH
 */

import $ from '$/jquery'
import _ from '$/underscore'
import ox from '$/ox'
import ext from '$/io.ox/core/extensions'
import ExtensibleView from '$/io.ox/backbone/views/extensible'
import { checkbox, compactSelect, fieldset, header } from '$/io.ox/core/settings/util'
import guardUtil from '@/io.ox/guard/util'
import mini from '$/io.ox/backbone/mini-views'
import notifications from '$/io.ox/core/notifications'
import core from '@/io.ox/guard/oxguard_core'
import guardModel from '@/io.ox/guard/core/guardModel'
import gt from 'gettext'
import { settings } from '@/io.ox/guard/settings/guardSettings'
import '@/io.ox/guard/settings/style.scss'
import openSettings, { closeSettings } from '$/io.ox/settings/util'

ext.point('io.ox/guard/settings/detail').extend({
  index: 100,
  id: 'oxguardsettings',
  draw: function (baton) {
    const node = this
    if (guardUtil.isGuardLoaded()) {
      drawGuardSettings(node, baton)
    } else {
      guardUtil.addOnLoaded(function () {
        drawGuardSettings(node, baton)
      })
    }
    if (_.url.hash('guardReset') === 'true') {
      _.url.hash({ guardReset: null })
      import('@/io.ox/guard/core/createKeys').then(({ default: keys }) => {
        keys.createKeys(gt('This will create new keys to use for future encryption.'))
          .done(function () {
            $('#refreshuserkeys').click()
          })
          .fail(function (e) {
            if (e === 'cancel') {
              return
            }
            core.notifyError(e.responseText)
          })
      })
    }
  }
})

let INDEX = 0

function drawGuardSettings (node, baton) {
  guardUtil.hasSetupDone()
    .done(function () {
      node.empty()
      node.addClass('guard-settings')
      node.append(
        new ExtensibleView({ point: 'io.ox/guard/settings/detail/view', model: settings })
          .inject({
            getPasswordOptions: function () {
              return ([
                { label: gt('Ask each time'), value: 0 },
                { label: gt('10 minutes'), value: 10 },
                { label: gt('20 minutes'), value: 20 },
                { label: gt('30 minutes'), value: 30 },
                { label: gt('1 hour'), value: 60 },
                { label: gt('2 hours'), value: 120 },
                { label: gt('Session'), value: 99999 }
              ])
            }
          })
          .build(function () {
            this.listenTo(settings, 'change', function () {
              settings.saveAndYell().then(
                function ok () {
                  //
                },
                function fail () {
                  notifications.yell('error', gt('Could not save settings'))
                }
              )
            })
          })
          .render().$el
      )
    })
    .fail(function () {
      import('$/io.ox/core/capabilities').then(({ default: capabilities }) => {
        if (capabilities.has('guard-mail') || capabilities.has('guard-drive')) {
          node.append(setupGuard(node, baton))
        }
      })
    })
}

ext.point('io.ox/guard/settings/detail/view').extend(
  {
    id: 'header',
    index: INDEX += 100,
    render: function () {
      this.$el.addClass('io-ox-guard-settings').append(
        header(gt('%s security settings', guardModel().getName()))
      )
    }
  },
  {
    id: 'defaults',
    index: INDEX += 100,
    render: function () {
      if (guardUtil.isGuest() || !guardUtil.hasGuardMailCapability()) return // Guests don't set defaults for sending
      this.$el.append(
        $('<div class="guardDefaults">').append(
          fieldset(
            gt('Defaults'),
            // default to encrypt outgoing
            checkbox('defaultEncrypted', gt('Default to send encrypted when composing email'), settings),
            // default add signature
            checkbox('defaultSign', gt('Default adding signature to outgoing mails'), settings),
            // inline
            checkbox('defaultInline', gt('Default to using PGP inline for new mails'), settings).addClass('guardAdvanced').css('display', settings.get('advanced') ? 'block' : 'none'),
            compactSelect('defRemember', gt('Remember password default'), settings, this.getPasswordOptions())
          )
        )
      )
    }
  },
  {
    id: 'passwordManagement',
    index: INDEX += 100,
    render: function (baton) {
      this.$el.append(
        fieldset(
          gt('Password management'),
          baton.branch('buttons', null, $('<div class="form-group buttons" id="passwordDiv">'))
        )
      )
    }
  },
  {
    id: 'Advanced',
    index: INDEX += 100,
    render: function () {
      const advancedBox = checkbox('advanced', gt('Show advanced settings'), settings)
      advancedBox.on('change', function () {
        updateAdvanced()
      })
      this.$el.append(
        fieldset(
          gt('Advanced'),
          // default to encrypt outgoing
          advancedBox
        )
      )
      this.$el.append(
        $('<div class="guardAdvanced" style="display:' + (settings.get('advanced') ? 'block' : 'none') + '">')
          .append(drawAdvanced()).append(drawAutocrypt().append(drawVersion())))
      updateAdvanced()
    }
  }
)
//
// Buttons
//
ext.point('io.ox/guard/settings/detail/view/buttons').extend(
  {
    id: 'changePassword',
    index: 100,
    render: function (baton) {
      this.append(
        $('<button type="button" class="btn btn-default" id="changePassword">')
          .text(gt('Change password'))
          .on('click', openDialog)
      )

      function openDialog () {
        import('@/io.ox/guard/settings/views/changePasswordView').then(({ default: view }) => {
          const node = $('.guard-settings')
          view.open(node, baton)
        })
      }
    }
  },
  {
    id: 'resetPassword',
    index: 200,
    render: function () {
      // check enabled
      if (!guardModel().get('recoveryAvail') || guardModel().get('new')) { // If no recovery avail, then don't display reset button
        return
      }
      this.append(
        $('<button type="button" class="btn btn-default" id="resetPassword">')
          .text(gt('Reset password'))
          .on('click', openDialog)
      )

      function openDialog () {
        import('@/io.ox/guard/settings/views/resetPasswordView').then(({ default: view }) => {
          view.open()
        })
      }
    }
  },
  {
    id: 'changeEmail',
    index: 300,
    render: function () {
      // check enabled
      if (!guardModel().get('recoveryAvail') || guardModel().get('new')) { // If no recovery avail, then don't display reset button
        return
      }
      this.append(
        $('<button type="button" class="btn btn-default" id="changeEmail">')
          .text(gt('Set email address for reset'))
          .on('click', openDialog)
      )

      function openDialog () {
        import('@/io.ox/guard/settings/views/secondaryEmailView').then(({ default: view }) => {
          view.open()
        })
      }
    }
  },
  {
    id: 'deleteRecovery',
    index: 400,
    render: function (baton) {
      // check enabled
      if (!guardModel().get('recoveryAvail') || guardModel().get('new') || guardModel().getSettings().noDeleteRecovery || guardUtil.isGuest()) { // If no recovery avail, then don't display reset button
        return
      }
      const button = $('<button type="button" class="btn btn-default guardAdvanced" id="deleteRecovery">')
      // #. Option to remove the possibility of recovering your password
        .text(gt('Delete password recovery'))
        .on('click', openDialog)

      if (!settings.get('advanced')) { // Only visible if advanced checked
        button.css('display', 'none')
      }

      this.append(button)
      function openDialog () {
        import('@/io.ox/guard/settings/views/deleteRecoveryView').then(({ default: view }) => {
          const node = $('.guard-settings')
          view.open(node, baton)
        })
      }
    }
  }
)

function drawAdvanced () {
  const div = $('<div class="guardAdvanced" id="advancedDiv">')
  $('#deleteRecovery').show()
  const buttons = $('<div class="form-group buttons">')
  const downloadPublic = $('<div>')
  const downloadPublicButton = $('<button type="button" class="btn btn-default" name="downloadPublic">')
    .text(gt('Download my public key'))
    .on('click', downloadPublicKey)
  downloadPublic.append(downloadPublicButton)
  const userKeys = $('<button type="button" class="btn btn-default" name="yourKeys">')
    .text(gt('Your keys'))
    .on('click', openUserKeysDialog)
  function openUserKeysDialog () {
    import('@/io.ox/guard/settings/views/userKeysView').then(({ default: view }) => {
      view.open()
    })
  }
  let recipients = ''
  let autoCrypt = ''
  if (guardUtil.hasGuardMailCapability()) { // Guests don't manage Public keys for recipients
    recipients = $('<button type="button" class="btn btn-default" name="recipients">')
      .text(gt('Public keys of recipients'))
      .on('click', openDialog)
    if (guardUtil.autoCryptEnabled()) {
      autoCrypt = $('<button type="button" class="btn btn-default" name="autoCrypt">')
        .text(gt('Autocrypt Keys'))
        .on('click', openAutoCryptDialog)
    }
  }
  function openDialog () {
    import('@/io.ox/guard/settings/views/publicKeysView').then(({ default: view }) => {
      view.open({
        title: gt('Public keys of recipients')
      })
    })
  }
  function openAutoCryptDialog () {
    import('@/io.ox/guard/settings/views/autoCryptKeysView').then(({ default: view }) => {
      view.open()
    })
  }
  div.append(drawWipeOptions)
  div.append(fieldset(
    gt('Keys'), buttons.append(downloadPublic, userKeys, recipients, autoCrypt)
  ))
  return div
}

function drawWipeOptions () {
  function getOptions () {
    return [{ label: gt('Keep emails decrypted until browser closed'), value: false },
      { label: gt('Do not keep emails decrypted.'), value: true }]
  }

  return fieldset(
    gt('Mail Cache'),
    new mini.CustomRadioView({ list: getOptions(), name: 'wipe', model: settings }).render().$el
  ).attr('id', 'mailcache')
}

function drawAutocrypt () {
  if (!guardUtil.autoCryptEnabled()) return
  const div = $('<div class="autocryptSettings guardAdvanced">')
  const transfer = $('<button type="button" class="btn btn-default" name="transferKeys">')
    .text(gt('Autocrypt transfer keys'))
    .on('click', openTransferDialog)
  function openTransferDialog () {
    import('@/io.ox/guard/pgp/autocrypt/autocryptTransferView').then(({ default: view }) => {
      view.open()
    })
  }
  const autoAdd = checkbox('autoAddAutocrypt', gt('Import autocrypt keys without asking'), settings)
  div.append(fieldset(gt('Autocrypt'),
    checkbox('enableAutocrypt', gt('Search incoming emails for keys in header'), settings),
    autoAdd, transfer))
  settings.on('change:enableAutocrypt', function () {
    checkEnabled(autoAdd)
  })
  checkEnabled(autoAdd)
  return div
}

function checkEnabled (block) {
  const enabled = settings.get('enableAutocrypt')
  block.find('[name="autoAddAutocrypt"]').attr('disabled', !enabled)
  block.find('label').toggleClass('disabled', !enabled)
}

function downloadPublicKey () {
  import('@/io.ox/guard/api/keys').then(({ default: keysAPI }) => {
    // current key
    keysAPI.downloadAsFile({ keyType: 'public' })
  })
}

function drawVersion () {
  if (_.device('small')) return ('')
  const div = $('<div class="guardVersion">')
  fetch(ox.abs + ox.root + '/meta').then(async function (response) {
    if (response.ok) {
      const data = await response.json()
      data.forEach((s) => {
        if (s.id === 'guard-ui') {
          div.append('UI: ' + s.version + ' ' + s.buildDate)
        }
      })
      div.append('<br>build: ' + guardModel().get('server'))
    }
  })
  return (div)
}

export function updateAdvanced () {
  if (!settings.get('advanced')) {
    $('.guardAdvanced').hide()
    return
  }
  $('.guardAdvanced').show()
  $('#advancedDiv').html(drawAdvanced())
}

// Convert old Guard server settings to local.  Should be able to remove version after 7.10.  Remove call from oxguard/register
function initSettings (data) {
  settings.set('defaultEncrypted', data.pgpdefault === true)
  settings.set('defaultSign', data.pgpsign === true)
  settings.set('defaultInline', data.inline === true)
  settings.save()
}

function setupGuard (node, baton) {
  const div = $('<div>').addClass('io-ox-guard-settings')
  const passheader = header(gt('%s security settings', guardModel().getName()))
  const instructions = $('<div>')
  // #. product name
  instructions.append('<p>' + gt('In order to start with %s security, you must set up a password for your secured files.', guardModel().getName()) + '</p>')
  const startButton = $('<button type="button" class="btn btn-default guard-start-button">' + gt('Start') + '</button>')
  startButton.click(function () {
    import('@/io.ox/guard/core/createKeys').then(({ default: keys }) => {
      closeSettings()
      keys.createKeysWizard()
        .done(function (result) {
          openSettings('virtual/settings/io.ox/guard')
          ext.point('io.ox/guard/settings/detail').invoke('draw', node, baton)
        })
    })
  })
  return (div.append(passheader).append(instructions).append(startButton))
}

export default initSettings
