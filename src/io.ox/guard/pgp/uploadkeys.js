/**
 * This work is provided under the terms of the CREATIVE COMMONS PUBLIC
 * LICENSE. This work is protected by copyright and/or other applicable
 * law. Any use of the work other than as authorized under this license
 * or copyright law is prohibited.
 *
 * http://creativecommons.org/licenses/by-nc-sa/2.5/
 *
 * © 2016 OX Software GmbH, Germany. info@open-xchange.com
 *
 * @author Greg Hill <greg.hill@open-xchange.com>
 */

import $ from '$/jquery'
import ext from '$/io.ox/core/extensions'
import ox from '$/ox'
import yell from '$/io.ox/core/yell'
import ModalView from '$/io.ox/backbone/views/modal'
import notify from '$/io.ox/core/notifications'
import PasswordView from '@/io.ox/guard/core/passwordView'
import guardModel from '@/io.ox/guard/core/guardModel'
import core from '@/io.ox/guard/oxguard_core'
import gt from 'gettext'

let uploading = false

// Uploads to personal key chain
// upload(files) for uploading public key only
// upload (file, private, pgp Password, Guard password) for uploading private key
function upload (files, priv, pgp, guard) {
  const deferred = $.Deferred()
  if (uploading) return $.Deferred().reject()
  const formData = new FormData()
  for (let l = 0; l < files.length; l++) {
    if (!validFileType(files[l])) {
      deferred.reject()
      return deferred
    }
    if (files[l] !== undefined) { formData.append('key' + l, files[l]) }
  }
  if (priv) {
    formData.append('keyPassword', pgp)
    formData.append('newPassword', guard)
  }
  uploading = true
  const url = ox.apiRoot + '/oxguard/keys?action=upload&respondWithJSON=true&session=' + ox.session
  $.ajax({
    url: url,
    type: 'POST',
    data: formData,
    processData: false, // tell jQuery not to process the data
    contentType: false, // tell jQuery not to set contentType
    success: function (data) {
      if (data === null) data = ''
      if (core.checkJsonOK(data)) {
        try {
          if (data.data && data.data.keyRings) {
            const keys = data.data.keyRings
            let added = gt('Added keys: \r\n')
            for (let i = 0; i < keys.length; i++) {
              const key = keys[i]
              if (key.privateRing && key.privateRing.keys) {
                const privateKeys = key.privateRing.keys
                added = added + gt('Private keys: ')
                for (let p = 0; p < privateKeys.length; p++) {
                  const pKey = privateKeys[p]
                  added = added + pKey.fingerPrint + '\r\n'
                  for (let d = 0; d < pKey.userIds.length; d++) {
                    added = added + pKey.userIds[d] + ' '
                  }
                  added = added + '\r\n'
                }
              } else {
                const publicKeys = key.publicRing.keys
                added = added + gt('Public keys: ')
                for (let k = 0; k < publicKeys.length; k++) {
                  const pubKey = publicKeys[k]
                  added = added + pubKey.fingerPrint + '\r\n'
                  for (let j = 0; j < pubKey.userIds.length; j++) {
                    added = added + pubKey.userIds[j] + ' '
                  }
                  added = added + '\r\n'
                }
              }
            }
            notify.yell('success', added)
          }
        } catch (e) {
          console.log(e)
        }
        $('input[name="publicKey"]').val('')
        deferred.resolve('OK')
      } else {
        deferred.reject(data.error)
      }
      uploading = false
    },
    error: function (XMLHttpRequest) {
      $('input[name="publicKey"]').val('')
      uploading = false
      const resp = XMLHttpRequest.responseText.trim()
      switch (resp.trim()) {
        case 'Bad UID':
          notify.yell('error', gt('Failed to upload') + '\r\n' + gt('Invalid Email Address'))
          break
        case 'Bad ID':
          notify.yell('error', gt('The public key must have your primary email as a user ID'))
          break
        default:
          notify.yell('error', gt('Failed to upload') + ' ' + XMLHttpRequest.responseText.trim())
          break
      }
      deferred.reject(XMLHttpRequest.responseText.trim())
    }
  })
  return (deferred)
}

// Verify ascii data contains key before upload
function validateKeyData (key) {
  if (!/(-----BEGIN PGP PRIVATE KEY BLOCK-----|-----BEGIN PGP PUBLIC KEY BLOCK-----)[\d\w\r\n:. +/=]+(-----END PGP PRIVATE KEY BLOCK-----|-----END PGP PUBLIC KEY BLOCK-----)/i.test(key)) {
    notify.yell('error', gt('Does not appear to be a valid key file type'))
    return false
  }
  return true
}

// Upload ascii key data
function uploadPublicKey (key) {
  const deferred = $.Deferred()
  if (!validateKeyData(key)) {
    deferred.reject()
    return deferred
  }
  const formData = new FormData()
  formData.append('key', key)
  const url = ox.apiRoot + '/oxguard/keys?action=upload&session=' + ox.session
  $.ajax({
    url: url,
    type: 'POST',
    data: formData,
    processData: false, // tell jQuery not to process the data
    contentType: false, // tell jQuery not to set contentType
    success: function (data) {
      if (core.checkJsonOK(data)) {
        notify.yell('success', gt('Keys uploaded'))
        $('#refreshuserkeys').click()
        deferred.resolve()
      }
    },
    error: function (XMLHttpRequest) {
      $('input[name="publicKey"]').val('')
      uploading = false
      const resp = XMLHttpRequest.responseText.trim()
      switch (resp.trim()) {
        case 'Bad UID':
          notify.yell('error', gt('Failed to upload') + '\r\n' + gt('Invalid Email Address'))
          break
        case 'Bad ID':
          notify.yell('error', gt('The public key must have your primary email as a user ID'))
          break
        default:
          notify.yell('error', gt('Failed to upload') + ' ' + XMLHttpRequest.responseText.trim())
          break
      }
      deferred.reject(XMLHttpRequest.responseText.trim())
    }
  })
  return deferred
}

function uploadExternalKey (files) {
  const deferred = $.Deferred()
  if (uploading) return $.Deferred().reject()
  const formData = new FormData()
  for (let l = 0; l < files.length; l++) {
    if (files[l] !== undefined) {
      if (!validFileType(files[l])) {
        deferred.reject()
        return deferred
      }
      formData.append('key' + l, files[l])
    }
  }
  uploading = true
  const url = ox.apiRoot + '/oxguard/keys?action=uploadExternalPublicKey&respondWithJSON=true&session=' + ox.session
  $.ajax({
    url: url,
    type: 'POST',
    data: formData,
    processData: false, // tell jQuery not to process the data
    contentType: false, // tell jQuery not to set contentType
    success: function (data) {
      if (data === null) data = ''
      if (core.checkJsonOK(data)) {
        deferred.resolve('ok')
      } else {
        deferred.reject()
      }
      uploading = false
    },
    error: function (XMLHttpRequest) {
      $('input[name="publicKey"]').val('')
      deferred.reject(XMLHttpRequest.responseText.trim())
      uploading = false
    }
  })
  return (deferred)
}

function validFileType (file) {
  if (!/^.*\.(txt|pgp|gpg|asc)$/i.test(file.name)) {
    console.error('bad file type ' + file.name)
    notify.yell('error', gt('Does not appear to be a valid key file type'))
    return false
  }
  return true
}

function createPasswordPrompt (files, def) {
  return new ModalView({
    async: true,
    point: 'oxguard/pgp/uploadKeysPasswordPrompt',
    title: gt('Upload Private Keys'),
    width: 640,
    enter: 'ok',
    focus: '#pgppassword'
  })
    .inject({
      doUpload: function () {
        return doUploadPrivate(files)
      }
    })
    .build(function () {
    })
    .addButton({ label: gt('OK'), action: 'ok' })
    .addCancelButton()
    .on('ok', function () {
      const dialog = this
      this.doUpload().done(function () {
        def.resolve()
        dialog.close()
      })
        .fail(function (e) {
          yell(e)
          $('#uploaderror').text(e)
          dialog.idle()
        })
    })
    .open()
}

ext.point('oxguard/pgp/uploadKeysPasswordPrompt').extend(
  {
    index: 100,
    id: 'passwordPrompts',
    render: function () {
      this.$body.append(
        createPrompts()
      )
    }
  })

function createPrompts () {
  const explain = $('<div><p>' + gt('Please enter passwords for the upload') + '</p>')
  const passdiv = $('<div>').addClass('row-fluid')
  const newogpassword = new PasswordView.view({ id: 'guardpassword', class: 'password_prompt', validate: true }).getProtected()
  const newogpassword2 = new PasswordView.view({ id: 'guardpassword2', class: 'password_prompt', validate: true }).getProtected()
  const pgpPassword = new PasswordView.view({ id: 'pgppassword', class: 'password_prompt' }).getProtected()
  const hint = $('<td>')
  const pw1 = $('<tr>').append($('<td class="pw">').append('<label for="guardpassword">' + gt('Enter new password for the key:') + '</label>')).append($('<td>').append(newogpassword))
  const pw2 = $('<tr>').append($('<td class="pw">').append('<label for="guardpassword2">' + gt('Confirm Password:') + '</label>')).append($('<td>').append(newogpassword2))
  const noSaveWorkAround = $('<input style="display:none" type="text" name="dontremember"/><input style="display:none" type="password" name="dontrememberpass"/>')
  const table = $('<table class="og_password_prompt"/>')
    .append(noSaveWorkAround)
    .append($('<tr>').append($('<td class="pw"><label for="pgppassword">' + gt('Private key password for the key you are uploading:') + '</label></td>')).append($('<td>').append(pgpPassword)))
    .append(pw1)
    .append(pw2)
    .append($('<tr>').append('<td>').append(hint))
  const errormessage = $('<span id="uploaderror" style="color:red;"></span>')
  passdiv.append(table)
  import('@/io.ox/guard/core/passwords').then(({ default: pass }) => {
    pass.passwordCheck(newogpassword, hint)
    pass.passwordCompare(newogpassword2, newogpassword, hint)
  })
  return explain.append(passdiv).append(errormessage)
}

function doUploadPrivate (files) {
  const deferred = $.Deferred()
  const pgp = $('#pgppassword').val()
  const guard = $('#guardpassword').val()
  const guard2 = $('#guardpassword2').val()
  if (guard !== guard2) {
    deferred.reject(gt('Passwords not equal'))
    return deferred
  }
  if (guardModel().getSettings().min_password_length !== undefined) { // Check min password length
    if (guard.trim().length < guardModel().getSettings().min_password_length) {
      deferred.reject(gt('New password must be at least %s characters long', guardModel().getSettings().min_password_length))
      return deferred
    }
  } else if (guard.trim().length < 2) { // If no min defined, make sure at least 2 characters there
    uploadPrivate(files)
    return deferred
  }
  upload(files, true, pgp, guard)
    .done(function () {
      deferred.resolve('OK')
    })
    .fail(function (e) {
      console.log(e)
      deferred.reject(e ? e.trim() : '')
      handleFail(e, notify)
    })
  return deferred
}

function uploadPrivate (files) {
  const deferred = $.Deferred()
  for (let i = 0; i < files.length; i++) {
    if (!validFileType(files[i])) {
      deferred.reject()
      return deferred
    }
  }
  createPasswordPrompt(files, deferred)
  return deferred
}

function handleFail (e, notify) {
  if (e === 'duplicate') {
    notify.yell('error', gt('This key already exists'))
    return
  }
  if (e === 'Bad pgp') {
    notify.yell('error', gt('Bad PGP Password'))
    return
  }
  if (e === 'Bad password') {
    notify.yell('error', gt('Bad Password'))
    return
  }
  if (e === 'Bad ID') {
    notify.yell('error', gt('The private key must have your primary email as a user ID'))
    return
  }
  if (e === 'Bad Merge Password') {
    notify.yell('error', gt('Must use same password as existing key to merge'))
    return
  }
  notify.yell('error', gt('Problem uploading Private Key ') + '\r\n' + e)
}

export default {
  upload: upload,
  uploadPrivate: uploadPrivate,
  uploadPublicKey: uploadPublicKey,
  uploadExternalKey: uploadExternalKey
}
