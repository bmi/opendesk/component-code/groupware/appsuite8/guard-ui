/**
 * This work is provided under the terms of the CREATIVE COMMONS PUBLIC
 * LICENSE. This work is protected by copyright and/or other applicable
 * law. Any use of the work other than as authorized under this license
 * or copyright law is prohibited.
 *
 * http://creativecommons.org/licenses/by-nc-sa/2.5/
 *
 * © 2016 OX Software GmbH, Germany. info@open-xchange.com
 *
 * @author Greg Hill <greg.hill@open-xchange.com>
 */

import Backbone from '$/backbone'
import $ from '$/jquery'
import ext from '$/io.ox/core/extensions'
import ModalView from '$/io.ox/backbone/views/modal'
import gt from 'gettext'
import '@/io.ox/guard/pgp/autocrypt/style.scss'

const POINT = 'oxguard/autoCrypt/transferDone'
let INDEX = 0

function open (passcode) {
  openModalDialog(passcode)
}

function openModalDialog (passcode) {
  const dialog = new ModalView({
    async: true,
    point: POINT,
    title: gt('Autocrypt Transfer Keys'),
    id: 'autoCryptStartTransfer',
    width: 640,
    model: new Backbone.Model({ passcode: passcode }),
    enter: 'done'
  })
    .addButton({ label: gt('Done'), action: 'done' })
    .on('done', function () {
      dialog.close()
    })
    .on('copy', function () {
      dialog.idle()
      const passText = $('#passcodeText')
      passText.show()
      passText.focus()
      passText.select()
      try {
        document.execCommand('copy')
        passText.hide()
      } catch (e) {
        console.error(e)
      }
    })
  if (document.queryCommandSupported('copy')) {
    dialog.addAlternativeButton({ label: gt('Copy to clipboard'), action: 'copy' })
  }
  dialog.open()
  return dialog
}

function getCodeDiv (code) {
  const itemDiv = $('<div class="autocryptpasscode">')
  const items = code.split('-')
  for (let i = 0; i < items.length; i++) {
    itemDiv.append(items[i])
    if (i < items.length - 1) {
      itemDiv.append('<span class="passcodespacer">-<span>')
    }
    if ((i + 1) % 3 === 0) {
      itemDiv.append('<br>')
    }
  }
  return itemDiv
}

ext.point(POINT).extend(
  {
    index: INDEX += 100,
    id: 'Header',
    render: function () {
      const div = $('<div class="autocryptTransfDone">')
        .append(gt('An Email with the secure information required to transfer your keys has been sent.'))
      this.$body.append(
        div
      )
    }
  },
  {
    index: INDEX += 100,
    id: 'code',
    render: function (baton) {
      const codeDiv = $('<div>').addClass('autocryptTransfDone')
      const p = $('<p>').append(gt('To protect your keys, the email was encrypted with the following passcode.  You will need this code in order to import the keys into another client (including the dashes).'))
      const div = getCodeDiv(baton.model.get('passcode'))
      const hiddenTextArea = $('<textarea style="display:none" id="passcodeText">').val(baton.model.get('passcode'))
      this.$body.append(codeDiv.append(p).append(div).append(hiddenTextArea))
    }
  })

export default {
  open: open
}
