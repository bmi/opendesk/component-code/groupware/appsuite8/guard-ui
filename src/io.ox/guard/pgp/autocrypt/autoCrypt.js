/* All content on this website (including text, images, source
 * code and any other original works), unless otherwise noted,
 * is licensed under a Creative Commons License.
 *
 * http://creativecommons.org/licenses/by-nc-sa/2.5/
 *
 * Copyright (C) 2016-2020 OX Software GmbH
 */

import $ from '$/jquery'
import _ from '$/underscore'
import ox from '$/ox'
import http from '@/io.ox/guard/core/og_http'
import core from '@/io.ox/guard/oxguard_core'
import yell from '$/io.ox/core/yell'
import gt from 'gettext'
import { settings } from '@/io.ox/guard/settings/guardSettings'
import guardUtil from '@/io.ox/guard/util'

const checking = []

function getAddFlag () {
  return (settings.get('autoAddAutocrypt') === true || settings.get('advanced') !== true)
}

// Only check a specific header string once
function alreadyChecking (header) {
  const id = getHash(header)
  if (checking.includes(id)) return true // Already checking
  checking.push(id)
}

// Get a hash of the header string.  Used to track if previously checked
function getHash (header) {
  let hash = 0; let i; let chr
  if (header.length === 0) return hash
  for (i = 0; i < header.length; i++) {
    chr = header.charCodeAt(i)
    hash = ((hash << 5) - hash) + chr
    hash |= 0 // Convert to 32bit integer
  }
  return hash
}

// Check if an array of emails (to, from, cc) contain an email address
function contains (array, email) {
  let found = false
  array.forEach(function (t) {
    if (t && t.length > 1) {
      if (t[1].indexOf(email) > -1) found = true
    }
  })
  return found
}

// Check if the header contains email address from one of the recipients
function checkEmailAddress (header, model, multiple) {
  const headers = header.split(';')
  if (!headers || headers.length === 0) return false
  const addr = headers[0]
  const addrSplit = addr.split('=')
  if (addrSplit.length < 2) return false
  const email = addrSplit[1]
  const from = model.get('from')
  if (_.isArray(from)) {
    if (contains(from, email)) return true
  } else if (from.indexOf(email) > -1) return true
  if (multiple) {
    return contains(model.get('to'), email) || contains(model.get('cc'), email)
  }
  return false
}

function doCheckHeader (header, model, confirmed, multiple) {
  const def = $.Deferred()
  if (!guardUtil.autoCryptEnabled()) return def.reject('disabled') // system wide disabled
  if (!settings.get('enableAutocrypt')) return def.reject('disabled') // User disabled
  if (!confirmed && alreadyChecking(header)) return def.reject('repeat') // already checking this header id
  if (!checkEmailAddress(header, model, multiple)) return def.reject('bad header')
  const data = {
    header: header,
    from: model.get('from'),
    date: model.get('sent_date')
  }
  let params = ''
  if (getAddFlag() || confirmed) {
    params = '&add=true' + (confirmed ? '&confirmed=true' : '')
  }
  http.doPut(ox.apiRoot + '/oxguard/keys?action=putautocrypt', params, data)
    .done(function (result) {
      if (result.error) {
        console.error(result.error)
        return
      }
      const data = result.data
      if (!data.added && data.new) {
        import('@/io.ox/guard/pgp/autocrypt/autocryptPromptView').then(({ default: view }) => {
          view.open(model, data, header, def, multiple)
        })
        return
      }
      def.resolve()

      if (confirmed) {
        yell('success', gt('Key imported'))
      }
    })
    .fail(function (e) {
      console.log(e)
      def.reject()
    })
  return def
}

function loopGossipHeaders (headers, index, model, confirmed) {
  if (index < headers.length) {
    doCheckHeader(headers[index], model, confirmed, true)
      .done(function () {
        loopGossipHeaders(headers, ++index, model, confirmed)
      })
      .fail(function (reason) {
        if (reason) { // If cancel all, terminate loop
          return
        }
        loopGossipHeaders(headers, ++index, model, confirmed)
      })
  }
}

function transferKeys (password) {
  const def = $.Deferred()
  const params = ''
  const data = {
    password: password
  }
  http.post(ox.apiRoot + '/oxguard/keys?action=autocrypttransfer', params, data)
    .done(function (result) {
      def.resolve(result)
    })
  return def
}

const autocrypt = {
  perform: function (header, model, confirmed) {
    return doCheckHeader(header, model, confirmed)
  },
  checkGossip: function (model, confirmed) {
    const headers = model.get('headers')['Autocrypt-Gossip']
    loopGossipHeaders(headers, 0, model, confirmed)
  },
  check: function (model, confirmed) {
    const header = model.get('headers').Autocrypt
    return doCheckHeader(header, model, confirmed)
  },
  verify: function (e) {
    e.preventDefault()
    const id = $(e.target).parent().attr('ids')
    const verified = $(e.target).parent().attr('verified') === 'true'
    const params = '&keyids=' + encodeURI(id) +
            '&session=' + ox.session + '&verified=' + (verified ? 'false' : 'true') // invert verified
    $.ajax({
      url: ox.apiRoot + '/oxguard/keys?action=verifyAutoCryptKey' + params,
      type: 'PUT',
      success: function (data) {
        core.checkJsonOK(data)
        $('.refreshkeys').click()
      }
    })
      .fail(function (data) {
        console.log(data)
      })
  },
  doStart: function (att) {
    import('@/io.ox/guard/pgp/autocrypt/autocryptStartupView').then(({ default: view }) => {
      view.open(att)
    })
  },
  transferKeys: transferKeys
}

export default autocrypt
