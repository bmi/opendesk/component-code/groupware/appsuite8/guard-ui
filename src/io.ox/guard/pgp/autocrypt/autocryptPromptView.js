/**
 * This work is provided under the terms of the CREATIVE COMMONS PUBLIC
 * LICENSE. This work is protected by copyright and/or other applicable
 * law. Any use of the work other than as authorized under this license
 * or copyright law is prohibited.
 *
 * http://creativecommons.org/licenses/by-nc-sa/2.5/
 *
 * © 2016 OX Software GmbH, Germany. info@open-xchange.com
 *
 * @author Greg Hill <greg.hill@open-xchange.com>
 */

import Backbone from '$/backbone'
import $ from '$/jquery'
import ext from '$/io.ox/core/extensions'
import ModalView from '$/io.ox/backbone/views/modal'
import keyDetails from '@/io.ox/guard/pgp/keyDetails'
import icons from '@/io.ox/guard/core/icons'
import gt from 'gettext'
import '@/io.ox/guard/pgp/autocrypt/style.scss'

const POINT = 'oxguard/autoCrypt/view'
let INDEX = 0

function open (model, keydata, header, def, multiple) {
  return openModalDialog(model, keydata, header, def, multiple)
}

function openModalDialog (model, keyData, header, def, multiple) {
  const dialog = new ModalView({
    async: true,
    point: POINT,
    title: gt('New key found'),
    id: 'autoCryptImport',
    width: 640,
    enter: 'add',
    model: new Backbone.Model({
      keyData: keyData,
      request: model,
      details: keyDetails.keyDetail(keyData.keys)
    })
  })
    .addButton({ label: gt('Add'), action: 'add' })
    .on('add', function () {
      import('@/io.ox/guard/pgp/autocrypt/autoCrypt').then(({ default: autocrypt }) => {
        autocrypt.perform(header, model, true).then(function () {
          def.resolve()
        })
      })
      this.close()
    })
    .on('cancel', function () {
      def.reject()
    })
    .addCancelButton()
  if (multiple) {
    dialog.addAlternativeButton({ label: gt('Cancel All'), action: 'cancelAll' })
      .on('cancelAll', function () {
        dialog.close()
        def.reject('all')
      })
  }
  dialog.open()
  return dialog
}

ext.point(POINT).extend(
  {
    index: INDEX += 100,
    id: 'acprompt',
    render: function (baton) {
      const div = $('<div class="autoheader">')
      div.append(gt('A new public key was sent for this sender.  Would you like to import it?'))
      div.append($('<div class="autoCryptSummary">').append(getSummary(baton.model)))
      const acDetails = $('<div class="ac-toggle-details">').click(showDetails)
      acDetails.append($('<a>').append(gt('Details')))
      acDetails.append(icons.getIcon({ name: 'arrow-down-up' }).css('margin-left', '7px'))
      this.$body.append(div.append(acDetails))
    }
  },
  {
    index: INDEX += 100,
    id: 'detail',
    render: function (baton) {
      const div = $('<div class="autocryptDiv">')
      this.$body.append(
        div.append(baton.model.get('details').div)
      )
    }
  })

function showDetails (e) {
  e.preventDefault()
  $('.ac-toggle-details').toggleClass('open')
  if ($('.ac-toggle-details').hasClass('open')) {
    $('.autocryptDiv').show()
  } else {
    $('.autocryptDiv').hide()
  }
}

function getSummary (model) {
  const details = model.get('details')
  const fingerprint = (details.fingerprints && details.fingerprints.length > 0)
    ? '<span class="fingerprint">' + details.fingerprints[0].substring(details.fingerprints[0].length - 8) + '</span>'
    : ''
  return details.keyids + fingerprint
}

export default {
  open: open
}
