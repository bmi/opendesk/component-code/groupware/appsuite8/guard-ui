/**
 * This work is provided under the terms of the CREATIVE COMMONS PUBLIC
 * LICENSE. This work is protected by copyright and/or other applicable
 * law. Any use of the work other than as authorized under this license
 * or copyright law is prohibited.
 *
 * http://creativecommons.org/licenses/by-nc-sa/2.5/
 *
 * © 2016 OX Software GmbH, Germany. info@open-xchange.com
 *
 * @author Greg Hill <greg.hill@open-xchange.com>
 */

import $ from '$/jquery'
import Backbone from '$/backbone'
import ext from '$/io.ox/core/extensions'
import ModalView from '$/io.ox/backbone/views/modal'
import keysAPI from '@/io.ox/guard/api/keys'
import core from '@/io.ox/guard/oxguard_core'
import gt from 'gettext'
import '@/io.ox/guard/pgp/style.scss'

const POINT = 'oxguard/pgp/signatureview'
let INDEX = 0

function open (keyid) {
  return keysAPI.getSignatures(keyid).then(function (data) {
    openModalDialog(data)
  })
}

function openModalDialog (data) {
  return new ModalView({
    async: false,
    point: POINT,
    title: gt('Public Keys Detail'),
    id: 'pkeyDetail',
    width: 500,
    model: new Backbone.Model({ data: data })
  })
    .addCloseButton()
    .open()
}

ext.point(POINT).extend(
  // Draw signature
  {
    index: INDEX += 100,
    id: 'detail',
    render: function (baton) {
      const data = baton.model.get('data')
      const table = $('<table class="signatureTable">')

      if (data.length === 0) return this.$body.append($('<b>').text(gt('No signatures')))

      // #. Table headers for the ID "signed", the level of the "certification", and who it was "signed by"
      table.append($('<th>' + gt('ID Signed') + '</th><th>' + gt('Certification') + '</th><th>' + gt('Signed by') + '</th>'))
      for (let i = 0; i < data.length; i++) {
        const tr = $('<tr>')
        const td1 = $('<td>')

        if (data[i].image) {
          if (data[i].image !== undefined) {
            const image = $('<img alt="Embedded Image" src="' + data[i].image.imageData + '" style="max-height:50px;max-width:50px;"/>')
            td1.append(image)
          } else {
            // #. Type of object signed was an image
            data[i].ID = gt('Image') // Translate image response
            td1.append(data[i].ID)
          }
        } else if (data[i].userId) {
          td1.append($('<span>' + core.htmlSafe(data[i].userId) + '</span>'))
        }
        tr.append(td1)
        const td2 = $('<td>')
        const signatureType = $('<span>').attr('title', data[i].description).append(translate(data[i].signatureType))
        td2.append(signatureType)
        tr.append(td2)
        const td3 = $('<td>')
        if (data[i].keyMissing) {
          td3.append(gt('Missing public'))
        }
        if (data[i].issuer) {
          td3.append(core.htmlSafe(data[i].issuer))
        }
        tr.append(td3)
        table.append(tr)
      }

      this.$body.append(
        table
      )
    }
  }
)

function translate (result) {
  switch (result) {
    case 19:
      // #. In table, the key was "Positively" identified by the signer (verification level)
      return (gt('Positive'))
    case -1:
      // #. In table, the signature failed to be verified
      return (gt('Failed check'))
    case 0:
      // #. In table, there was no public key available to verify the signature
      return (gt('Missing public'))
    case 18:
      // #. In table, the key was "Casually" identified by the signer (verification level)
      return (gt('Casual'))
    case 32:
      // #. In table, the key was revoked with this signature
      return (gt('Revocation'))
    case 31:
      // #. In table, the key was "Directly" certified by the signer (verification level)
      return (gt('Direct cert'))
    case 16:
      // #. In table, the key had "Default certification" by the signer (verification level)
      return (gt('Default Certification'))
    case 40:
      // #. In table, the key was revoked with this signature
      return (gt('Revocation'))
    case 24:
      // #. In table, the key is a bound subkey
      return (gt('Subkey Binding'))
    case 25:
      // #. In table, primary key binding
      return (gt('Primary Binding'))
    default:
      return (result)
  }
}

export default {
  open: open
}
