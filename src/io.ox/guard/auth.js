/**
 * All content on this website (including text, images, source
 * code and any other original works), unless otherwise noted,
 * is licensed under a Creative Commons License.
 *
 * http://creativecommons.org/licenses/by-nc-sa/2.5/
 *
 * author Greg Hill <greg.hill@open-xchange.com>
 *
 * Copyright (C) 2016-2020 OX Software GmbH
 */

import ox from '$/ox'
import core from '@/io.ox/guard/oxguard_core'
import util from '@/io.ox/guard/util'
import guardModel from '@/io.ox/guard/core/guardModel'
import gt from 'gettext'
import $ from '$/jquery'
import _ from '$/underscore'

const auth_core = {}

// Perform auth against Guard server
// Return auth code if any
auth_core.authorize = function authorize (baton, o) {
  const options = _.extend({}, o)
  const def = $.Deferred()
  checkAuth(options, def)
  return def
}

function doAuthCheck (options, def) {
  if (options.forceRelogin) { // If we must relogin, show password prompt
    promptPassword(options, def)
  } else { // Else check if we have authentication already in session
    core.checkAuth(options)
      .done(function (data) {
        def.resolve(data.auth)
      })
      .fail(function () {
        promptPassword(options, def)
      })
  }
}
function checkAuth (options, def) {
  // If we have known auth code, then proceed without deferred action (needed for tabs)
  if (util.hasStoredPassword()) {
    doAuthCheck(options, def)
    return
  }
  ensureSetup().then(function () {
    doAuthCheck(options, def)
  }, def.reject)
}

// Display password prompt to authorize
function promptPassword (options, def) {
  const prompt = options.optPrompt ? options.optPrompt : gt('Enter %s security password', guardModel().getName())
  core.getPassword(prompt, true, options) // Get password
    .done(function (passdata) {
      if (passdata.duration > -1 || options.minSingleUse) {
        core.savePassword(passdata.password, passdata.duration)
          .done(function (data) {
            def.resolve(data.auth)
            if (_.isFunction(options.callback)) {
              options.callback(data.auth)
            }
          })
          .fail(function (data) {
            if (_.isFunction(options.failAuthCallback)) {
              options.failAuthCallback()
            }
            handleFail(data.auth || data.code)
            def.reject(data.auth, data)
          })
      } else {
        core.auth(ox.user_id, passdata.password)
          .done(function (data) {
            if (data.auth.length > 20 && !(/Error:/i.test(data.auth))) {
              if (_.isFunction(options.callback)) {
                options.callback(data.auth)
              }
              def.resolve(data.auth)
            } else {
              if (_.isFunction(options.failAuthCallback)) {
                options.failAuthCallback()
              }
              handleFail(data.auth || data.code)
              def.reject(data.auth, data)
            }
          })
      }
    })
    .fail(function () {
      def.reject('cancel')
    })
}

function ensureLoaded () {
  if (guardModel().isLoaded()) return $.when()
  const def = $.Deferred()
  let counter = 0
  var timer = setInterval(function () {
    if (guardModel().isLoaded()) {
      def.resolve()
      clearInterval(timer)
    }
    if (counter++ > 100) {
      def.reject()
      clearInterval(timer) // not loading
      console.error('Response timeout from Guard server')
    }
  }, 100)
  return def
}

function ensureSetup () {
  const def = $.Deferred()
  ensureLoaded().then(function () {
    if (!util.isGuardConfigured()) {
      import('@/io.ox/guard/core/createKeys').then(({ default: keys }) => {
        keys.createKeysWizard()
          .then(def.resolve, def.reject)
      })
      return
    }
    // If keys were set up by receiving email, possible user hasn't had prompt to create password.
    if (guardModel().needsPassword()) {
      import('@/io.ox/guard/core/tempPassword').then(({ default: tempPass }) => {
        const go = function () {
          def.resolve()
        }
        tempPass.createOxGuardPasswordPrompt(null, go)
      })
    } else {
      def.resolve()
    }
  }, def.reject)

  return def
}

function handleFail (auth) {
  switch (auth.toLowerCase()) {
    case 'bad password':
      core.notifyError(gt('Bad password'))
      return
    case 'lockout':
      core.notifyError(gt('Account Locked out'))
      return
    case 'grd-mw-0003':
      core.notifyError(gt('Bad password'))
      break
    case 'grd-mw-0005':
      core.notifyError(gt('Account locked out due to bad attempts.  Please try again later.'))
      break
    default:
      core.notifyError(gt('Unspecified error') + '\n' + auth)
  }
}

auth_core.handleFail = handleFail

auth_core.ensureSetup = ensureSetup

export default auth_core
