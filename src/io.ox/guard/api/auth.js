/**
 * This work is provided under the terms of the CREATIVE COMMONS PUBLIC
 * LICENSE. This work is protected by copyright and/or other applicable
 * law. Any use of the work other than as authorized under this license
 * or copyright law is prohibited.
 *
 * http://creativecommons.org/licenses/by-nc-sa/2.5/
 *
 * © 2016 OX Software GmbH, Germany. info@open-xchange.com
 *
 * @author Frank Paczynski <frank.paczynski@open-xchange.com>
 */

import Events from '$/io.ox/core/event'
import util from '@/io.ox/guard/api/util'
import http from '$/io.ox/core/http'
import guardModel from '@/io.ox/guard/core/guardModel'
import _ from '$/underscore'
import $ from '$/jquery'

// https://documentation.open-xchange.com/components/guard/2.10.3/#tag/auth

const encryptPasswords = util.encryptPasswords

// minutesValid
// -1: or single use
//  0: indefinite

const api = {

  // Performs a login to obtain an authentication token
  // optionally: saves the token in the user session for specified period of time (minutesValid))
  authAndSave: function (data) {
    return http.POST({
      module: 'guard-json',
      params: {
        action: 'auth-token',
        time: new Date().getTime()
      },
      contentType: 'application/json; charset=utf-8;',
      data: JSON.stringify(encryptPasswords(data))
    }).fail(util.showError)
  },

  // Checks with the middleware if a valid authentication token exists within the session
  check: function () {
    api.trigger('before:check')
    return http.GET({
      module: 'guard-json',
      params: { action: 'auth-token' }
    }).then(function (authdata) {
      let data = authdata
      if (_.isArray(authdata)) {
        data = authdata[0] // temporary fix for using 2.10.7 backend with 3.0
      }
      // TODO: most of this "what situation do we have here?" should be handled on the server side
      if (!data) return $.Deferred().reject('none')
      // error cases
      const invalid = data.minutesValid === undefined || !_.isNumber(data.minutesValid) || !data.auth
      const singleUse = data.minutesValid === -1
      if (singleUse) guardModel().clearAuth()
      if (invalid) return $.Deferred().reject('exp')
      // 99999: session
      api.trigger('success:check', data)
      return data
    }).fail(function (e) {
      api.trigger('fail:check', e)
      if (_.isObject(e)) util.showError(e)
    })
  },

  // reset token in the user's session
  resetToken: function () {
    // TODO: should passcode be nulled in general?
    guardModel().clearAuth()
    // destroy auth token
    return api.setToken('')
  },

  // Saves an existing authentication token (obtained from a login
  // response) in the user's session for specified period of time.
  setToken: function (auth, time) {
    return http.PUT({
      module: 'guard-json',
      params: { action: 'auth-token' },
      data: {
        auth: auth,
        minutesValid: time || -1
      }
    }).done(function (data) {
      return (data || {}).auth
    }).fail(util.showError)
  }
}

Events.extend(api)

export default api
