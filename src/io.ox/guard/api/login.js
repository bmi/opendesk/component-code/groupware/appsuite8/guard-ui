/**
 * This work is provided under the terms of the CREATIVE COMMONS PUBLIC
 * LICENSE. This work is protected by copyright and/or other applicable
 * law. Any use of the work other than as authorized under this license
 * or copyright law is prohibited.
 *
 * http://creativecommons.org/licenses/by-nc-sa/2.5/
 *
 * © 2016 OX Software GmbH, Germany. info@open-xchange.com
 *
 * @author Frank Paczynski <frank.paczynski@open-xchange.com>
 */

import Events from '$/io.ox/core/event'
import util from '@/io.ox/guard/api/util'
import http from '$/io.ox/core/http'
import $ from '$/jquery'

// https://documentation.open-xchange.com/components/guard/2.10.3/#tag/login

const encryptPasswords = util.encryptPasswords

const api = {

  // Performs a login to obtain an authentication token and various user specific settings.
  login: function (data, keyid) {
    return http.POST({
      module: 'oxguard/login',
      params: {
        action: 'login',
        keyid: keyid,
        time: new Date().getTime()
      },
      contentType: 'application/json; charset=utf-8;',
      processResponse: false,
      data: JSON.stringify(encryptPasswords(data))
    }).then(function (data) {
      if (data.error) return $.Deferred().reject(data.error)
      return data
    }).fail(util.showError)
  },

  // Performs a login to obtain an authentication token and various user specific settings.
  changePassword: function (data) {
    return http.POST({
      module: 'oxguard/login',
      params: {
        action: 'changepass'
      },
      contentType: 'application/json; charset=utf-8;',
      processResponse: false,
      data: JSON.stringify(data)
    }).fail(util.showError)
  }
}

Events.extend(api)

export default api
