/**
 * This work is provided under the terms of the CREATIVE COMMONS PUBLIC
 * LICENSE. This work is protected by copyright and/or other applicable
 * law. Any use of the work other than as authorized under this license
 * or copyright law is prohibited.
 *
 * http://creativecommons.org/licenses/by-nc-sa/2.5/
 *
 * © 2016 OX Software GmbH, Germany. info@open-xchange.com
 *
 * @author Frank Paczynski <frank.paczynski@open-xchange.com>
 */

import * as encr from '@/io.ox/guard/crypto/encr'
import _ from '$/underscore'
import util from '@/io.ox/guard/util'
import gt from 'gettext'
import guardModel from '@/io.ox/guard/core/guardModel'
import core from '@/io.ox/guard/oxguard_core'

let badCount = 0

function handleJsonError (error, errorResponse, baton) {
  errorResponse.errorMessage = gt('Error')
  errorResponse.retry = false
  if (error.code !== undefined) {
    switch (error.code) {
      case 'GRD-PGP-0005':
        if (guardModel().hasAuth()) { // If bad password based on auth, then clear auth and redo
          guardModel().clearAuth()
          if (baton) baton.view.redraw()
          return
        }
        errorResponse.errorMessage = gt('Unable to decrypt Email, incorrect password.')
        core.notifyError(gt('Bad password'))
        badCount++
        if (badCount > 2 && guardModel().get('recoveryAvail')) {
          errorResponse.errorMessage += ('<br/>' + gt('To reset or recover your password, click %s', '<a id="guardsettings" href="#">' + gt('Settings') + '</a>'))
        }
        errorResponse.retry = true
        break
      case 'NOSERVER':
        errorResponse.errorMessage = gt('Unable to contact encryption server. You will not be able to encrypt or decrypt items until this is resolved.  Please contact your adminstrator or try again later')
        errorResponse.retry = false
        break
      default:
        if (error.error !== undefined) {
          errorResponse.errorMessage = error.error
        }
    }
  }
}

const apiUtil = {
  encryptPasswords: function (data) {
    data = data || {}
    if (!guardModel().get('pubKey')) return data

    _.each(['password', 'encr_password', 'extrapass'], function (key) {
      // missing prop or empty value
      if (!(key in data) || !data[key]) return
      const encrypted = encr.cryptPass(data[key])
      if (!encrypted) return
      const target = 'e_' + key
      data[target] = encrypted
      delete data[key]
    })
    return data
  },

  format: util.format,

  showError: function (errorJson) {
    const errorResp = {}
    try {
      handleJsonError(errorJson, errorResp)
    } catch (e) {
      errorResp.errorMessage = gt('Unspecified error')
    }
    core.notifyError(gt(errorResp.errorMessage))
  }
}

export default apiUtil
