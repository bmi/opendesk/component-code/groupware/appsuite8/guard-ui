/**
 * This work is provided under the terms of the CREATIVE COMMONS PUBLIC
 * LICENSE. This work is protected by copyright and/or other applicable
 * law. Any use of the work other than as authorized under this license
 * or copyright law is prohibited.
 *
 * http://creativecommons.org/licenses/by-nc-sa/2.5/
 * © 2017 OX Software GmbH, Germany. info@open-xchange.com
 *
 * @author Greg Hill <greg.hill@open-xchange.com>
 */

/**
 * This resets a user's Guard account, referenced by userIndex
 * Changes the URL.  Will need redirect after calling (such as using the login command)
 */

const puppeteer = require('puppeteer'),
codecept = require('codeceptjs');

function getServerURL () {
    const config = codecept.config.get(),
        driver = config.helpers['WebDriver'] || config.helpers['Puppeteer'],
        ox = config.helpers['OpenXchange'],
        url = ox.serverURL || driver.url,
        pathArray = url.split('/'),
        protocol = pathArray[0],
        host = pathArray[2];
    return `${protocol}//${host}`;
}

async function launch(url) {

    const browser = await puppeteer.launch({ headless: true, args: ['--no-sandbox'] });
    const page = await browser.newPage();
    await page.goto(url);

    // Fetches page's content
    const title = await page.content();
    await browser.close();
}

module.exports  = async function (users) {
    
        var launchURL = getServerURL();
        if (launchURL.search('appsuite\\/?$') >= 0) launchURL = launchURL.substring(0, launchURL.search('appsuite\\/?$'));
        if (!/\/$/.test(launchURL)) launchURL += '/';
        launchURL += 'appsuite/';
        var promises = [];
        users.forEach(function (userData) {
            var user = userData.userdata;
            promises.push(launch(launchURL + 'api/oxguard/demo?action=reset&email=' + user.email1));
        });
    await Promise.all(promises);  // wait for all to complete
    return this;
    
    };