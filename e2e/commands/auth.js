/**
 * This work is provided under the terms of the CREATIVE COMMONS PUBLIC
 * LICENSE. This work is protected by copyright and/or other applicable
 * law. Any use of the work other than as authorized under this license
 * or copyright law is prohibited.
 *
 * http://creativecommons.org/licenses/by-nc-sa/2.5/
 * © 2017 OX Software GmbH, Germany. info@open-xchange.com
 *
 * @author Greg Hill <greg.hill@open-xchange.com>
 */

/**
 * Enter password into authorization box
 * @param userIndex {number} the users position in the global users array
 */
module.exports = function (o) {
  this.waitForVisible('#ogPassword')
  this.insertCryptPassword('#ogPassword', o)
  this.click('.btn[data-action="ok"]')
  return this
}
