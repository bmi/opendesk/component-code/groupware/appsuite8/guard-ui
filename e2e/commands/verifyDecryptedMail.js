/**
 * This work is provided under the terms of the CREATIVE COMMONS PUBLIC
 * LICENSE. This work is protected by copyright and/or other applicable
 * law. Any use of the work other than as authorized under this license
 * or copyright law is prohibited.
 *
 * http://creativecommons.org/licenses/by-nc-sa/2.5/
 * © 2017 OX Software GmbH, Germany. info@open-xchange.com
 *
 * @author Greg Hill <greg.hill@open-xchange.com>
 */

/**
 * Verifies the decrypted email data is as expected
 *
 */

const SELECTOR = require('../constants')

module.exports = async function (subject, data, signed) {
  this.waitForVisible(SELECTOR.DETAIL_SUBJECT)
  this.see(subject)
  this.waitForVisible('.oxguard_icon_fa') // Verify PGP icon
  if (signed) { // If signed, verify signed icon visible
    this.waitForVisible('.guard_signed')
  }
  this.waitForElement('.mail-detail-frame')
  within({ frame: '.mail-detail-frame' }, () => {
    this.waitForText(data, 3)
  })
}
