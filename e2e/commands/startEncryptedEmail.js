/**
 * This work is provided under the terms of the CREATIVE COMMONS PUBLIC
 * LICENSE. This work is protected by copyright and/or other applicable
 * law. Any use of the work other than as authorized under this license
 * or copyright law is prohibited.
 *
 * http://creativecommons.org/licenses/by-nc-sa/2.5/
 * © 2017 OX Software GmbH, Germany. info@open-xchange.com
 *
 * @author Greg Hill <greg.hill@open-xchange.com>
 */

/**
 * This sends a new email to recipient
 */

const SELECTOR = require('../constants.js')

module.exports = function (o) {
  const userdata = o.user.userdata
  // Open compose
  this.wait(2)
  this.click('New email')
  this.waitForVisible(SELECTOR.WINDOW_BUSY)
  this.waitForInvisible(SELECTOR.WINDOW_BUSY)
  this.waitForVisible(SELECTOR.EDITOR)
  this.click('.toggle-encryption')
  this.waitForVisible('#ogPassword')
  this.fillField('#ogPassword', userdata.password)
  this.click('.btn[data-action="ok"]')
}
