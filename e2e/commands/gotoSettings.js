/**
 * This work is provided under the terms of the CREATIVE COMMONS PUBLIC
 * LICENSE. This work is protected by copyright and/or other applicable
 * law. Any use of the work other than as authorized under this license
 * or copyright law is prohibited.
 *
 * http://creativecommons.org/licenses/by-nc-sa/2.5/
 * © 2017 OX Software GmbH, Germany. info@open-xchange.com
 *
 * @author Greg Hill <greg.hill@open-xchange.com>
 */

/**
 * This simplifies the input of passwords into input fields.
 * There is an array of users defined in the globals. If you want to fill in a password of such a user
 * you can simply use this function.
 * @param subsequent  boolean if this is a subsequent visit to settings, the security dropdown will already be open
 */
const { I } = inject()

module.exports = {

  locators: {
    tree: locate({ css: '.io-ox-settings-main .tree-container' }).as('Tree'),
    main: locate({ css: '.io-ox-settings-main .scrollable-pane' }).as('Main content')
  },

  waitForApp () {
    I.waitForElement(this.locators.tree)
    I.waitForElement(this.locators.main)
  },

  async open (folder) {
    I.click('#io-ox-topbar-settings-dropdown-icon button')
    I.click('#topbar-settings-dropdown a[data-name="settings-app"]')
    this.waitForApp()
    if (folder) {
      this.select(folder)
    }
  },

  close () {
    I.waitForVisible('.io-ox-settings-main .close-settings')
    I.click('.io-ox-settings-main .close-settings')
  },

  select (label) {
    I.waitForText(label, 5, this.locators.tree)
    I.click(locate('.folder-label').withText(label).inside(this.locators.tree))
    I.waitForVisible(locate(`~${label}`).inside(this.locators.main))
  },

  waitForSettingsSave (timeout = 7) {
    I.waitForResponse(response => response.url().includes('api/jslob') && response.request().method() === 'PUT', timeout)
  },

  async goToSettings (initial) {
    await this.open('Security')
    this.waitForApp()
    await this.select('Guard')
    this.waitForApp()
    I.wait(1)
  }
}
