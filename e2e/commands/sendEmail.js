/**
 * This work is provided under the terms of the CREATIVE COMMONS PUBLIC
 * LICENSE. This work is protected by copyright and/or other applicable
 * law. Any use of the work other than as authorized under this license
 * or copyright law is prohibited.
 *
 * http://creativecommons.org/licenses/by-nc-sa/2.5/
 * © 2017 OX Software GmbH, Germany. info@open-xchange.com
 *
 * @author Greg Hill <greg.hill@open-xchange.com>
 */

/**
 * This sends a new email to recipient
 */

const SELECTOR = require('../constants')

module.exports = function (o, subject, data, attachment, options) {
  const userdata = o.user.userdata
  options = options || {}

  const optionsDropdown = locate({ css: SELECTOR.MAIL_OPTIONS }).as('Options dropdown')

  // Open compose
  this.wait(2)
  this.click('New email')
  this.waitForVisible(SELECTOR.WINDOW_BUSY)
  this.waitForInvisible(SELECTOR.WINDOW_BUSY)
  this.waitForVisible(SELECTOR.EDITOR)

  // Mark as secure unless specified as non-encrypted
  if (!options.unencrypted) {
    this.seeElement('.io-ox-mail-compose-window .toggle-encryption')
    this.click('.io-ox-mail-compose-window .toggle-encryption')
    this.waitForVisible('#ogPassword')
    this.fillField('#ogPassword', userdata.password)
    this.click('.btn[data-action="ok"]')
    this.waitForVisible(SELECTOR.GUARD_LOCK)
    this.wait(1) // delay for possible auth
  }

  if (options.attachKey) {
    this.click(optionsDropdown)
    this.waitForText('Attach my public key', 10, '.dropdown.open .dropdown-menu')
    this.click('Attach my public key', '.dropdown.open .dropdown-menu')
    this.waitForElement('.file[title="public.asc"]')
  }

  if (options.sign) {
    this.click(optionsDropdown)
    this.waitForText('Sign email', 10, '.dropdown.open .dropdown-menu')
    this.click('Sign email', '.dropdown.open .dropdown-menu')
  }

  // Verify HTML
  this.click(optionsDropdown)
  this.click('HTML')
  this.waitForVisible('.io-ox-mail-compose .editor .tox-edit-area')

  if (options.inline) {
    this.wait(1)
    this.waitForVisible(SELECTOR.SECURITY_OPTIONS)
    this.click(SELECTOR.SECURITY_OPTIONS)
    this.waitForVisible('.dropdown.open a[data-name="PGPFormat"]')
    this.click('.dropdown.open a[data-value="inline"]')
    this.waitForVisible('.btn[data-action="ok"]')
    this.click('.btn[data-action="ok"]')
    this.waitForInvisible('.modal-backdrop')
  }
  const I = this
  // Insert test data
  let recipLocation = SELECTOR.TO_FIELD
  if (options.type) {
    if (options.type === 'cc') {
      I.click('CC')
      I.waitForVisible('[placeholder="CC"]')
      recipLocation = SELECTOR.CC
    }
    if (options.type === 'bcc') {
      I.click('BCC')
      I.waitForVisible('[placeholder="BCC"]')
      recipLocation = SELECTOR.BCC
    }
  }
  this.fillField(recipLocation, userdata.email1)
  this.fillField(SELECTOR.SUBJECT, subject)

  if (options.inline) { // Inline plaintext only
    I.fillField('.plain-text', data)
  } else {
    within({ frame: SELECTOR.EDITOR_IFRAME }, async () => {
      I.fillField(SELECTOR.EMAIL_BODY, data)
    })
  }

  // Attachments if any
  if (attachment) {
    this.attachFile('.composetoolbar input[type="file"]', attachment)
    this.wait(2)
  }

  // Verify key is next to recipient names
  if (!options.unencrypted) {
    this.waitForVisible(SELECTOR.TRUSTED_KEY)
  }
  if (options.startOnly) {
    return
  }

  this.click('Send')

  if (options.sign) {
    this.waitForVisible('#ogPassword')
    this.fillField('#ogPassword', userdata.password)
    this.click('.btn[data-action="ok"]')
  }
  this.waitForVisible('.io-ox-mail-window .leftside')
  I.waitForInvisible('.mail-progress', attachment ? 60 : 10)
}
