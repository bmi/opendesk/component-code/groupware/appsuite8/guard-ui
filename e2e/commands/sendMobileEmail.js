/**
 * This work is provided under the terms of the CREATIVE COMMONS PUBLIC
 * LICENSE. This work is protected by copyright and/or other applicable
 * law. Any use of the work other than as authorized under this license
 * or copyright law is prohibited.
 *
 * http://creativecommons.org/licenses/by-nc-sa/2.5/
 * © 2017 OX Software GmbH, Germany. info@open-xchange.com
 *
 * @author Greg Hill <greg.hill@open-xchange.com>
 */

/**
 * This sends a new email to recipient
 */

const SELECTOR = require('../constants')

module.exports = function (o, subject, data, attachment, options) {
  const userdata = o.user.userdata
  options = options || {}

  // Open compose
  this.wait(2)
  this.click('.inline-toolbar [data-action="io.ox/mail/actions/compose"]')
  this.waitForVisible(SELECTOR.WINDOW_BUSY)
  this.waitForInvisible(SELECTOR.WINDOW_BUSY)
  this.waitForVisible(SELECTOR.EDITOR)

  // Mark as secure unless specified as non-encrypted
  if (!options.unencrypted) {
    this.click('Security')
    this.waitForVisible('.dropdown-menu a[data-name="encrypt"]')
    this.click('Encrypt')
    this.waitForVisible('#ogPassword')
    this.fillField('#ogPassword', userdata.password)
    this.click('.btn[data-action="ok"]')
  }

  if (options.attachKey) {
    this.click('Security')
    this.waitForVisible('.dropdown-menu a[data-name="PubKey"]')
    this.click('Attach my key')
  }

  if (options.sign) {
    this.click('Security')
    this.waitForVisible('.dropdown-menu a[data-name="PGPSignature"]')
    this.click('Sign')
  }

  if (options.inline) {
    this.wait(1)
    this.click('Security')
    this.waitForVisible('.dropdown-menu a[data-name="PGPFormat"]')
    this.click('.dropdown-menu a[data-value="inline"]')
    this.waitForVisible('.btn[data-action="okx"]')
    this.click('.btn[data-action="okx"]')
    this.waitForInvisible('.modal-backdrop')
  }

  // Insert test data
  this.fillField(SELECTOR.TO_FIELD, userdata.email1)
  this.fillField(SELECTOR.SUBJECT, subject)
  const I = this

  if (options.inline) { // Inline plaintext only
    I.fillField('.plain-text', data)
  } else {
    within({ frame: SELECTOR.EDITOR_IFRAME }, async () => {
      I.fillField(SELECTOR.EMAIL_BODY, data)
    })
  }

  // Attachments if any
  if (attachment) {
    this.attachFile('.composetoolbar input[type="file"]', attachment)
    this.wait(2)
  }

  // Verify key is next to recipient names
  if (!options.unencrypted) {
    this.waitForVisible(SELECTOR.TRUSTED_KEY)
    // Send
    this.click('Send Encrypted')
  } else {
    this.click('Send')
  }

  if (options.sign) {
    this.waitForVisible('#ogPassword')
    this.fillField('#ogPassword', userdata.password)
    this.click('.btn[data-action="ok"]')
  }
  this.waitForVisible('.inline-toolbar [data-action="io.ox/mail/actions/compose"]')
  I.waitForInvisible('.mail-progress')
}
