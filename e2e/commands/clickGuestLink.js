/**
 * This work is provided under the terms of the CREATIVE COMMONS PUBLIC
 * LICENSE. This work is protected by copyright and/or other applicable
 * law. Any use of the work other than as authorized under this license
 * or copyright law is prohibited.
 *
 * http://creativecommons.org/licenses/by-nc-sa/2.5/
 * © 2017 OX Software GmbH, Germany. info@open-xchange.com
 *
 * @author Greg Hill <greg.hill@open-xchange.com>
 */

/**
 * Finds the link in a guest link email and clicks
 * 
 */


module.exports  = function (subject, hint) {
    this.waitForVisible(SELECTOR.DETAIL_SUBJECT);
    this.see(subject);
    this.switchTo('.mail-detail-frame');
    if (hint) {
        this.see(hint);
    }
    this.click('a');  // Check data in body
};