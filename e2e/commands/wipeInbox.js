/**
 * This work is provided under the terms of the CREATIVE COMMONS PUBLIC
 * LICENSE. This work is protected by copyright and/or other applicable
 * law. Any use of the work other than as authorized under this license
 * or copyright law is prohibited.
 *
 * http://creativecommons.org/licenses/by-nc-sa/2.5/
 * © 2017 OX Software GmbH, Germany. info@open-xchange.com
 *
 * @author Greg Hill <greg.hill@open-xchange.com>
 */

/**
 * This wipes a users inbox
 * 
 */


module.exports  = function () {
    this.waitForVisible('.io-ox-mail-window .window-body .classic-toolbar');
    // Mark empty inbox (needed for temporary password prompt with correct email)
    this.selectFolder('Inbox');
    this.waitForVisible(SELECTOR.FOLDER_CONTEXT_MENU);
    this.click(SELECTOR.FOLDER_CONTEXT_MENU);
    this.click('.dropdown.open a[data-action="clearfolder"]');
    this.waitForElement('.btn[data-action="delete"]');
    this.click('.btn[data-action="delete"]');
};
