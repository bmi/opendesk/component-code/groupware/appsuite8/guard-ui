const SELECTOR = {
  // Core
  TOPBAR_HELP: '#io-ox-topbar-help-dropdown-icon > .btn.dropdown-toggle',
  ALERT_SUCCESS: '.io-ox-alert-success',
  ALERT_SUCCESS_CLOSE: '.io-ox-alert-success [data-action="close"]',
  DIALOG_CANCEL: '.btn[data-action="cancel"]',

  WINDOW_BUSY: '.window-blocker.io-ox-busy',

  UNREAD: '.io-ox-mail-window .leftside ul li.unread',
  REFRESH: '#io-ox-refresh-icon',
  REFRESH_SPIN: '#io-ox-refresh-icon .animate-spin',

  FOLDER_CONTEXT_MENU: '.selected .contextmenu-control',
  FOLDER_CONTEXT_MENU_MARK_READ: '.dropdown.open a[data-action="markfolderread"]',

  GRID_OPTIONS: '.grid-options button[data-toggle="dropdown"]',

  FILE_MORE: '.dropdown-toggle[data-action="more"]',

  DONE_BUTTON: '.btn-primary[data-action="done"]',
  DONE: '.btn[data-action="done"]',

  APP_LAUNCHER: '.btn.dropdown-toggle.launcher-btn',
  LAUNCHER_DROPDOWN: '.launcher-dropdown',

  // Mail
  DETAIL_SUBJECT: '.io-ox-mail-window .mail-detail-pane .subject',
  DETAIL_VIEW: '.inline-toolbar [data-action="io.ox/mail/attachment/actions/view"]',
  DETAIL_SAVE: '.inline-toolbar [data-action="io.ox/mail/attachment/actions/save"]',
  REPLY: '.classic-toolbar a[data-action="io.ox/mail/actions/reply"]',
  MAIL_MORE_MENU: '.thread-view .mail-header-actions [data-action="more"]',
  EDIT_DRAFT_LINK: '.btn[data-action="io.ox/mail/actions/edit"]',

  // Drive
  VIEWER_CLOSE: '.classic-toolbar [data-action="io.ox/core/viewer/actions/toolbar/close"]',
  VIEWER_IMAGE: '.swiper-slide-active .viewer-displayer-item.viewer-displayer-image',
  VIEWER_WHITE_BACKGROUND: '.white-page.plain-text',
  VIEWER_FILENAME: '.filename',
  VERSION_TOGGLE: '.viewer-fileversions.sidebar-panel .panel-toggle-btn',
  TEXT_EDIT: 'a[data-action="io.ox/files/actions/editor"]',
  TEXT_EDITOR_CONTENT: 'textarea.content.form-control',
  DRIVE_INVITE: 'a[data-action="io.ox/files/actions/share"]',
  MOBILE_VIEW: 'a[data-action="io.ox/files/actions/viewer"]',
  GUARD_ENCRYPT: '.dropdown-menu [data-action="oxguard/encrypt"]',
  GUARD_DECRYPT: '.dropdown-menu [data-action="oxguard/remencrypt"]',
  NEW_BUTTON: '.window-sidepanel .btn[data-toggle="dropdown"]',

  GUARD_FILE_SELECTOR: locate('li').withDescendant(locate('.extension').withText('.txt.pgp')),
  GUARD_FILE_SELECTOR_DOCX: locate('li').withDescendant(locate('.extension').withText('.docx.pgp')),

  // WIZARD
  WIZARD_NEXT: '.btn.btn-primary[data-action="next"]',
  SPOTLIGHT: '.wizard-overlay.wizard-spotlight.abs',

  // Mail compose
  GUARD_LOCK: 'a.toggle-encryption .bi-lock',
  TRUSTED_KEY: '.oxguard_token.trusted_key .bi-key',
  UNTRUSTED_KEY: '.oxguard_token.untrusted_key .bi-key',
  GUEST_ICON: '.bi-person.key',
  SECURITY_OPTIONS: '.security-options [data-toggle="dropdown"]',
  MAIL_OPTIONS: '[data-extension-id="composetoolbar-menu"] a[aria-label="Options"]',

  EDITOR: '.io-ox-mail-compose textarea.plain-text,.io-ox-mail-compose .contenteditable-editor',
  EDITOR_IFRAME: '.io-ox-mail-compose-window .editor iframe',
  EMAIL_BODY: '.mce-content-body',

  SAVE_AND_CLOSE: '.io-ox-mail-compose-window button[aria-label="Save and close"]',
  TO_FIELD: '.io-ox-mail-compose div[data-extension-id="to"] input.tt-input',
  SUBJECT: '.io-ox-mail-compose [name="subject"]',
  CC: '.io-ox-mail-compose div[data-extension-id="cc"] input.tt-input',
  BCC: '.io-ox-mail-compose div[data-extension-id="bcc"] input.tt-input',

  // GUARD ICONS
  SIGNATURE_OK: '.bi-pencil-square.guard_signed',

  // GUARD
  PASSWORD_PROMPT: 'input.form-control.password_prompt',
  GUARD_PASS_BUTTON: '.btn.oxguard_passbutton',

  // Guard Settings
  YOUR_KEYS: '.btn[name="yourKeys"]'
}

module.exports = SELECTOR
