/* eslint-env node, es2017 */
var testRunContext;

// please create .env file based on .evn-example
require('dotenv').config();

['LAUNCH_URL', 'PROVISIONING_URL', 'CONTEXT_ID'].forEach(function notdefined(key) {
    if (process.env[key]) return;
    console.error('\x1b[31m', `ERROR: Missing value for environment variable '${key}'. Please specify a '.env' file analog to '.env-example'.`);
    process.exit();
});

//  Parse the domain from launch URL to use in share configuration
function getShareDomain() {
    var url = process.env.LAUNCH_URL;
    if (url.indexOf('//') > 0) {
        url = url.substring(url.indexOf('//') + 2);
    }
    if (url.indexOf('/') > 0) {
        url = url.substring(0, url.indexOf('/'));
    }
    return url;
}

module.exports.config = {
    tests: './tests/**/*_test.js',
    timeout: 240,
    output: './output/',
    helpers: {
        Puppeteer: {
            url: process.env.LAUNCH_URL,
            host: process.env.SELENIUM_HOST,
            smartWait: 1000,
            waitForTimeout: 10000,
            loginTimeout: 30000,
            browser: 'chrome',
            chrome: {
                args: ['--no-sandbox']
            },
            restart: true,
            windowSize: '1024x800',
            uniqueScreenshotNames: true,
            timeouts: {
                script: 5000
            },
            // set HEADLESS=false in your terminal to show chrome window
            show: process.env.HEADLESS ? process.env.HEADLESS === 'false' : false
        },
        OpenXchange: {
            require: './helper',
            mxDomain: process.env.MX_DOMAIN,
            serverURL: process.env.PROVISIONING_URL,
            contextId: process.env.CONTEXT_ID,
            filestoreId: process.env.FILESTORE_ID,
            smtpServer: process.env.SMTP_SERVER,
            imapServer: process.env.IMAP_SERVER,
            shareDomain: process.env.SHARE_DOMAIN || getShareDomain(),
            skipTests: process.env.SKIP_TESTS,
            newContext: parseInt(process.env.CONTEXT_ID, 10) + 10,
            admin: {
                login: process.env.E2E_ADMIN_USER || 'oxadminmaster',
                password: process.env.E2E_ADMIN_PW || 'secret'
            },
            whatsNew: process.env.WHATS_NEW
        },
        GuardHelper: {
            require: './helper.js'
        }
    },
    include: {
        I: './guardActor',
        users: '@open-xchange/codecept-helper/src/users.js',
        contexts: '@open-xchange/codecept-helper/src/contexts.js',
        settings: './commands/gotoSettings'
    },
    bootstrap: async () => {
        // setup chai
        var chai = require('chai');
        global.assert = chai.assert;
        chai.config.includeStack = true;

        var codecept = require('codeceptjs'),
            config = codecept.config.get(),
            helperConfig = config.helpers.OpenXchange,
            seleniumReady;
        seleniumReady = new Promise(function (resolve, reject) {
            if (config.helpers.WebDriver && /127\.0\.0\.1/.test(config.helpers.WebDriver.host)) {
                require('@open-xchange/codecept-helper').selenium
                    .start()
                    .then(resolve, reject);
            } else {
                resolve('');
            }
        });

        const contexts = codecept.container.support('contexts'),
        helper = new (require('@open-xchange/codecept-helper').helper)();
        let testRunContext = await contexts.create();
        if (typeof testRunContext.id !== 'undefined') helperConfig.contextId = testRunContext.id;
        await seleniumReady.catch(err => console.error(err));
    },
    teardown: async function () {

        var { contexts } = global.inject();
        // we need to run this sequentially, less stress on the MW
        for (let ctx of contexts.filter(ctx => ctx.id > 10)) {
            if (ctx.id !== 10) await ctx.remove().catch(e => console.error(e.message));
        }

        //HACK: defer killing selenium, because it's still needed for a few ms
        setTimeout(function () {
            require('@open-xchange/codecept-helper').selenium.stop();
        }, 500);
    },
    reporter: 'mocha-multi',
    mocha: {
        reporterOptions: {
            'codeceptjs-cli-reporter': {
                stdout: '-'
            },
            'mocha-junit-reporter': {
                stdout: '-',
                options: {
                    jenkinsMode: true,
                    mochaFile: './output/junit.xml',
                    attachments: false // add screenshot for a failed test
                }
            }
        }
    },
    plugins: {
        filterSuite: {
            enabled: process.env.CI,
            require: '@open-xchange/codecept-helper/src/plugins/filterSuite',
            suiteFilePath: process.env.FILTER_SUITE || [],
            filterFn: process.env.runOnly === 'true' ? () => false : undefined
        },
        allure: { enabled: true },
        browserLogReport: {
            require: './plugins/browserLogReport/index',
            enabled: true
        },
        customizePlugin: {
            require: process.env.CUSTOMIZE_PLUGIN || "./plugins/guardProvisioning/index.cjs",
            enabled: true
        }
    },
    rerun: {
        minSuccess: Number(process.env.MIN_SUCCESS),
        maxReruns: Number(process.env.MAX_RERUNS)
    },
    name: 'App Suite Guard UI'
};

