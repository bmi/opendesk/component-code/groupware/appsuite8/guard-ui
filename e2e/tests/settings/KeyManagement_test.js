/**
 * This work is provided under the terms of the CREATIVE COMMONS PUBLIC
 * LICENSE. This work is protected by copyright and/or other applicable
 * law. Any use of the work other than as authorized under this license
 * or copyright law is prohibited.
 *
 * http://creativecommons.org/licenses/by-nc-sa/2.5/
 * © 2017 OX Software GmbH, Germany. info@open-xchange.com
 *
 * @author Greg Hill <greg.hill@open-xchange.com>
 */

/// <reference path="../../steps.d.ts" />

const SELECTOR = require('../../constants.js')

Feature('Settings - Key Management')

Before(async function ({ users }) {
  await Promise.all([
    users.create(),
    users.create()
  ])
})

After(async function ({ users }) {
  await users.removeAll()
})

const assert = require('chai').assert

Scenario('View details of your keys', async function ({ I, users, settings }) {
  const o = {
    user: users[0]
  }

  await I.setupUser(o, true) // need advanced settings

  I.login('app=io.ox/mail', o)

  await I.verifyUserSetup(o)

  // Next, log in to settings
  await settings.goToSettings()

  // Check yourkeys list has at least two keys
  I.waitForVisible(SELECTOR.YOUR_KEYS)
  I.click(SELECTOR.YOUR_KEYS)
  I.waitForVisible('#userkeytable')
  I.waitForVisible('.userpgpdetail')
  I.seeNumberOfVisibleElements('tr', 2)

  // Check key details
  I.click('.userpgpdetail')
  I.waitForVisible('.details dl')
  const keyDetail = await I.grabTextFrom('#privKeyDetail .modal-body')
  assert.include(keyDetail, 'Fingerprint', 'Key Detail includes fingerprint')

  // Check able to check signatures
  I.click('.btn[data-action="signatures"]')
  I.waitForVisible('#pkeyDetail')
  I.see('Public Keys Detail', '#pkeyDetail .modal-title')
  I.seeNumberOfVisibleElements('.signatureTable tr', 2)
  I.see('Positive', '[title="Positive certification of a User ID"]')
})

Scenario('Upload public keys and verify available', async function ({ I, users, settings }) {
  const o = {
    user: users[0]
  }

  await I.setupUser(o, true) // need advanced settings

  I.login('app=io.ox/mail', o)

  await I.verifyUserSetup(o)

  // Next, log in to settings
  await settings.goToSettings()

  I.waitForVisible('.btn[name="recipients"]')
  I.click('.btn[name="recipients"]')

  I.waitForElement('.publicKeyDiv [name="publicKey"]')

  I.executeAsyncScript(async function (done) {
    const { default: $ } = await import('./jquery.js')
    const { default: uploader } = await import('./io.ox/guard/pgp/uploadkeys.js')
    $('[name="publicKey"]').on('change', function () {
      const files = this.files
      if (files.length > 0) {
        uploader.uploadExternalKey(files)
          .always(function () {

          })
      }
    })
    done()
  })
  I.attachFile('.publicKeyDiv [name="publicKey"]', 'testFiles/email-example-com.asc')
  I.wait(1)

  // Confirm uploaded, and check key details
  I.click('.refreshkeys')
  I.wait(1)
  I.seeNumberOfVisibleElements('tr', 1)
  I.see('Test User ', 'td')
  I.click('.oxguard_pubkeylist a[ids="6be6dc8 b2f16762"]')
  I.waitForVisible('#pkeyDetail .modal-title')
  I.see('Public Keys Detail', '.modal-title')
  I.seeNumberOfVisibleElements('#keyDetail', 2)

  I.click('Signatures')
  I.waitForVisible('.signatureTable')
  I.seeNumberOfVisibleElements('.signatureTable tr', 2)
  I.see('Positive', '.signatureTable')
  I.click('.modal[data-point="oxguard/pgp/signatureview"] [data-action="cancel"]')
  I.wait(1)

  I.click('#pkeyDetail [data-action="cancel"]')
  I.click('#pkeyList [data-action="cancel"]')

  settings.close()

  // Start a mail to see if key properly loaded
  I.openApp('Mail')
  I.waitForVisible('.io-ox-mail-window .classic-toolbar-container .classic-toolbar')
  I.startEncryptedEmail(o)
  I.waitForVisible(SELECTOR.GUARD_LOCK)

  // Add uploaded key email address and confirm key found and properly labeled
  I.fillField(SELECTOR.TO_FIELD, 'email@example.com')
  I.fillField('[name="subject"]', 'test')
  I.waitForVisible(SELECTOR.TRUSTED_KEY)
  I.moveCursorTo(SELECTOR.TRUSTED_KEY, 5, 5)
  I.waitForVisible('.tooltip')
  I.see('Uploaded', '.tooltip')

  // Cleanup
  I.click(SELECTOR.SAVE_AND_CLOSE)
  I.waitForElement('.modal')
  I.click('Delete draft')
  I.waitForInvisible('.modal-backdrop')
  I.logout()
})

Scenario('Private Key Download and Upload', async function ({ I, users, settings }) {
  const o = {
    user: users[0]
  }

  await I.setupUser(o, true) // need advanced settings

  I.login('app=io.ox/mail', o)

  await I.verifyUserSetup(o)

  // Next, log in to settings
  await settings.goToSettings()

  // First, download the current private key
  I.waitForVisible(SELECTOR.YOUR_KEYS)
  I.click(SELECTOR.YOUR_KEYS)
  I.waitForVisible('.delPriv')
  // Get the keyId from table and download
  const keyId = await I.grabAttributeFrom('#userkeytable tr', 'data-id')
  const downloadedKey = await I.downloadPrivateKey(keyId, o)

  // OK, delete the key
  I.click('.delPriv')
  I.waitForVisible('.modal[data-point="io.ox/guard/settings/deletePrivate"]')
  I.click('.btn[data-action="delete"]')
  I.waitForVisible('#deletepass')
  I.insertCryptPassword('#deletepass', o)
  I.click('Delete')

  // Upload dialog should appear now
  I.waitForVisible('.modal[data-point="io.ox/guard/settings/uploadPrivate"]')
  const userdata = o.user.userdata
  // We have to manualy perform the upload here
  await I.executeAsyncScript(async function (downloadedKey, userdata, done) {
    const formData = new FormData()
    formData.append('key', downloadedKey)
    formData.append('keyPassword', userdata.password)
    formData.append('newPassword', userdata.password)
    const { default: ox } = await import('./ox.js')
    const { default: $ } = await import('./jquery.js')
    const url = ox.apiRoot + '/oxguard/keys?action=upload&respondWithJSON=true&session=' + ox.session
    $.ajax({
      url: url,
      type: 'POST',
      data: formData,
      processData: false, // tell jQuery not to process the data
      contentType: false, // tell jQuery not to set contentType
      success: function (data) {
        done(data)
      },
      fail: function (e) {
        done(e)
      }
    })
  }, downloadedKey, userdata)

  I.waitForVisible('.btn-default[data-action="cancel"]')
  // Cancel dialog since uploaded already
  I.click('.btn-default[data-action="cancel"]')

  // Confirm key is now there and same
  I.click('#refreshuserkeys')
  I.wait(1)
  I.waitForVisible('.delPriv')
  const checkKeyId = await I.grabAttributeFrom('#userkeytable tr', 'data-id')
  assert.equal(checkKeyId[0], keyId[0], 'Key Ids should match')
})

Scenario('Revoke Private Key', async function ({ I, users, settings }) {
  const o = {
    user: users[1]
  }

  await I.setupUser(o, true) // need advanced settings

  I.login('app=io.ox/mail', o)

  await I.verifyUserSetup(o)

  // Next, log in to settings
  await settings.goToSettings()

  // Revoke the key
  I.waitForVisible(SELECTOR.YOUR_KEYS)
  I.click(SELECTOR.YOUR_KEYS)
  I.waitForVisible('.delPriv')
  I.click('.delPriv')
  I.waitForVisible('.modal[data-point="io.ox/guard/settings/deletePrivate"]')
  I.click('.btn[data-action="revoke"]')
  I.waitForVisible('#revokepass')
  I.insertCryptPassword('#revokepass', o)
  I.selectOption('#settings-revokereason', 'KEY_SUPERSEDED')
  I.click('.btn[data-action="revoke"]')

  // Confirm drawn as revoked
  I.waitForVisible('tr.revoked')

  // Check detail
  I.click('.userpgpdetail')
  I.waitForVisible('.info-line.stripes-red')
  I.see('Revoked', '.info-line')
})
