/**
 * I work is provided under the terms of the CREATIVE COMMONS PUBLIC
 * LICENSE. I work is protected by copyright and/or other applicable
 * law. Any use of the work other than as authorized under I license
 * or copyright law is prohibited.
 *
 * http://creativecommons.org/licenses/by-nc-sa/2.5/
 * © 2017 OX Software GmbH, Germany. info@open-xchange.com
 *
 * @author Greg Hill <greg.hill@open-xchange.com>
 */

/// <reference path="../../steps.d.ts" />

const SELECTOR = require('../../constants.js')

Feature('AutoCrypt Tests')

Before(async function ({ users }) {
  await users.create()
})

After(async function ({ users }) {
  await users.removeAll()
})

function verify (I, o, advanced) {
  I.startEncryptedEmail(o)
  I.waitForVisible(SELECTOR.GUARD_LOCK)
  I.fillField(SELECTOR.TO_FIELD, 'actest@encr.us')
  I.fillField(SELECTOR.SUBJECT, 'test')
  I.waitForVisible(advanced ? SELECTOR.TRUSTED_KEY : SELECTOR.UNTRUSTED_KEY)
  I.click(SELECTOR.SAVE_AND_CLOSE)
  I.waitForVisible('.modal-dialog')
  I.click('Delete draft')
}

Scenario('Autocrypt header recognized - Advanced user', async function ({ I, users }) {
  const o = {
    user: users[0]
  }
  await I.setupUser(o, true) // need advanced settings

  I.login('app=io.ox/mail', o)

  await I.verifyUserSetup(o)

  await I.haveMail({ folder: 'default0/INBOX', path: 'testFiles/ac.eml' })

  I.click(SELECTOR.REFRESH)
  I.waitForVisible(SELECTOR.UNREAD)
  I.click(SELECTOR.UNREAD)

  I.waitForVisible('.autoCryptSummary')
  I.see('C878615D')
  I.click('.ac-toggle-details a')
  I.see('IDs: Autocrypt Test')
  I.see('C878 615D')
  I.see('4011 35EE')
  I.click('Add')
  I.waitForVisible(SELECTOR.ALERT_SUCCESS)
  I.see('Key imported', SELECTOR.ALERT_SUCCESS)
  I.click(SELECTOR.ALERT_SUCCESS_CLOSE)

  verify(I, o, true)

  I.logout()
})

Scenario('Autocrypt signature - Advanced user', async function ({ I, users }) {
  const o = {
    user: users[0]
  }
  await I.setupUser(o, true) // need advanced settings

  I.login('app=io.ox/mail', o)

  await I.verifyUserSetup(o)

  await I.haveMail({ folder: 'default0/INBOX', path: 'testFiles/signed.eml' })

  I.click(SELECTOR.REFRESH)
  I.waitForVisible(SELECTOR.UNREAD)
  I.click(SELECTOR.UNREAD)

  I.waitForVisible('.autoCryptSummary')
  I.wait(2) // wait for background signature check to finish.  Avoid race
  I.click('Add')
  I.waitForVisible(SELECTOR.ALERT_SUCCESS)
  I.see('Key imported', SELECTOR.ALERT_SUCCESS)
  I.waitForInvisible('.oxguard_info')
  I.waitForVisible(SELECTOR.SIGNATURE_OK) // Should pull again and verify signature
  I.wait(1)
  I.dontSee('Missing public key')

  I.logout()
})

Scenario('Autocrypt header recognized - Basic user', async function ({ I, users }) {
  const o = {
    user: users[0]
  }

  I.login('app=io.ox/mail', o)

  await I.verifyUserSetup(o)

  await I.haveMail({ folder: 'default0/INBOX', path: 'testFiles/ac.eml' })

  I.click(SELECTOR.REFRESH)
  I.waitForVisible(SELECTOR.UNREAD)
  I.click(SELECTOR.UNREAD)

  I.wait(1) // Wait a moment for silent import

  verify(I, o)

  I.logout()
})

Scenario('Autocrypt signature- Basic user', async function ({ I, users }) {
  const o = {
    user: users[0]
  }

  I.login('app=io.ox/mail', o)

  await I.verifyUserSetup(o)

  await I.haveMail({ folder: 'default0/INBOX', path: 'testFiles/signed.eml' })

  I.click(SELECTOR.REFRESH)
  I.waitForVisible(SELECTOR.UNREAD)
  I.click(SELECTOR.UNREAD)
  I.waitForVisible('.oxguard_info')
  I.wait(1) // Wait a moment

  I.see('Missing public key')

  I.logout()
})
