/**
 * This work is provided under the terms of the CREATIVE COMMONS PUBLIC
 * LICENSE. This work is protected by copyright and/or other applicable
 * law. Any use of the work other than as authorized under this license
 * or copyright law is prohibited.
 *
 * http://creativecommons.org/licenses/by-nc-sa/2.5/
 * © 2017 OX Software GmbH, Germany. info@open-xchange.com
 *
 * @author Greg Hill <greg.hill@open-xchange.com>
 */

/// <reference path="../../steps.d.ts" />

Feature('Remembering Passwords')

const SELECTOR = require('../../constants')

Before(async function ({ users }) {
  await users.create()
})

After(async function ({ users }) {
  await users.removeAll()
})

Scenario('Decrypt multiple emails with one password', async function ({ I, users }) {
  const count = 4 // number of emails
  const o = {
    user: users[0]
  }
  const user = o.user

  I.login('app=io.ox/mail', o)

  await I.verifyUserSetup(o)

  for (let i = 0; i < count; i++) {
    await I.haveMail({
      attachments: [{
        content: 'Test content ' + i,
        content_type: 'text/html',
        disp: 'inline'
      }],
      from: [[user.get('display_name'), user.get('primaryEmail')]],
      sendtype: 0,
      subject: 'Test subject ' + i,
      to: [[user.get('display_name'), user.get('primaryEmail')]],
      security: {
        encrypt: true,
        sign: false
      }
    })
  }
  I.waitForElement(SELECTOR.REFRESH_SPIN)
  I.waitForDetached(SELECTOR.REFRESH_SPIN)
  I.click(SELECTOR.REFRESH)
  I.waitForVisible(SELECTOR.UNREAD)

  let subj = await I.grabTextFrom('.unread .subject .drag-title')
  I.click('.list-item.unread') // click one email

  I.waitForElement(SELECTOR.PASSWORD_PROMPT)
  I.insertCryptPassword(SELECTOR.PASSWORD_PROMPT, o)
  I.retry(3).click('Remember my password')
  I.click(SELECTOR.GUARD_PASS_BUTTON)

  I.verifyDecryptedMail(subj[0], subj[0].replace('subject', 'content'), false)

  for (let j = 1; j < count; j++) {
    subj = await I.grabTextFrom('.unread .subject .drag-title')
    I.click('.list-item.unread')
    const subject = Array.isArray(subj) ? subj[0] : subj
    I.verifyDecryptedMail(subject, subject.replace('subject', 'content'), false)
  }
})
