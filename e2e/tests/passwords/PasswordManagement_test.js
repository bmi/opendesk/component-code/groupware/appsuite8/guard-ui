/**
 * This work is provided under the terms of the CREATIVE COMMONS PUBLIC
 * LICENSE. This work is protected by copyright and/or other applicable
 * law. Any use of the work other than as authorized under this license
 * or copyright law is prohibited.
 *
 * http://creativecommons.org/licenses/by-nc-sa/2.5/
 * © 2017 OX Software GmbH, Germany. info@open-xchange.com
 *
 * @author Greg Hill <greg.hill@open-xchange.com>
 */

/// <reference path="../../steps.d.ts" />

Feature('Settings - Password Management')

const SELECTOR = require('../../constants')

Before(async function ({ users }) {
  await users.create()
})

After(async function ({ users }) {
  await users.removeAll()
})

Scenario('Reset Password and Change', async function ({ I, users, settings }) {
  const o = {
    user: users[0]
  }

  I.login('app=io.ox/mail', o)

  await I.verifyUserSetup(o)

  // Next, log in to settings
  await settings.goToSettings()

  // Reset the password
  I.see('Reset password')
  I.click('Reset password')
  I.waitForVisible('.modal[data-point="io.ox/guard/settings/reset"]')
  I.seeTextEquals('Reset Password', '.modal[data-point="io.ox/guard/settings/reset"] .modal-title') // Header
  I.click('.btn[data-action="reset"]')
  I.waitForInvisible('.modal[data-point="io.ox/guard/settings/reset"] .modal-dialog')

  settings.close()

  // Goto mail and wait for mail to arrive
  I.openApp('Mail')
  I.waitForVisible('.io-ox-mail-window .leftside')
  I.wait(1)
  I.click(SELECTOR.REFRESH)
  I.waitForVisible(SELECTOR.UNREAD)
  I.click(SELECTOR.UNREAD)

  // Pull the new temporary password from the email
  I.waitForVisible('.mail-detail-frame')
  I.switchTo('.mail-detail-frame')
  const newPass = await I.grabTextFrom('.bodyBox')
  I.switchTo()

  // Got back to settings and change the password
  await settings.goToSettings()
  I.waitForVisible('#oldogpassword')
  I.fillField('#oldogpassword', newPass)
  I.insertCryptPassword('#newogpassword', o)
  I.insertCryptPassword('#newogpassword2', o)
  I.click('OK')

  // Confirm good change of the temporary password
  I.waitForVisible(SELECTOR.ALERT_SUCCESS)
  I.seeTextEquals('Password changed successfully.', SELECTOR.ALERT_SUCCESS)
})
