/**
 * This work is provided under the terms of the CREATIVE COMMONS PUBLIC
 * LICENSE. This work is protected by copyright and/or other applicable
 * law. Any use of the work other than as authorized under this license
 * or copyright law is prohibited.
 *
 * http://creativecommons.org/licenses/by-nc-sa/2.5/
 * © 2017 OX Software GmbH, Germany. info@open-xchange.com
 *
 * @author Greg Hill <greg.hill@open-xchange.com>
 */

/// <reference path="../../steps.d.ts" />

const SELECTOR = require('../../constants.js')

Feature('Drive - Encrypt-Decrypt')

Before(async function ({ users }) {
  await Promise.all([
    users.create(),
    users.create()
  ])
})

After(async function ({ users }) {
  await users.removeAll()
})

const assert = require('chai').assert

Scenario('Mobile: Encrypt and then view a file', async function ({ I, users }) {
  const o = {
    user: users[0]
  }
  const infostoreFolderID = await I.grabDefaultFolder('infostore')
  const fileId = await I.haveFile(infostoreFolderID, 'testFiles/simpleText.txt', o)
  const id = fileId.folder_id + '.' + fileId.id

  await I.openMobile()
  I.login('app=io.ox/files', o)

  await I.verifyUserSetup(o)

  // Encrypt the file
  I.waitForVisible('.list-view li[data-cid="' + id + '"]')
  I.tap('.list-view li[data-cid="' + id + '"] .list-item-checkmark')
  I.wait(1)
  I.click(SELECTOR.FILE_MORE)
  I.click('Encrypt')
  I.wait(1)
  I.waitForVisible('.extension')
  I.see('.txt.pgp', '.extension')

  // Now, try decrypting/viewing
  I.tap(SELECTOR.GUARD_FILE_SELECTOR)
  // I.waitForVisible(SELECTOR.MOBILE_VIEW)
  // I.tap(SELECTOR.MOBILE_VIEW)
  I.auth(o)

  I.waitForVisible(SELECTOR.VIEWER_WHITE_BACKGROUND)
  const content = await I.grabTextFrom(SELECTOR.VIEWER_WHITE_BACKGROUND)
  assert.include(content, 'simple text', 'Decrypted data content')

  // Cleanup
  I.click(SELECTOR.VIEWER_CLOSE)
  I.wait(1)
  I.logout()
})

async function verifyThreeVersions (I) {
  I.waitForText('3', '.version-count')
  I.waitForVisible(SELECTOR.VERSION_TOGGLE)
  I.waitForEnabled(SELECTOR.VERSION_TOGGLE)
  I.retry(5).click(SELECTOR.VERSION_TOGGLE)
  I.waitForElement('.versiontable.table')
  I.waitNumberOfVisibleElements('tr.version', 3, 10)
}

Scenario('Encrypt and decrypt file with multiple versions', async function ({ I, users }) {
  const o = {
    user: users[0]
  }
  const infostoreFolderID = await I.grabDefaultFolder('infostore')
  const fileId = await I.haveFile(infostoreFolderID, 'testFiles/simpleText.txt', o)
  const id = fileId.folder_id + '.' + fileId.id

  I.login('app=io.ox/files', o)

  await I.verifyUserSetup(o)

  // Select the file to edit
  I.waitForVisible('.list-view li[data-cid="' + id + '"]')
  I.click('.list-view li[data-cid="' + id + '"]')
  I.wait(1)

  I.click(SELECTOR.TEXT_EDIT)
  I.waitForVisible(SELECTOR.TEXT_EDITOR_CONTENT)
  I.fillField(SELECTOR.TEXT_EDITOR_CONTENT, '2')
  I.click('Save')
  I.wait(1)
  I.fillField(SELECTOR.TEXT_EDITOR_CONTENT, '3')
  I.click('Save')
  I.wait(1)
  I.click('Close')
  await verifyThreeVersions(I)
  I.click('tr.version[data-version-number="2"] button.dropdown-toggle')
  I.waitForText('Make this the current version')
  I.click('Make this the current version')
  I.wait(1)
  // Encrypt
  I.waitForVisible(SELECTOR.FILE_MORE)
  I.click(SELECTOR.FILE_MORE)
  I.waitForVisible(SELECTOR.GUARD_ENCRYPT)
  I.click(SELECTOR.GUARD_ENCRYPT)
  I.wait(1)
  I.waitForVisible(SELECTOR.GUARD_FILE_SELECTOR)
  I.click(SELECTOR.GUARD_FILE_SELECTOR)
  await verifyThreeVersions(I)

  I.wait(1)
  I.click(SELECTOR.FILE_MORE)
  I.click(SELECTOR.GUARD_DECRYPT)
  I.auth(o)
  I.waitForInvisible(locate('.extension').withText('.txt.pgp'))

  I.dontSee('.txt.pgp', '.extension')
  I.waitForVisible('.file-type-txt')
  I.click('.file-type-txt')

  await verifyThreeVersions(I)

  // Verify selected version persisted
  I.doubleClick('.file-type-txt')
  I.waitForVisible(SELECTOR.VIEWER_WHITE_BACKGROUND)
  const content = await I.grabTextFrom(SELECTOR.VIEWER_WHITE_BACKGROUND)
  assert.include(content, '2', 'File content')

  // Cleanup
  I.click(SELECTOR.VIEWER_CLOSE)
  I.wait(1)
  I.logout()
})

Scenario('Desktop: Encrypt and then view a file', async function ({ I, users }) {
  const o = {
    user: users[0]
  }
  const infostoreFolderID = await I.grabDefaultFolder('infostore')
  const fileId = await I.haveFile(infostoreFolderID, 'testFiles/simpleText.txt', o)
  const id = fileId.folder_id + '.' + fileId.id

  I.login('app=io.ox/files', o)

  await I.verifyUserSetup(o)

  // Encrypt the file
  I.waitForVisible('.list-view li[data-cid="' + id + '"]')
  I.click('.list-view li[data-cid="' + id + '"]')
  I.wait(1)
  I.click(SELECTOR.FILE_MORE)
  I.click(SELECTOR.GUARD_ENCRYPT)
  I.wait(1)
  I.waitForVisible(SELECTOR.GUARD_FILE_SELECTOR)
  I.see('.txt.pgp', '.list-view')

  // Now, try decrypting/viewing
  I.doubleClick(SELECTOR.GUARD_FILE_SELECTOR)
  I.auth(o)

  I.waitForVisible(SELECTOR.VIEWER_WHITE_BACKGROUND)
  const content = await I.grabTextFrom(SELECTOR.VIEWER_WHITE_BACKGROUND)
  assert.include(content, 'simple text', 'Decrypted data content')

  // Cleanup
  I.click(SELECTOR.VIEWER_CLOSE)
  I.wait(1)
  I.logout()
})

Scenario('Remove encryption and then view a file', async function ({ I, users }) {
  const o = {
    user: users[0],
    cryptoAction: 'encrypt'
  }
  const infostoreFolderID = await I.grabDefaultFolder('infostore')

  I.login('app=io.ox/files', o)

  await I.verifyUserSetup(o)
  const fileId = await I.haveFile(infostoreFolderID, 'testFiles/simpleText.txt', o)
  const id = fileId.folder_id + '.' + fileId.id
  I.wait(1)
  I.click(SELECTOR.REFRESH)
  // Encrypt the file
  I.waitForVisible('.list-view li[data-cid="' + id + '"]')
  I.click('.list-view li[data-cid="' + id + '"]')
  I.wait(1)
  I.click(SELECTOR.FILE_MORE)
  I.click(SELECTOR.GUARD_DECRYPT)
  I.auth(o)
  I.waitForInvisible(locate('.extension').withText('.txt.pgp'))
  I.waitForVisible('.file-type-txt')

  // Now, try decrypting/viewing
  I.doubleClick('.file-type-txt')
  I.waitForVisible(SELECTOR.VIEWER_WHITE_BACKGROUND)
  const content = await I.grabTextFrom(SELECTOR.VIEWER_WHITE_BACKGROUND)
  assert.include(content, 'simple text', 'Decrypted data content')

  // Cleanup
  I.click(SELECTOR.VIEWER_CLOSE)
  I.wait(1)
  I.logout()
})
