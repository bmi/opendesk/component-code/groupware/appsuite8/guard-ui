/**
 * This work is provided under the terms of the CREATIVE COMMONS PUBLIC
 * LICENSE. This work is protected by copyright and/or other applicable
 * law. Any use of the work other than as authorized under this license
 * or copyright law is prohibited.
 *
 * http://creativecommons.org/licenses/by-nc-sa/2.5/
 * © 2017 OX Software GmbH, Germany. info@open-xchange.com
 *
 * @author Greg Hill <greg.hill@open-xchange.com>
 */

/// <reference path="../../steps.d.ts" />

const SELECTOR = require('../../constants.js')

Feature('Drive - File Sharing')

Before(async function ({ users }) {
  await Promise.all([
    users.create(),
    users.create()
  ]) // Second user for revoke test
})

After(async function ({ users }) {
  await users.removeAll()
})

const assert = require('chai').assert

Scenario('Share an encrypted file', async function ({ I, users }) {
  const o = {
    user: users[0],
    cryptoAction: 'encrypt'
  }
  const o2 = {
    user: users[1]
  }

  // Make sure second user setup
  I.login('app=io.ox/files', o2)

  await I.verifyUserSetup(o2)
  I.logout()

  // Login as first user, and setup
  const infostoreFolderID = await I.grabDefaultFolder('infostore')

  I.login('app=io.ox/files', o)

  await I.verifyUserSetup(o)
  const fileId = await I.haveFile(infostoreFolderID, 'testFiles/simpleText.txt', o)
  const id = fileId.folder_id + '.' + fileId.id

  // OK, let's share
  I.wait(1)
  I.waitForVisible(SELECTOR.REFRESH)
  I.click(SELECTOR.REFRESH)
  I.waitForVisible('.list-view li[data-cid="' + id + '"]')
  I.click('.list-view li[data-cid="' + id + '"]')
  I.wait(1)
  I.waitForVisible(SELECTOR.DRIVE_INVITE)
  I.click(SELECTOR.DRIVE_INVITE)
  I.auth(o)
  I.waitForVisible('.tt-input')
  I.click('.tt-input')
  I.wait(1)
  I.fillField('.tt-input', o2.user.userdata.email1)
  I.wait(1)
  I.pressKey('Tab') // remove focus
  I.wait(1)
  I.click('.btn-primary[data-action="save"]') // share
  I.waitForInvisible('.modal-content')
  I.logout()

  I.login('app=io.ox/files', o2)
  I.waitForElement('.file-list-view.complete')
  I.wait(1)
  I.selectFolder('Shared files')
  I.waitForInvisible(SELECTOR.WINDOW_BUSY)
  I.waitForVisible(SELECTOR.GUARD_FILE_SELECTOR)
  I.see('.txt.pgp', '.extension')

  // Now, try decrypting/viewing
  I.doubleClick(SELECTOR.GUARD_FILE_SELECTOR)
  I.auth(o2)

  I.waitForVisible(SELECTOR.VIEWER_WHITE_BACKGROUND, 15)
  const content = await I.grabTextFrom(SELECTOR.VIEWER_WHITE_BACKGROUND)
  assert.include(content, 'simple text', 'Decrypted data content')

  // Cleanup
  I.click(SELECTOR.VIEWER_CLOSE)
  I.wait(1)
  I.logout()
})
