/**
 * I work is provided under the terms of the CREATIVE COMMONS PUBLIC
 * LICENSE. I work is protected by copyright and/or other applicable
 * law. Any use of the work other than as authorized under I license
 * or copyright law is prohibited.
 *
 * http://creativecommons.org/licenses/by-nc-sa/2.5/
 * © 2017 OX Software GmbH, Germany. info@open-xchange.com
 *
 * @author Greg Hill <greg.hill@open-xchange.com>
 */

/// <reference path="../../steps.d.ts" />

Feature('Recovery test')

const SELECTOR = require('../../constants')

Before(async function ({ users }) {
  await users.create()
})

After(async function ({ users }) {
  await users.removeAll()
})

Scenario('Recovery draft with sign and PGP Inline set', async function ({ I, users }) {
  const o = {
    user: users[0]
  }
  const testSubject = 'Recovery test'
  const testData = 'Test data'

  await I.setupUser(o, true) // Advanced settings

  await I.haveSetting({ 'io.ox/mail': { 'didYouKnow/saveOnCloseDontShowAgain': true } })

  I.login('app=io.ox/mail', o)

  await I.verifyUserSetup(o)

  I.waitForVisible('.io-ox-mail-window .leftside')
  await I.sendEmail(o, testSubject, testData, undefined, { sign: true, inline: true, startOnly: true })

  I.logout()

  I.login('app=io.ox/mail')

  I.waitForVisible('.io-ox-mail-window .leftside')

  I.waitForElement('.folder-node[title*="Drafts"]')
  I.waitForElement(locate('.folder-node[title*="Drafts"] .folder-counter').withText('1'))
  I.click(locate('.folder-node[title*="Drafts"] .folder-counter').withText('1'))
  I.waitForText(testSubject, 5, '~Messages')
  I.click(testSubject, '~Messages')
  I.waitForElement('.mail-detail')
  I.wait(3)
  I.waitForVisible(SELECTOR.EDIT_DRAFT_LINK)
  I.click(SELECTOR.EDIT_DRAFT_LINK)
  I.wait(1)
  I.auth(o)

  I.waitForVisible('.plain-text')
  I.wait(1)
  I.seeInField('.plain-text', testData)

  I.waitForVisible(SELECTOR.SECURITY_OPTIONS)
  I.click(SELECTOR.SECURITY_OPTIONS)

  I.waitForElement('a[data-name="PGPSignature"][aria-checked="true"]')
  I.waitForElement('a[data-value="inline"][aria-checked="true"]')
})

Scenario('Recovery draft with sign only', async function ({ I, users }) {
  const o = {
    user: users[0]
  }
  const testSubject = 'Recovery test'
  const testData = 'Test data'

  await I.setupUser(o, true) // Advanced settings

  I.login('app=io.ox/mail', o)

  await I.verifyUserSetup(o)
  I.waitForVisible('.io-ox-mail-window .leftside')
  await I.sendEmail(o, testSubject, testData, undefined, { sign: true, unencrypted: true, startOnly: true })

  I.logout()

  I.login('app=io.ox/mail')

  I.waitForVisible('.io-ox-mail-window .leftside')

  I.waitForElement('.folder-node[title*="Drafts"]')
  I.waitForElement(locate('.folder-node[title*="Drafts"] .folder-counter').withText('1'))
  I.click(locate('.folder-node[title*="Drafts"] .folder-counter').withText('1'))
  I.waitForText(testSubject, 5, '~Messages')
  I.click(testSubject, '~Messages')
  I.waitForElement('.mail-detail')
  I.waitForVisible(SELECTOR.EDIT_DRAFT_LINK)
  I.click(SELECTOR.EDIT_DRAFT_LINK)

  I.wait(1)
  I.waitForInvisible(SELECTOR.WINDOW_BUSY)
  I.waitForVisible(SELECTOR.EDITOR)
  I.waitForVisible(SELECTOR.MAIL_OPTIONS)
  I.click(SELECTOR.MAIL_OPTIONS)
  I.waitForVisible('.open a[data-name="PGPSignature"][aria-checked="true"]')
  I.switchTo('iframe')
  I.see(testData)
  I.switchTo()
})
