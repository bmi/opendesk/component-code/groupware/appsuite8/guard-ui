/**
 * This work is provided under the terms of the CREATIVE COMMONS PUBLIC
 * LICENSE. This work is protected by copyright and/or other applicable
 * law. Any use of the work other than as authorized under this license
 * or copyright law is prohibited.
 *
 * http://creativecommons.org/licenses/by-nc-sa/2.5/
 * © 2017 OX Software GmbH, Germany. info@open-xchange.com
 *
 * @author Greg Hill <greg.hill@open-xchange.com>
 */

/// <reference path="../../steps.d.ts" />

Feature('Composition Space Tests')

Before(async function ({ users }) {
  await users.create()
})

After(async function ({ users }) {
  await users.removeAll()
})

// Composition space recovery removed in 8.0

/*
const SELECTOR = require('../../constants')

async function createTestEmail (I, userdata, savePassword) {
  I.click('New email')
  I.waitForVisible(SELECTOR.WINDOW_BUSY)
  I.waitForInvisible(SELECTOR.WINDOW_BUSY)
  I.waitForVisible(SELECTOR.EDITOR)
  I.fillField(SELECTOR.SUBJECT, 'Test email')
  within({ frame: SELECTOR.EDITOR_IFRAME }, async () => {
    I.fillField(SELECTOR.EMAIL_BODY, 'This is test data')
  })
  I.click('.toggle-encryption')
  I.waitForVisible('#ogPassword')
  if (savePassword) {
    I.click('#rememberpass')
  }
  I.fillField('#ogPassword', userdata.password)
  I.click('.btn[data-action="ok"]')
  I.waitForText('Saving...', 17, '.inline-yell')
}

async function checkRestored (I, userdata) {
  I.waitForVisible('.taskbar-button[data-action="restore"]')
  I.click('.taskbar-button[data-action="restore"]')

  I.waitForVisible(SELECTOR.WINDOW_BUSY)

  if (userdata) {
    I.waitForVisible('#ogPassword')
    I.fillField('#ogPassword', userdata.password)
    I.click('.btn[data-action="ok"]')
    I.waitForInvisible(SELECTOR.WINDOW_BUSY)
  } else {
    I.waitForInvisible(SELECTOR.WINDOW_BUSY)
  }

  within({ frame: SELECTOR.EDITOR_IFRAME }, async () => {
    I.see('This is test data')
  })
  I.see('This email will be encrypted', '.encrypted')
}

Scenario('Recover with saved password', async function ({ I, users }) {
  const o = {
    user: users[0]
  }
  const userdata = o.user.userdata

  // Log in as User 0
  I.login('app=io.ox/mail', o)
  I.waitForVisible('.io-ox-mail-window .window-body')
  I.verifyUserSetup(userdata) // Verify user has Guard setup

  await createTestEmail(I, userdata, true)

  I.refreshPage()
  I.waitForVisible('.io-ox-mail-window .window-body')

  await checkRestored(I)
})

Scenario('Recover with without saved password - refresh', async function ({ I, users }) {
  const o = {
    user: users[0]
  }
  const userdata = o.user.userdata

  // Log in as User 0
  I.login('app=io.ox/mail', o)
  I.waitForVisible('.io-ox-mail-window .window-body')
  I.verifyUserSetup(userdata) // Verify user has Guard setup

  await createTestEmail(I, userdata)

  I.refreshPage()
  I.waitForVisible('.io-ox-mail-window .window-body')

  await checkRestored(I)
})

Scenario('Recover with without saved password - logout', async function ({ I, users }) {
  const o = {
    user: users[0]
  }
  const userdata = o.user.userdata

  // Log in as User 0
  I.login('app=io.ox/mail', o)
  I.waitForVisible('.io-ox-mail-window .window-body')
  I.verifyUserSetup(userdata) // Verify user has Guard setup

  await createTestEmail(I, userdata)

  I.logout()
  I.login('app=io.ox/mail', o)
  I.waitForVisible('.io-ox-mail-window .window-body')

  await checkRestored(I, userdata)
})

Scenario('Draft saving works after signout Guard', async function ({ I, users }) {
  const o = {
    user: users[0]
  }
  const userdata = o.user.userdata

  // Log in as User 0
  I.login('app=io.ox/mail', o)
  I.waitForVisible('.io-ox-mail-window .window-body')
  I.verifyUserSetup(userdata) // Verify user has Guard setup

  await createTestEmail(I, userdata, true)

  I.click('#io-ox-topbar-account-dropdown-icon .dropdown-toggle')
  I.waitForVisible('[data-name="logoutOG"]')
  I.click('[data-name="logoutOG"]')

  I.wait(1)

  within({ frame: SELECTOR.EDITOR_IFRAME }, async () => {
    I.fillField(SELECTOR.EMAIL_BODY, 'Added data')
  })

  I.waitForVisible('#ogPassword', 15)
  I.fillField('#ogPassword', userdata.password)
  I.click('.btn[data-action="ok"]')

  I.waitForText('Saved', 5, '.inline-yell')
})

Scenario('Closing compose after signout Guard', async function ({ I, users }) {
  const o = {
    user: users[0]
  }
  const userdata = o.user.userdata

  // Log in as User 0
  I.login('app=io.ox/mail', o)
  I.waitForVisible('.io-ox-mail-window .window-body')
  I.verifyUserSetup(userdata) // Verify user has Guard setup

  await createTestEmail(I, userdata, true)

  I.click('#io-ox-topbar-account-dropdown-icon .dropdown-toggle')
  I.waitForVisible('[data-name="logoutOG"]')
  I.click('[data-name="logoutOG"]')

  I.wait(1)

  within({ frame: SELECTOR.EDITOR_IFRAME }, async () => {
    I.fillField(SELECTOR.EMAIL_BODY, 'Added data')
  })

  I.click(SELECTOR.SAVE_AND_CLOSE)
  I.waitForElement('.modal')
  I.click('Save draft')

  I.waitForVisible('#ogPassword', 15)
  I.fillField('#ogPassword', userdata.password)
  I.click('.btn[data-action="ok"]')

  I.waitForText('Saved', 5, '.inline-yell')

  I.click(SELECTOR.SAVE_AND_CLOSE)
  I.waitForElement('.modal')
  I.click('Save draft')

  I.wait(1)

  I.waitForInvisible('.io-ox-mail-compose-window .editor')
})
*/
