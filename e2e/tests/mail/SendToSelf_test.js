/**
 * This work is provided under the terms of the CREATIVE COMMONS PUBLIC
 * LICENSE. This work is protected by copyright and/or other applicable
 * law. Any use of the work other than as authorized under this license
 * or copyright law is prohibited.
 *
 * http://creativecommons.org/licenses/by-nc-sa/2.5/
 * © 2017 OX Software GmbH, Germany. info@open-xchange.com
 *
 * @author Greg Hill <greg.hill@open-xchange.com>
 */

/// <reference path="../../steps.d.ts" />

Feature('Send Encrypted To Self')

const SELECTOR = require('../../constants')

const largeFilename = 'testFiles/testLargeData.txt'
const fs = require('fs')

Before(async function ({ users }) {
  await users.create()

  await new Promise(function (resolve, reject) {
    let fileData = ''
    for (let i = 0; i < 2500000; i++) {
      fileData += 'test '
    }
    fs.writeFile(largeFilename, fileData, (err) => {
      resolve()
    })
  })
})

After(async function ({ users }) {
  await users.removeAll()
  fs.unlinkSync(largeFilename)
})

async function sendMail (I, subject, data, o, attachment, options) {
  // Open compose
  I.sendEmail(o, subject, data, attachment, options)

  I.waitForVisible('.io-ox-mail-window .leftside')
  I.click(SELECTOR.REFRESH)
  I.waitForVisible(SELECTOR.UNREAD)
  I.click(SELECTOR.UNREAD)
  I.waitForVisible(SELECTOR.DETAIL_SUBJECT)
  I.see(subject)

  // Decrypt
  I.waitForElement(SELECTOR.GUARD_PASS_BUTTON)
  I.waitForElement(SELECTOR.PASSWORD_PROMPT)
  I.insertCryptPassword(SELECTOR.PASSWORD_PROMPT, o)
  I.click(SELECTOR.GUARD_PASS_BUTTON)

  I.verifyDecryptedMail(subject, data)
}

Scenario('Compose and receive encrypted email using to', async function ({ I, users }) {
  const o = {
    user: users[0]
  }

  await I.setupUser(o, true)

  // Log in as User 0
  await I.login('app=io.ox/mail', o)

  I.waitForVisible('.io-ox-mail-window .window-body')

  I.verifyUserSetup(o) // Verify user has Guard setup

  const random = Math.floor((Math.random() * 100000))
  const subject = 'Test Subject ' + random
  const data = 'Test data ' + random

  await sendMail(I, subject, data, o)

  // OK, done
  I.logout()
})

Scenario('Compose and receive encrypted email using bcc', async function ({ I, users }) {
  const o = {
    user: users[0]
  }

  await I.setupUser(o, true)

  // Log in as User 0
  I.login('app=io.ox/mail', o)

  I.waitForVisible('.io-ox-mail-window .window-body')

  I.verifyUserSetup(o) // Verify user has Guard setup

  const random = Math.floor((Math.random() * 100000))
  const subject = 'Test Subject ' + random
  const data = 'Test data ' + random

  await sendMail(I, subject, data, o, undefined, { type: 'bcc' })

  I.dontSee('Blind') // make sure Blind copy not seen
  // OK, done
  I.logout()
})

Scenario('Compose and receive encrypted email using cc', async function ({ I, users }) {
  const o = {
    user: users[0]
  }

  await I.setupUser(o, true)

  // Log in as User 0
  I.login('app=io.ox/mail', o)

  I.waitForVisible('.io-ox-mail-window .window-body')

  I.verifyUserSetup(o) // Verify user has Guard setup

  const random = Math.floor((Math.random() * 100000))
  const subject = 'Test Subject ' + random
  const data = 'Test data ' + random

  await sendMail(I, subject, data, o, undefined, { type: 'cc' })

  // OK, done
  I.logout()
})

Scenario('Test umlauts, compose and receive encrypted email', async function ({ I, users }) {
  const o = {
    user: users[0]
  }

  await I.setupUser(o, true)

  // Log in as User 0
  I.login('app=io.ox/mail', o)

  I.waitForVisible('.io-ox-mail-window .window-body')

  I.verifyUserSetup(o) // Verify user has Guard setup

  const random = Math.floor((Math.random() * 100000))
  const subject = 'Üben Subject ' + random
  const data = 'die Prüfung ' + random

  await sendMail(I, subject, data, o)

  // OK, done
  I.logout()
})

Scenario('Compose and receive encrypted pgp-inline email', async function ({ I, users }) {
  const o = {
    user: users[0]
  }

  await I.setupUser(o, true)

  // Log in as User 0
  I.login('app=io.ox/mail', o)

  I.waitForVisible('.io-ox-mail-window .window-body')

  I.verifyUserSetup(o) // Verify user has Guard setup

  const random = Math.floor((Math.random() * 100000))
  const subject = 'Test Subject ' + random
  const data = 'Test data ' + random

  // Open compose
  I.sendEmail(o, subject, data, undefined, { inline: true })

  I.waitForVisible('.io-ox-mail-window .leftside')
  I.click(SELECTOR.REFRESH)
  I.waitForVisible(SELECTOR.UNREAD)
  I.click(SELECTOR.UNREAD)
  I.waitForVisible(SELECTOR.DETAIL_SUBJECT)
  I.see(subject)

  // Decrypt
  I.waitForElement(SELECTOR.GUARD_PASS_BUTTON)
  I.waitForElement(SELECTOR.PASSWORD_PROMPT)
  I.insertCryptPassword(SELECTOR.PASSWORD_PROMPT, o)
  I.click(SELECTOR.GUARD_PASS_BUTTON)

  I.verifyDecryptedMail(subject, data)

  // OK, done
  I.logout()
})

/*
Scenario('Compose and receive large encrypted email', async function (I, users) {

    const o = {
        user: users[0]
    };

    await I.setupUser(o, true);

    // Log in as User 0
    I.login('app=io.ox/mail', o);

    I.waitForVisible('.io-ox-mail-window .window-body');

    I.verifyUserSetup(o);  // Verify user has Guard setup

    var random = Math.floor((Math.random() * 100000));
    var subject = "Test Subject " + random;
    var data = "Test data " + random;

    // Mark inbox as all read
    I.selectFolder('Inbox');
    I.waitForVisible(SELECTOR.FOLDER_CONTEXT_MENU);
    I.click(SELECTOR.FOLDER_CONTEXT_MENU);
    I.click(SELECTOR.FOLDER_CONTEXT_MENU_MARK_READ);

    // Open compose

    I.sendEmail(o, subject, data, largeFilename);

    I.waitForVisible('.io-ox-mail-window .leftside', 20);
    I.click(SELECTOR.REFRESH);
    I.waitForVisible(SELECTOR.UNREAD, 20);
    I.click(SELECTOR.UNREAD);
    I.waitForVisible(SELECTOR.DETAIL_SUBJECT);
    I.see(subject);

    // Decrypt
    I.waitForElement(SELECTOR.GUARD_PASS_BUTTON);
    I.waitForElement(SELECTOR.PASSWORD_PROMPT);
    I.insertCryptPassword(SELECTOR.PASSWORD_PROMPT, o);
    I.click(SELECTOR.GUARD_PASS_BUTTON);

    I.verifyDecryptedMail(subject, data);

    // Verify Attachment
    I.waitForVisible('a[data-action="io.ox/mail/attachment/actions/view"]');
    I.click('a[data-action="io.ox/mail/attachment/actions/view"]');

    // Verify password prompt
    I.auth(o);

    I.waitForVisible('.white-page.letter.plain-text', 60);
    I.see('test test test', '.plain-text');

    // Close it
    I.click('a[data-action="io.ox/core/viewer/actions/toolbar/close"]');
    I.waitForVisible('.io-ox-mail-window .leftside');

    // OK, done
    I.logout();

});
*/
