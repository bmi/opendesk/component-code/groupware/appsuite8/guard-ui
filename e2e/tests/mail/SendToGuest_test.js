/**
 * This work is provided under the terms of the CREATIVE COMMONS PUBLIC
 * LICENSE. This work is protected by copyright and/or other applicable
 * law. Any use of the work other than as authorized under this license
 * or copyright law is prohibited.
 *
 * http://creativecommons.org/licenses/by-nc-sa/2.5/
 * © 2017 OX Software GmbH, Germany. info@open-xchange.com
 *
 * @author Greg Hill <greg.hill@open-xchange.com>
 */

/// <reference path="../../steps.d.ts" />

Feature('Send Encrypted To guest user')

const codecept = require('codeceptjs')
const SELECTOR = require('../../constants')
const ox = codecept.config.get().helpers.OpenXchange

Before(async function ({ users, contexts }) {
  const ctx = ox.newContext
  await contexts.create({ id: ctx, filestoreId: ox.filestoreId }).then(async function (context) {
    context.hasCapability('guard')
    context.doesntHaveCapability('guard-pin')
    context.hasCapability('guard-mail')
    await users.create(undefined, context)
    await context.hasConfig('com.openexchange.share.guestHostname', ox.shareDomain)
  })

  await contexts.create({ id: ctx + 10, filestoreId: ox.filestoreId }).then(async function (context) {
    const guestUser = await users.create(undefined, context) // Two users
    await guestUser.hasAccessCombination('groupware_standard')
    await guestUser.doesntHaveCapability('guard')
  })
})

After(async function ({ users, contexts, I }) {
  await I.wipeUser(users).catch(e => console.log(e)) // cleanup guest accounts
  await users.removeAll()
  await contexts.removeAll()
})

Scenario('Compose and email to guest user and reply', async function ({ I, users }) {
  if (await I.amDisabled('guest')) return

  const o1 = {
    user: users[0]
  }
  const o2 = {
    user: users[1]
  }

  await I.setupUser(o1, true)
  await I.setupUser(o2)

  await o2.user.doesntHaveCapability('guard') // Non Guard user.  Guest only

  // Log in as User 0
  I.login('app=io.ox/mail', o1)
  I.waitForVisible('.io-ox-mail-window .window-body .classic-toolbar')

  I.verifyUserSetup(o1) // Verify user has Guard setup

  const random = Math.floor((Math.random() * 100000))
  const subject = 'Test Subject ' + random
  const data = 'Test data ' + random

  // Compose email to Guest
  I.sendEmailGuest(o2, subject, data)

  I.logout()

  // Login to retrieve guest invite email
  I.login('app=io.ox/mail', o2)

  I.waitForVisible('.io-ox-mail-window .leftside')
  I.click(SELECTOR.REFRESH)
  I.waitForVisible(SELECTOR.UNREAD)
  I.click(SELECTOR.UNREAD)
  I.wait(1)
  I.waitForVisible('.mail-detail-frame')
  I.switchTo('.mail-detail-frame')
  I.see('Test introduction text')
  I.click('.bodyButton a')

  I.wait(2)
  // Opening Guest user tab
  await I.switchToNextTab()

  I.waitForVisible('.io-ox-mail-window', 30)

  I.changeTemporaryPassword(o2)

  // I.waitForVisible('.wizard-footer [data-action="done"]');
  // I.click('.wizard-footer [data-action="done"]');

  I.wait(1)

  // Confirm email decrypted
  I.waitForVisible('.mail-detail-frame')
  I.switchTo('.mail-detail-frame')
  I.see(data)
  I.switchTo()

  // Reply
  I.click('.mail-header-actions .more-dropdown .dropdown-toggle')
  I.click('Reply')
  I.auth(o2)

  I.waitForVisible(SELECTOR.WINDOW_BUSY)
  I.waitForInvisible(SELECTOR.WINDOW_BUSY)
  // Check unable to add recipients

  I.dontSeeElementInDOM('.recipient-actions')

  // Change contents to ReplyTest to verify later
  await within({ frame: SELECTOR.EDITOR_IFRAME }, async () => {
    I.see(data, SELECTOR.EMAIL_BODY)
    I.see('wrote:')
    I.click(SELECTOR.EMAIL_BODY)
    I.fillField(SELECTOR.EMAIL_BODY, 'ReplyTest')
  })

  I.waitForVisible(SELECTOR.TRUSTED_KEY)
  // Send
  I.click('Send')

  I.waitForVisible('.io-ox-mail-window .leftside')
  I.wait(3)
  I.waitForInvisible('.mail-progress')

  // Cleanup
  // Guest link logout will bring this tab back to the recipient tab logged in
  I.closeCurrentTab()
  I.wait(1)

  // Logout from Guest webmail
  I.logout()

  // Log in as User 0
  I.login('app=io.ox/mail', o1)

  // Verify sender can read the reply
  I.waitForVisible('.io-ox-mail-window .window-body .classic-toolbar')
  I.waitForVisible(SELECTOR.UNREAD)
  I.click(SELECTOR.UNREAD)
  I.waitForElement(SELECTOR.GUARD_PASS_BUTTON)
  I.waitForElement(SELECTOR.PASSWORD_PROMPT)
  I.insertCryptPassword(SELECTOR.PASSWORD_PROMPT, o1)
  I.click(SELECTOR.GUARD_PASS_BUTTON)

  I.verifyDecryptedMail(subject, 'ReplyTest')
})

Scenario('Compose and email to guest user using bcc', async function ({ I, users }) {
  if (await I.amDisabled('guest')) return

  const o1 = {
    user: users[0]
  }
  const o2 = {
    user: users[1]
  }

  await I.setupUser(o1, true)
  await I.setupUser(o2)

  await o2.user.doesntHaveCapability('guard') // Non Guard user.  Guest only

  // Log in as User 0
  I.login('app=io.ox/mail', o1)

  I.waitForVisible('.io-ox-mail-window .window-body .classic-toolbar')

  I.verifyUserSetup(o1) // Verify user has Guard setup

  const random = Math.floor((Math.random() * 100000))
  const subject = 'Test Subject ' + random
  const data = 'Test data ' + random

  // Compose email to Guest
  I.sendEmailGuest(o2, subject, data, undefined, { type: 'bcc' })

  I.logout()

  // Login to retrieve guest invite email
  I.login('app=io.ox/mail', o2)

  I.waitForVisible('.io-ox-mail-window .leftside')
  I.click(SELECTOR.REFRESH)
  I.waitForVisible(SELECTOR.UNREAD)
  I.click(SELECTOR.UNREAD)
  I.wait(1)
  I.waitForVisible('.mail-detail-frame')
  I.switchTo('.mail-detail-frame')
  I.see('Test introduction text')
  I.click('.bodyButton a')

  I.wait(2)
  // Opening Guest user tab
  await I.switchToNextTab()

  I.waitForVisible('.io-ox-mail-window', 30)

  I.changeTemporaryPassword(o2)

  I.wait(1)
  I.dontSee('Blind') // make sure Blind copy not seen
  // Confirm email decrypted
  I.waitForVisible('.mail-detail-frame')
  I.switchTo('.mail-detail-frame')
  I.see(data)
  I.switchTo()
})
