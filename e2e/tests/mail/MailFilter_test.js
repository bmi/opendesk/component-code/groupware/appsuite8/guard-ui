/**
 * This work is provided under the terms of the CREATIVE COMMONS PUBLIC
 * LICENSE. This work is protected by copyright and/or other applicable
 * law. Any use of the work other than as authorized under this license
 * or copyright law is prohibited.
 *
 * http://creativecommons.org/licenses/by-nc-sa/2.5/
 * © 2020 OX Software GmbH, Germany. info@open-xchange.com
 *
 * @author Greg Hill <greg.hill@open-xchange.com>
 */

/// <reference path="../../steps.d.ts" />

const SELECTOR = require('../../constants')

Feature('Mail Filter')

Before(async function ({ users }) {
  await users.create()
})

After(async function ({ users }) {
  await users.removeAll()
})

async function createFilterRule (I, name, condition, comparison, action) {
  I.login('app=io.ox/settings&folder=virtual/settings/io.ox/mailfilter')

  I.waitForVisible('.settings-detail-pane .io-ox-mailfilter-settings h1')
  I.see('Mail Filter Rules')

  I.see('There is no rule defined')

  // create a test rule and check the inintial display
  I.click('Add new rule')
  I.see('Create new rule')
  I.see('This rule applies to all messages. Please add a condition to restrict this rule to specific messages.')

  I.fillField('rulename', name)

  // add condition
  if (condition) {
    I.click('Add condition')
    I.click(condition)
  }

  // add action
  I.click('Add action')
  I.waitForText(action)
  I.click(action)

  I.click('Save')
  I.waitForInvisible('.modal-dialog')
  I.waitForVisible('.io-ox-mailfilter-settings .list-group')
}

Scenario('MailFilter: Signature check', async function ({ I, users }) {
  if (await I.amDisabled('mailfilter')) return

  const o1 = {
    user: users[0]
  }

  await I.setupUser(o1, true)

  await createFilterRule(I, 'Signature Check', 'PGP signature', '', 'Set color flag')

  I.dontSee('no rule defined')
  I.see('Signature Check')

  I.openApp('Mail')

  I.verifyUserSetup(o1) // Verify user has Guard setup

  // Test data
  const random = Math.floor((Math.random() * 100000))
  const subject = 'Test Subject ' + random
  const data = 'Test data ' + random

  // Open compose
  I.sendEmail(o1, subject, data, undefined, { unencrypted: true, sign: true })

  I.waitForVisible('.io-ox-mail-window .leftside')
  I.waitForVisible(SELECTOR.UNREAD)
  I.click(SELECTOR.UNREAD)

  I.waitForVisible('.guard_signed')
  I.waitForVisible('.flag-picker .btn.flag_1')

  I.logout()
})

Scenario('MailFilter: Encrypt action', async function ({ I, users }) {
  if (await I.amDisabled('mailfilter')) return

  const o1 = {
    user: users[0]
  }

  await I.setupUser(o1, true)

  await createFilterRule(I, 'Encrypt Action', undefined, '', 'Encrypt the email')

  I.dontSee('no rule defined')
  I.see('Encrypt Action')

  I.openApp('Mail')

  I.verifyUserSetup(o1) // Verify user has Guard setup

  // Test data
  const random = Math.floor((Math.random() * 100000))
  const subject = 'Test Subject ' + random
  const data = 'Test data ' + random

  // Open compose
  I.sendEmail(o1, subject, data, undefined, { unencrypted: true, sign: true })

  I.waitForVisible('.io-ox-mail-window .leftside')
  I.waitForVisible(SELECTOR.UNREAD)
  I.click(SELECTOR.UNREAD)

  // Decrypt
  I.waitForElement(SELECTOR.GUARD_PASS_BUTTON)
  I.waitForElement(SELECTOR.PASSWORD_PROMPT)
  I.insertCryptPassword(SELECTOR.PASSWORD_PROMPT, o1)
  I.click(SELECTOR.GUARD_PASS_BUTTON)

  I.verifyDecryptedMail(subject, data, true)

  I.logout()
})
