{{/*
Create the name of the service account to use
*/}}
{{- define "guard-ui.serviceAccountName" -}}
{{- if .Values.serviceAccount.create }}
{{- default (include "ox-common.names.fullname" .) .Values.serviceAccount.name }}
{{- else }}
{{- default "default" .Values.serviceAccount.name }}
{{- end }}
{{- end }}
